

EXAMPLES = $(notdir $(wildcard examples/*))


## Build all examples
.PHONY: all build build-examples
all build build-examples: $(addprefix build-example-,$(EXAMPLES))

## Build an example
build-example-%:
	$(MAKE) -C examples/$* build

## Show this help text
.PHONY: help
help:
	@$(strip awk '/^## / { \
		sub(/^## /,""); \
		getline t; \
		while (t ~ /^(#|\.PHONY)/) getline t; \
		gsub(/:.*/,"",t); \
		printf " "" %-26s %s\n",t,$$0}' \
		$(MAKEFILE_LIST))

## Run an example
run-example-%:
	$(MAKE) -C examples/$* run

## Sync git submodules
.PHONY: sync
sync:
	git submodule sync
	git submodule update --init --recursive

## Sync all git submodules needed for examples
.PHONY: sync-examples
sync-examples:
	git submodule sync
	git submodule update --init --recursive --checkout

## Generate elrond_helpers/elrond_default_syscalls.c
.PHONY: gen-syscalls
gen-syscalls:
	$(strip ./elrond_helpers/gen_default_syscalls.py \
		./example-deps/musllibc/arch/aarch64_sel4/bits/syscall.h.in \
		-x elrond_helpers/elrond_syscalls.c \
		-o elrond_helpers/elrond_default_syscalls.c )

## Generate REFERENCE.md
.PHONY: gen-reference
gen-reference:
	$(strip ./gen_reference.py -o REFERENCE.md)


## Clean any generated files
.PHONY: clean clean-examples
clean clean-examples: $(addprefix clean-example-,$(EXAMPLES))

## Clean an example
clean-example-%:
	$(MAKE) -C examples/$* clean

