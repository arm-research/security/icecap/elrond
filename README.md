# Elrond

Elrond is an [seL4][seL4] userspace ELF loader and configurator.
It's goal is to expressively take care the glue code necessary to load and
run seL4 processes, without leaving the comfort of seL4's C APIs.

``` c
elrond_add_shared(&elrond, "task*", "test_shared", 4096, NULL, NULL);
elrond_ep_t task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);
elrond_add_call(&elrond, "task1", "test_call", task2_ep, NULL);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
#include <elrond_helpers.h>

char *test_shared = NULL;
ELROND_INFO("test_shared", &test_shared);
seL4_CPtr test_call = seL4_CapNull;
ELROND_INFO("test_call", &test_call);

int main(void) {
    strcpy(test_shared, "Chris was here!");
    seL4_SetMR(0, strlen(test_shared));
    seL4_Send(test_call, seL4_MessageInfo_new(0, 0, 0, 1));
}
```

``` rust
#![no_main]
use elrond_helpers as elrond;

#[elrond::info("test_shared")]
static mut TEST_SHARED: *mut u8 = std::ptr::null_mut();

#[elrond::call_cb("test_call")]
fn test_call(msg: seL4_MessageInfo_t) {
    assert_eq!(unsafe { seL4_MessageInfo_get_length(msg) }, 1);
    // prints: task2: Chris was here!
    println!("task2: {}", std::str::from_utf8(unsafe { 
        std::slice::from_raw_parts(TEST_SHARED, seL4_GetMR(0))
    });
}
```

Elrond is mainly the ELF loader and configurator that runs in a root task.
However, this repo contains a couple of other optional components to help with
building complete systems.

The systems in the examples here generally follow this layout:

![elrond-diagram][elrond-diagram]

For more details, see the documentation over the various components:
- [elrond/README.md][elrond]
- [elrond_helpers/README.md][elrond_helpers]
- [elrond_rust_helpers/README.md][elrond_rust_helpers]

Or the reference, for a quick list of everything in Elrond:
- [REFERENCE.md][reference]

# Build and run examples

To download example-specific dependencies:

``` bash
make sync-examples
```

Note this uses submodules with `update = none` to avoid these submodules being
downloaded in dependent projects.

To build and run a specific example:

``` bash
make build-example-09-kitchen-sink
make run-example-09-kitchen-sink
```

Use `ctrl-a x` to exit QEMU.

You can also build/run an example from its directory:

``` bash
cd examples/09-kitchen-sink
make build
make run
```

You can also build all examples at once, though note the examples build in
serial, don't share artifacts, and may take a while. In practice you may not
want to do this unless you are CI:

``` bash
make build-examples
```

These are all using seL4's build system, but wrapped into a high-level
command-only Makefile.

For more info on available commands:

``` bash
make help
```


[seL4]: https://sel4.systems
[elrond]: elrond/README.md
[elrond_helpers]: elrond_helpers/README.md
[elrond_rust_helpers]: elrond_rust_helpers/README.md
[elrond-diagram]: images/elrond-diagram.svg
[reference]: REFERENCE.md
