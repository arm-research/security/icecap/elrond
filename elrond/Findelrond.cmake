
set(ELROND_PATH "${CMAKE_CURRENT_LIST_DIR}" CACHE STRING "")
mark_as_advanced(ELROND_PATH)

function(elrond_import_project)
    add_subdirectory(${ELROND_PATH} elrond)
endfunction()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(
    elrond
    DEFAULT_MSG
    ELROND_PATH
)
