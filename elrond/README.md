# Elrond

Elrond is an [seL4][seL4] userspace ELF loader and configurator.
It's goal is to expressively take care the glue code necessary to load and
run seL4 processes, without leaving the comfort of seL4's C APIs.

## Running a process

Here's a small root task that uses Elrond to spawn a process:

``` c
#include <sel4/sel4.h>
#include <cpio/cpio.h>
#include <elrond.h>

extern const uint8_t _cpio_archive[];
extern const uint8_t _cpio_archive_end[];

int main(void) {
    // bootstrap elrond
    elrond_t elrond;
    elrond_bootstrap(&elrond, "root");
    printf("root: successfully bootstrapped elrond\n");

    // find our elf file in the CPIO archive
    size_t task1_elf_size = 0;
    const char *task1_elf = cpio_get_file(
            _cpio_archive, _cpio_archive_end-_cpio_archive,
            "task1", &task1_elf_size);

    // spawn our process
    elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

    // useful debugging prints
    elrond_debugdump(&elrond, NULL);
    seL4_DebugDumpScheduler();

    // don't segfault
    seL4_TCB_Suspend(seL4_CapInitThreadTCB);
}
```

[`elrond_bootstrap`][elrond_bootstrap] contains the common code necessary to
bootstrap an seL4 system and Elrond instance, which mostly involves
initializing cspace/vspace allocators.
As an alternative, [`elrond_create`][elrond_create] creates an Elrond instance
on existing cspace/vspace allocators, which is useful when you already have a
bootstrapped system.

`elrond_bootstrap` is not that interesting, so the rest of this document will
assume a bootstrapped Elrond instance.

The only thing to watch out for is if you run into out-of-memory errors in the
root task.
[elrond/CMakeLists.txt](CMakeLists.txt) has a number of configuration options
that control the several bootstrap heaps.

[`elrond_spawn`][elrond_spawn] is where the real magic happens.
Taking an ELF file, `elrond_spawn` creates a new cspace, a new vspace, a
new TCB, loads the ELF file into the vspace, and starts the process running.

``` c
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

Elrond can also spawn a process with a traditional argument list:

``` c
elrond_spawnl(&elrond, "task1", task1_elf, task1_elf_size,
        "task1", "--verbose", "do", "the", "thing", NULL);
```

And, with [`elrond_add_env`][elrond_add_env], environment variables:

``` c
elrond_add_env(&elrond, "task*", "WITH_STYLE", "1");
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

### Process patterns

The first argument to [`elrond_add_env`][elrond_add_env] is a process pattern.
Process patterns are used in Elrond to describe which processes inherit which
pieces of configuration.
This makes it easy to share configuration between processes without leaking
configuration to every process in the system:

``` c
elrond_add_env(&elrond, "task1", "TASK1_GETS_THIS",    "1");
elrond_add_env(&elrond, "task2", "TASK2_GETS_THIS",    "2");
elrond_add_env(&elrond, "*",     "EVERYONE_GETS_THIS", "3");
elrond_add_env(&elrond, "",      "NO_ONE_GETS_THIS",   "4");
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

Process patterns are comma-separated glob patterns. Here are some examples:

```
pattern         "task1" "task2" "task3" "root"  "rutabaga"
"task1"         y
"task*"         y       y       y
"task1,task2"   y       y
"task*,root"    y       y       y       y
"*a*"           y       y       y               y
"t*3"                           y
"r*t*"                                  y       y
"*"             y       y       y       y       y
""
```

### _start

Elrond follows the common [System V ABI][system_v] when spawning a process.
This involves starting the process at the entry point specified in the
`e_entry` field of the ELF header, usually named `_start`, with a stack
containing the `argv`, `envp`, and `auxv` arrays used to initialize the
C runtime.

Here's the layout of the entry point's stack:

```
sp -> [ argc         ] 1 word

      [ argv[0]      ] 1 word
      [ argv[1]      ] 1 word
      [ ...          ] ...
      [ argv[n]      ] 1 word
      [ NULL         ] 1 word

      [ envp[0]      ] 1 word
      [ envp[1]      ] 1 word
      [ ...          ] ...
      [ envp[n]      ] 1 word
      [ NULL         ] 1 word

      [ auxv[0]      ] 2 words
      [ auxv[1]      ] 2 words
      [ ...          ] ...
      [ auxv[n]      ] 2 words
      [ NULLx2       ] 2 words

      [ extra stuff  ] ...
```

`argv` is a NULL terminated array of strings, `envp` is a NULL terminated
array of strings in the form "KEY=VALUE", and `auxv` is a NULL terminated
array of key-value [auxiliary vectors][aux_vectors] containing extra
system information used to initialize the C runtime.

After this, it's up to the contents of the ELF file to actually get to main.
The easiest way to do this is to link against [`libsel4runtime`][libsel4runtime],
a library provided by seL4 for this purpose.
But at this point the process is in it's own cspace/vspace, so Elrond doesn't
really care.

### Process configuration

Elrond also a number of options for controlling how processes are spawned:

- [`elrond_add_cspace_size`][elrond_add_cspace_size]
- [`elrond_add_stack_size`][elrond_add_stack_size]
- [`elrond_add_heap_size`][elrond_add_heap_size]
- [`elrond_add_priority`][elrond_add_priority]

``` c
elrond_add_priority(&elrond, "task*", 100);
elrond_add_priority(&elrond, "important_task*", 200);
elrond_add_priority(&elrond, "unimportant_task*", 0);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
elrond_spawn(&elrond, "important_task3", task3_elf, task3_elf_size);
elrond_spawn(&elrond, "unimportant_task4", task4_elf, task4_elf_size);
```

By default, these use the configurable defaults specified in
[elrond/CMakeLists.txt](CMakeLists.txt).


## Configurating a process

So what does it mean to configurate a process?

### Elrond info sections

In addition to arguments and environment variables, Elrond provides a flexible
mechanism for writing extra info into the process at load time.
It does this by looking for special `.note.elrond.info.<key>` sections in the
ELF file, which describe where to write info associated with a given key.

The layout of each info section is nothing more than an array of pointers to
where to write the info, this makes it easy to express info sections directly
in C without needing to use a custom linker-script.

So, for example, if you told Elrond to provide some info with
[`elrond_add_info`][elrond_add_info]:

``` c
elrond_add_info(&elrond, "task*",
        "test_info", &(uint32_t){0x12345678}, sizeof(uint32_t));
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

This info could be accessed by the process like so:

``` c
uint32_t test_info = 0;
__attribute__((section(".note.elrond.info.test_info,\"\",@note//")))
static const void *__elrond_info_test_info = &test_info;

int main(void) {
    // prints: test_info: 0x12345678
    printf("test_info: 0x%08x\n", test_info);
}
```

The use of `.note` sections has several conveniences: `.note` sections avoid
garbage collection by the linker, do not take of additional memory at runtime,
and are not loaded during load time.

And since same-named sections are concatenated during linking, info sections
can be provided by any number of source files, as long as they are included
in the final link.

Of course, nothing stops you from providing info sections via a custom
linker-script for more control over the final image.

### Configurating seL4 capabilities

But Elrond isn't just limited to configurating raw data.
With [`elrond_add_cap`][elrond_add_cap] and [`elrond_add_badged`][elrond_add_bagded],
Elrond can provision processes with capabilities that are copied or minted into
the process's cspace at load time.
In this case, the newly created per-process cptr is written into the given
info section.

``` c
vka_object_t ep;
vka_alloc_endpoint(elrond_vka(&elrond), &ep);
elrond_add_cap(&elrond, "task*", "test_ep", ep.cptr);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
seL4_CPtr test_ep = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ep,\"\",@note//")))
static const void *__elrond_info_test_ep = &test_ep;

int main(void) {
    seL4_SetMR(0, 42);
    seL4_Send(test_ep, seL4_MessageInfo_new(0, 0, 0, 1));
}
```

``` c
seL4_CPtr test_ep = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ep,\"\",@note//")))
static const void *__elrond_info_test_ep = &test_ep;

int main(void) {
    seL4_MessageInfo_t tag = seL4_Recv(test_ep, NULL);
    assert(seL4_MessageInfo_get_length(tag) == 1);
    // prints: task2: recved: 42
    printf("task2: recved: %ld\n", seL4_GetMR(0));
}
```

In theory, this is all you need to build a fully-featured seL4 system, but
Elrond does provide a couple more bits of functionality for convenience.

### Configurating shared memory

With [`elrond_add_shared`][elrond_add_shared], Elrond can easily provide shared
memory between processes and the root task.

``` c
elrond_add_shared(&elrond, "task*", "test_shared", 4096, NULL, NULL);
vka_object_t ntfn;
vka_alloc_notification(elrond_vka(&elrond), &ntfn);
elrond_add_cap(&elrond, "task*", "test_ntfn", ntfn.cptr);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
char *test_shared = NULL;
__attribute__((section(".note.elrond.info.test_shared,\"\",@note//")))
static const void *__elrond_info_test_shared = &test_shared;

seL4_CPtr test_ntfn = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ntfn,\"\",@note//")))
static const void *__elrond_info_test_ntfn = &test_ntfn;

int main(void) {
    strcpy(test_shared, "Chris was here!");
    seL4_Signal(test_ntfn);
}
```

``` c
char *test_shared = NULL;
__attribute__((section(".note.elrond.info.test_shared,\"\",@note//")))
static const void *__elrond_info_test_shared = &test_shared;

seL4_CPtr test_ntfn = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ntfn,\"\",@note//")))
static const void *__elrond_info_test_ntfn = &test_ntfn;

int main(void) {
    seL4_Wait(test_ntfn, NULL);
    // prints: task2: Chris was here!
    printf("task2: %s\n", test_shared);
}
```

[`elrond_add_pages`][elrond_add_pages] allows for more flexible page mapping
in the processes, taking an array of seL4 frame objects.
These frame objects can be duplicated in an `elrond_add_pages` and used in
multiple `elrond_add_pages` calls for creative memory sharing/mirroring.

``` c
vka_object_t frame;
vka_alloc_frame(elrond_vka(&elrond), 12, &frame);
elrond_add_pages(&elrond, "task*",
        "test_pages", 4096, (seL4_CPtr[]){frame.cptr}, 4096,
        seL4_AllRights, true, NULL);
elrond_add_pages(&elrond, "task*",
        "test_pages_ro", 4096, (seL4_CPtr[]){frame.cptr}, 4096,
        seL4_CanRead, true, NULL);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

``` c
char *test_pages = NULL;
__attribute__((section(".note.elrond.info.test_pages,\"\",@note//")))
static const void *__elrond_info_test_pages = &test_pages;

char *test_pages_ro = NULL;
__attribute__((section(".note.elrond.info.test_pages_ro,\"\",@note//")))
static const void *__elrond_info_test_pages_ro = &test_pages_ro;

int main(void) {
    strcpy(test_pages, "redrum");
    // prints: redrum
    printf("%s\n", test_pages_ro);
}
```

The last argument of `elrond_add_shared` and `elrond_add_pages` also allows you
to map the shared memory into the root task's vspace.

### Configurating IPC

IPC gets a little bit complicated.

You can, of course, just distribute endpoints as seL4 capabilities to each
processs like we did in the earlier example.
But, without the ability to poll multiple endpoints efficiently, you generally
want to give each inter-process call a unique badge, mint the caller's endpoint
with the badge, and share the badge with the callee.
This can get quite tedius.

To make things more complicated, seL4 has two types of IPC: synchronous message
passing with [`seL4_Send`][seL4_Send]/[`seL4_Call`][seL4_Call], and
asynchronous notifications with [`seL4_Signal`][seL4_Signal].
It's possible to recieve both messages and notifications on a single endpoint,
but only if you use [`seL4_TCB_BindNotification`][seL4_TCB_BindNotification] to
bind a notification object to a thread, in which case notifications can be
returned by any endpoint recieves in that thread.
If you do this, you also need to make sure the messages and endpoints have
unique badges in order to distinguish between the two.

To help with stitching together IPC, Elrond provides a high-level
[`elrond_ep_t`][elrond_ep_t] endpoint type, that acts as an allocator of call
and notification badges on a pair of seL4 endpoints.
[`elrond_add_ep`][elrond_add_ep] creates an endpoint for a process, with
[`elrond_add_call`][elrond_add_call] and [`elrond_add_ntfn`][elrond_add_ntfn]
creating badges and badged capabilities for IPC.

``` c
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "test_ep", &task2_ep);
elrond_add_call(&elrond, "task1", "test_call", task2_ep, NULL);
elrond_add_ntfn(&elrond, "task1", "test_ntfn", task2_ep, NULL);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
seL4_CPtr test_call = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_call,\"\",@note//")))
static const void *__elrond_info_test_call = &test_call;

seL4_CPtr test_ntfn = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ntfn,\"\",@note//")))
static const void *__elrond_info_test_ntfn = &test_ntfn;

int main(void) {
    // send to test_call
    seL4_SetMR(0, 42);
    seL4_Send(test_call, seL4_MessageInfo_new(0, 0, 0, 1));
    // signal test_ntfn
    seL4_Signal(test_ntfn);
}
```

``` c
seL4_CPtr test_ep_call_ep = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ep_call_ep,\"\",@note//")))
static const void *__elrond_info_test_ep_call_ep = &test_ep_call_ep;

seL4_CPtr test_ep_ntfn_ep = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ep_ntfn_ep,\"\",@note//")))
static const void *__elrond_info_test_ep_ntfn_ep = &test_ep_ntfn_ep;

seL4_Word test_call_badge = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_call_badge,\"\",@note//")))
static const void *__elrond_info_test_call_badge = &test_call_badge;

seL4_Word test_ntfn_badge = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ntfn_badge,\"\",@note//")))
static const void *__elrond_info_test_ntfn_badge = &test_ntfn_badge;

int main(void) {
    seL4_Word badge;
    seL4_MessageInfo_t tag = seL4_Recv(test_ep_call_ep, &badge);
    assert(badge == test_call_badge);
    assert(seL4_MessageInfo_get_length(tag) == 1);
    // prints: task2: recved: 0x0000000000000001 42
    printf("task2: recved: 0x%016x %ld\n", badge, seL4_GetMR(0));

    seL4_Wait(test_ep_ntfn_ep, &badge);
    // prints: task2: recved: 0x8000000000000001
    printf("task2: recved: 0x%016x\n", badge);
}
```

The endpoint named "main" is special in that the notification object will be
bound to the process's main TCB.

``` c
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);
elrond_add_call(&elrond, "task1", "test_call", task2_ep, NULL);
elrond_add_ntfn(&elrond, "task1", "test_ntfn", task2_ep, NULL);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
seL4_CPtr test_call = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_call,\"\",@note//")))
static const void *__elrond_info_test_call = &test_call;

seL4_CPtr test_ntfn = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_ntfn,\"\",@note//")))
static const void *__elrond_info_test_ntfn = &test_ntfn;

int main(void) {
    // send to test_call
    seL4_SetMR(0, 42);
    seL4_Send(test_call, seL4_MessageInfo_new(0, 0, 0, 1));
    // signal test_ntfn
    seL4_Signal(test_ntfn);
}
```

``` c
seL4_CPtr main_call_ep = seL4_CapNull;
__attribute__((section(".note.elrond.info.main_call_ep,\"\",@note//")))
static const void *__elrond_info_main_call_ep = &main_call_ep;

int main(void) {
    while (1) {
        seL4_Word badge;
        seL4_Recv(main_call_ep, &badge);
        // prints: task2: recved: 0x0000000000000001
        // prints: task2: recved: 0x8000000000000001
        printf("task2: recved: 0x%016x\n", badge);
    }
}
```

Additionally, [`elrond_add_wakeup`][elrond_add_wakeup] creates a badged
notification with no bits set.
This is useful for waking up a process waiting on an endpoint without
using any specific notification.

``` c
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);
elrond_add_wakeup(&elrond, "task1", "test_wakeup", task2_ep, NULL);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
seL4_CPtr test_wakeup = seL4_CapNull;
__attribute__((section(".note.elrond.info.test_wakeup,\"\",@note//")))
static const void *__elrond_info_test_wakeup = &test_wakeup;

int main(void) {
    // signal test_wakeup
    seL4_Signal(test_wakeup);
}
```

``` c
seL4_CPtr main_call_ep = seL4_CapNull;
__attribute__((section(".note.elrond.info.main_call_ep,\"\",@note//")))
static const void *__elrond_info_main_call_ep = &main_call_ep;

int main(void) {
    seL4_Word badge;
    seL4_Recv(main_call_ep, &badge);
    // prints: task2: recved: 0x8000000000000000
    printf("task2: recved: 0x%016x\n", badge);
}
```

The last argument of `elrond_add_call`, `elrond_add_ntfn`, and
`elrond_add_wakeup` also allows you to mint the badged capability
into the root task's cspace.

You can also create an `elrond_ep_t` from existing badged capabilities
using [`elrond_ep_frombadged`][elrond_ep_frombadged].
This makes it possible to redirect any APIs using `elrond_ep_t` to
a specific seL4 endpoint.

Here is the encoding for allocated badges.
The highest bit is used to distinguish between calls and notifications,
with `0x8000000000000000` reserved for wakeups, and `0x0000000000000000`
reserved as a null badge.

```
...                ...
0x0000000000000005 ELROND_CALL_BADGE(4)
0x0000000000000004 ELROND_CALL_BADGE(3)
0x0000000000000003 ELROND_CALL_BADGE(2)
0x0000000000000002 ELROND_CALL_BADGE(1)
0x0000000000000001 ELROND_CALL_BADGE(0)

0x0000000000000000 ELROND_NULL_BADGE

0x8000000000000000 ELROND_WAKEUP_BADGE

0x8000000000000001 ELROND_NTFN_BADGE(0)
0x8000000000000002 ELROND_NTFN_BADGE(1)
0x8000000000000004 ELROND_NTFN_BADGE(2)
0x8000000000000008 ELROND_NTFN_BADGE(3)
0x8000000000000010 ELROND_NTFN_BADGE(4)
...                ...
```

### Ring buffers

Elrond also provides a simple single-producer, single-consumer ring buffer
implementation with [`elrond_ring_t`][elrond_ring_t] and
[`elrond_add_ring`][elrond_add_ring].
Note that `elrond_ring_t` lives in a separate header file, `elrond_ring.h`.
Intended also as a small proof-of-concept, `elrond_ring_t` is built entirely out
of the above primitives.


``` c
elrond_ep_t *task1_ep;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);

elrond_add_ring(&elrond, "task1,task2",
        "test_ring", 8192,
        task1_ep, NULL,
        task2_ep, NULL);

elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

Since ring buffers are not really seL4 primitives, they require a bit of
software inside the process to be useful.
This is described in detail in [elrond_helpers/README.md][elrond_helpers-rings],
though, of course, ring buffers are completely optional.

Elrond in the root task has the same ring buffer API in Elrond helpers,
however it is limited to non-blocking operations, as there isn't really
a "main" endpoint in the root task.

## Elrond helpers

That's pretty much the full core of Elrond.

Elrond also has helpers to help write Elrond loaded processes for
both C and Rust.
However, it's worth noting that these are completely optional:

- [elrond_helpers][elrond_helpers]
- [elrond_rust_helpers][elrond_rust_helpers]


[seL4]: https://sel4.systems
[elrond_bootstrap]: ../REFERENCE.md#elrond-functions
[elrond_create]: ../REFERENCE.md#elrond-functions
[elrond_spawn]: ../REFERENCE.md#elrond-functions
[elrond_add_env]: ../REFERENCE.md#elrond-functions
[system_v]: http://www.sco.com/developers/devspecs/gabi41.pdf
[aux_vectors]: http://articles.manugarg.com/aboutelfauxiliaryvectors.html
[libsel4runtime]: https://github.com/seL4/sel4runtime
[elrond_add_cspace_size]: ../REFERENCE.md#elrond-functions
[elrond_add_stack_size]: ../REFERENCE.md#elrond-functions
[elrond_add_heap_size]: ../REFERENCE.md#elrond-functions
[elrond_add_priority]: ../REFERENCE.md#elrond-functions
[elrond_add_info]: ../REFERENCE.md#elrond-functions
[elrond_add_cap]: ../REFERENCE.md#elrond-functions
[elrond_add_bagded]: ../REFERENCE.md#elrond-functions
[elrond_add_shared]: ../REFERENCE.md#elrond-functions
[elrond_add_pages]: ../REFERENCE.md#elrond-functions
[seL4_Send]: https://docs.sel4.systems/projects/sel4/api-doc.html#send
[seL4_Call]: https://docs.sel4.systems/projects/sel4/api-doc.html#call
[seL4_Signal]: https://docs.sel4.systems/projects/sel4/api-doc.html#signal
[seL4_TCB_BindNotification]: https://docs.sel4.systems/projects/sel4/api-doc.html#bind-notification
[elrond_ep_t]: ../REFERENCE.md#elrond-endpoint-functions
[elrond_add_ep]: ../REFERENCE.md#elrond-functions
[elrond_add_call]: ../REFERENCE.md#elrond-functions
[elrond_add_ntfn]: ../REFERENCE.md#elrond-functions
[elrond_add_wakeup]: ../REFERENCE.md#elrond-functions
[elrond_ep_frombadged]: ../REFERENCE.md#elrond-endpoint-functions
[elrond_ring_t]: ../REFERENCE.md#elrond-ring-buffer-functions
[elrond_add_ring]: ../REFERENCE.md#elrond-ring-buffer-functions
[elrond_helpers-rings]: ../elrond_helpers/README.md#ring-buffers
[elrond_helpers]: ../elrond_helpers/README.md
[elrond_rust_helpers]: ../elrond_rust_helpers/README.md

