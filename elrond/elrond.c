//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include "elrond.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>

#include <sel4runtime/auxv.h>
#include <sel4utils/mapping.h>

// info section prefix
#define ELROND_INFO_PREFIX      ".note.elrond.info."
#define ELROND_INFO_PREFIX_LEN  (sizeof(ELROND_INFO_PREFIX)-1)


// utils
static inline size_t nlog2(size_t a) {
    return 8*sizeof(size_t) - __builtin_clzl(a-1);
}

static inline size_t npw2(size_t a) {
    return 1 << nlog2(a);
}

static inline size_t aligndown(size_t a, size_t alignment) {
    return a - (a % alignment);
}

static inline size_t alignup(size_t a, size_t alignment) {
    return aligndown(a + alignment-1, alignment);
}

static inline size_t min(size_t a, size_t b) {
    return a < b ? a : b;
}

static inline size_t max(size_t a, size_t b) {
    return a > b ? a : b;
}

// compare one string against 2 concatenated strings
static int strcmp2(const char *a, const char *b0, const char *b1) {
    size_t i = 0;
    size_t j = 0;
    for (; b0[i]; i++) {
        if (a[i] != b0[i]) {
            return a[i] - b0[i];
        }
    }

    for (; b1[j]; i++, j++) {
        if (a[i] != b1[j]) {
            return a[i] - b1[j];
        }
    }

    return a[i] - b1[j];
}



// test that a pattern matches a name
//
// a pattern in elrond is a comma-separated list of globs, some examples:
// "a,b"   - matches "a", "b"
// "a , b" - matches "a", "b"
// "a*,b*c" - matches "ardvark", "bic"
// ""      - matches nothing
// "*"     - matches everything
//
static bool elrond_match(const char *pattern, const char *key) {
    size_t key_len = strlen(key);
    while (*pattern) {
        // skip spaces and commas
        pattern += strspn(pattern, " ,");

        // find end of pattern
        size_t next_comma = strcspn(pattern, ",");
        size_t pattern_len = next_comma;
        while (pattern_len > 0 && pattern[pattern_len-1] == ' ') {
            pattern_len -= 1;
        }

        // compare glob pattern
        size_t i = 0;
        size_t j = 0;
        size_t backtrack_i = 0;
        size_t backtrack_j = 0;
        while (1) {
            if (i == pattern_len && j == key_len) {
                // found a match
                return true;
            } else if (i < pattern_len && pattern[i] == '*') {
                // match a glob, pend a backtrack
                i += 1;
                backtrack_i = i-1;
                backtrack_j = j+1;
            } else if (i < pattern_len && j < key_len && pattern[i] == key[j]) {
                // match a character
                i += 1;
                j += 1;
            } else {
                // no match, backtrack?
                if (backtrack_j > 0 && backtrack_j <= key_len) {
                    i = backtrack_i;
                    j = backtrack_j;
                } else {
                    break;
                }
            }
        }

        // found a match?
        if (i == pattern_len && j == key_len) {
            return true;
        }

        // move on to next pattern
        pattern = &pattern[next_comma];
    }

    return false;
}

// append onto an array with amortized doubling, returns NULL if out of memory
void *elrond_append(
        void **entries,
        size_t entry_size,
        size_t *entry_count,
        size_t *entry_capacity) {
    uint8_t *entries_ = (uint8_t*)*entries;
    size_t entry_count_ = *entry_count;
    size_t entry_capacity_ = *entry_capacity;

    entry_count_ += 1;
    if (entry_count_ >= entry_capacity_) {
        if (entry_capacity_ == 0) {
            entry_capacity_ = 4;
        } else {
            entry_capacity_ *= 2;
        }

        entries_ = realloc(entries_, entry_capacity_ * entry_size);
        if (!entries_) {
            return NULL;
        }
    }

    *entries = entries_;
    *entry_count = entry_count_;
    *entry_capacity = entry_capacity_;
    return &entries_[(entry_count_-1) * entry_size];
}



// create an elrond instance
int elrond_create(elrond_t *elrond,
        vka_t *vka,
        vspace_t *vspace,
        seL4_CPtr tcb,
        seL4_CPtr asid_pool) {
    // most of elrond is zero intialized, this memset
    // is doing a lot of work
    memset(elrond, 0, sizeof(elrond_t));

    elrond->vka = vka;
    elrond->vspace = vspace;
    elrond->tcb = tcb;
    elrond->asid_pool = asid_pool;

    return 0;
}

// optional overrideable configurations
int elrond_add_cspace_size(elrond_t *elrond, const char *pattern,
        size_t cspace_size) {
    return elrond_add_info(elrond, pattern,
            "cspace_size", &cspace_size, sizeof(size_t));
}

int elrond_add_stack_size(elrond_t *elrond, const char *pattern,
        size_t stack_size) {
    return elrond_add_info(elrond, pattern,
            "stack_size", &stack_size, sizeof(size_t));
}

int elrond_add_heap_size(elrond_t *elrond, const char *pattern,
        size_t heap_size) {
    return elrond_add_info(elrond, pattern,
            "heap_size", &heap_size, sizeof(size_t));
}

int elrond_add_priority(elrond_t *elrond, const char *pattern,
        uint8_t priority) {
    return elrond_add_info(elrond, pattern,
            "priority", &priority, sizeof(uint8_t));
}

int elrond_add_env(elrond_t *elrond, const char *pattern,
        const char *key, const char *val) {
    // allocate KEY=VAL string
    size_t key_len = strlen(key);
    size_t val_len = strlen(val);
    char *var = malloc(key_len + 1 + val_len + 1);
    if (!var) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }
    strcpy(var, key);
    var[key_len] = '=';
    strcpy(var+key_len+1, val);
    var[key_len+1+val_len] = '\0';

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    // append onto env list
    elrond_env_t *x = elrond_append(
            (void**)&elrond->envs,
            sizeof(elrond_env_t),
            &elrond->env_count,
            &elrond->env_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->var = var;
    return 0;
}

// NOTE this also allows '=' as a "null terminator", this allows
// env strings to be used as keys for lookup
static bool elrond_env_filter(void *ctx, void *entry) {
    const char *var = ctx;
    elrond_env_t *entry_ = entry;
    size_t var_len = strcspn(var, "=");
    size_t entry_len = strcspn(entry_->var, "=");
    return var_len == entry_len && memcmp(var, entry_->var, var_len) == 0;
}

static const char *elrond_lookup_env(elrond_t *elrond,
        const char *process,
        const char *var) {
    size_t var_len = strcspn(var, "=");

    elrond_env_t *x;
    for (size_t i = 0; i < elrond->env_count; i++) {
        x = &elrond->envs[elrond->env_count-1-i];
        if (process && !elrond_match(x->pattern, process)) {
            continue;
        }

        // NOTE this also allows '=' as a "null terminator", this allows
        // env strings to be used as keys for lookup
        size_t x_len = strcspn(x->var, "=");
        if (var_len == x_len && memcmp(var, x->var, var_len) == 0) {
            goto found;
        }
    }

    // not found
    return NULL;

    found:;
    // a missing '=' indicates no var
    if (x->var[var_len] != '=') {
        return NULL;
    }

    return x->var+var_len+1;
}

// overridable extra info loaded before init
int elrond_add_info(elrond_t *elrond, const char *pattern,
        const char *name, const void *data, size_t size) {
    char *name_ = "";
    if (name) {
        name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);
    }

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = size;
    if (size <= sizeof(seL4_Word)) {
        x->type = ELROND_TYPE_INLINED;
        x->u.word = 0;
        memcpy(&x->u.word, data, size);
    } else {
        x->type = ELROND_TYPE_DATA;
        void *data_ = malloc(size);
        if (!data_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        memcpy(data_, data, size);
        x->u.data = data_;
    }
    return 0;
}

static bool elrond_info_filter(void *ctx, void *entry) {
    const char *name = ctx;
    elrond_info_t *entry_ = entry;
    return strcmp(name, entry_->name) == 0;
}


static bool elrond_lookup_info(elrond_t *elrond,
        const char *process,
        const char *name,
        elrond_info_t *info) {
    for (size_t i = 0; i < elrond->info_count; i++) {
        elrond_info_t *info_ = &elrond->infos[elrond->info_count-1-i];
        if (process && !elrond_match(info_->pattern, process)) {
            continue;
        }

        if (info_->type == ELROND_TYPE_INLINED
                || info_->type == ELROND_TYPE_DATA
                || info_->type == ELROND_TYPE_CAP
                || info_->type == ELROND_TYPE_BADGED) {
            if (strcmp(name, info_->name) == 0) {
                *info = *info_;
                return true;
            }
        } else if (info_->type == ELROND_TYPE_SHARED) {
            if (strcmp(name, info_->name) == 0) {
                info->name = name;
                info->type = ELROND_TYPE_SHARED;
                info->u.shared = info_->u.shared;
                info->size = sizeof(uintptr_t);
                return true;
            } else if (strcmp2(name, info_->name, "_size") == 0) {
                info->name = name;
                info->type = ELROND_TYPE_INLINED;
                info->u.word = info_->u.shared
                        ? info_->u.shared->size
                        : 0;
                info->size = sizeof(size_t);
                return true;
            }
        } else if (info_->type == ELROND_TYPE_EP) {
            if (strcmp2(name, info_->name, "_call_ep") == 0) {
                info->name = name;
                info->type = ELROND_TYPE_CAP;
                info->u.cap = info_->u.ep->call_ep;
                info->size = sizeof(seL4_CPtr);
                return true;
            } else if (strcmp2(name, info_->name, "_ntfn_ep") == 0) {
                info->name = name;
                info->type = ELROND_TYPE_CAP;
                info->u.cap = info_->u.ep->ntfn_ep;
                info->size = sizeof(seL4_CPtr);
                return true;
            } else if (strcmp2(name, info_->name, "_call_count") == 0) {
                info->name = name;
                info->type = ELROND_TYPE_INLINED;
                info->u.word = info_->u.ep->call_count;
                info->size = sizeof(size_t);
                return true;
            } else if (strcmp2(name, info_->name, "_ntfn_count") == 0) {
                info->name = name;
                info->type = ELROND_TYPE_INLINED;
                info->u.word = info_->u.ep->ntfn_count;
                info->size = sizeof(size_t);
                return true;
            } else {
                // check if this is a badge
                for (size_t j = 0; j < info_->u.ep->call_count; j++) {
                    if (strcmp2(name, info_->u.ep->calls[j], "_badge") == 0) {
                        info->name = name;
                        info->type = ELROND_TYPE_INLINED;
                        info->u.word = ELROND_CALL_BADGE(j);
                        info->size = sizeof(seL4_Word);
                        return true;
                    }
                }

                for (size_t j = 0; j < info_->u.ep->ntfn_count; j++) {
                    if (strcmp2(name, info_->u.ep->ntfns[j], "_badge") == 0) {
                        info->name = name;
                        info->type = ELROND_TYPE_INLINED;
                        info->u.word = ELROND_NTFN_BADGE(j);
                        info->size = sizeof(seL4_Word);
                        return true;
                    }
                }
            }
        } else {
            notfound:;
            ZF_LOGE("Found unexpected %d", info_->type);
            assert(false);
        }
    }

    return false;
}

int elrond_add_cap(elrond_t *elrond, const char *pattern,
        const char *name, seL4_CPtr cap) {
    char *name_ = "";
    if (name) {
        name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);
    }

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = sizeof(seL4_CPtr);
    x->type = ELROND_TYPE_CAP;
    x->u.cap = cap;
    return 0;
}

int elrond_add_badged(elrond_t *elrond, const char *pattern,
        const char *name, seL4_CPtr cap, seL4_Word badge) {
    char *name_ = "";
    if (name) {
        name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);
    }

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = sizeof(seL4_CPtr);
    x->type = ELROND_TYPE_BADGED;
    x->u.badged.cap = cap;
    x->u.badged.badge = badge;
    return 0;
}


// read from process's vspace
int elrond_process_read(elrond_t *elrond,
        elrond_process_t *process,
        uintptr_t vaddr,
        void *data,
        size_t size) {
    uint8_t *data_ = data;
    while (size > 0) {
        // write at most one frame at a time
        size_t delta = min(PAGE_SIZE_4K - (vaddr % PAGE_SIZE_4K), size);

        // get cap for frame
        seL4_CPtr frame = vspace_get_cap(&process->vspace,
                (void*)PAGE_ALIGN_4K(vaddr));
        if (!frame) {
            ZF_LOGE("Could not get frame cap for vaddr %p",
                    (void*)PAGE_ALIGN_4K(vaddr));
            return -ENOENT;
        }

        // map it into our vspace
        void *mapping = sel4utils_dup_and_map(elrond->vka, elrond->vspace,
                frame, PAGE_BITS_4K);
        if (!mapping) {
            ZF_LOGE("Could not map frame for vaddr %p",
                    (void*)PAGE_ALIGN_4K(vaddr));
            return -EACCES;
        }

        // copy data
        memcpy(data_, mapping + (vaddr % PAGE_SIZE_4K), delta);

        // unmap our frame
        sel4utils_unmap_dup(elrond->vka, elrond->vspace,
                mapping, PAGE_BITS_4K);

        vaddr += delta;
        data_ += delta;
        size -= delta;
    }

    return 0;
}

// write to process's vspace
int elrond_process_write(elrond_t *elrond,
        elrond_process_t *process,
        uintptr_t vaddr,
        const void *data,
        size_t size) {
    const uint8_t *data_ = data;
    while (size > 0) {
        // write at most one frame at a time
        size_t delta = min(PAGE_SIZE_4K - (vaddr % PAGE_SIZE_4K), size);

        // get cap for frame
        seL4_CPtr frame = vspace_get_cap(&process->vspace,
                (void*)PAGE_ALIGN_4K(vaddr));
        if (!frame) {
            ZF_LOGE("Could not get frame cap for vaddr %p",
                    (void*)PAGE_ALIGN_4K(vaddr));
            return -ENOENT;
        }

        // map it into our vspace
        void *mapping = sel4utils_dup_and_map(elrond->vka, elrond->vspace,
                frame, PAGE_BITS_4K);
        if (!mapping) {
            ZF_LOGE("Could not map frame for vaddr %p",
                    (void*)PAGE_ALIGN_4K(vaddr));
            return -EACCES;
        }

        // copy data
        memcpy(mapping + (vaddr % PAGE_SIZE_4K), data_, delta);

        // unmap our frame
        sel4utils_unmap_dup(elrond->vka, elrond->vspace,
                mapping, PAGE_BITS_4K);

        vaddr += delta;
        data_ += delta;
        size -= delta;
    }

    return 0;
}

// push onto a process's stack
//
// don't do this when the process is running!
int elrond_process_push(elrond_t *elrond,
        elrond_process_t *process,
        uintptr_t *sp,
        const void *data,
        size_t size) {
    size_t sp_ = *sp - size;
    int err = elrond_process_write(elrond, process, sp_, data, size);
    if (err) {
        return err;
    }

    *sp = sp_;
    return 0;
}

// determine the process's configuration
static int elrond_process_configure(elrond_t *elrond,
        elrond_process_t *process,
        const char **argv) {
    // find argc
    process->arg_count = 0;
    while (argv && argv[process->arg_count]) {
        process->arg_count += 1;
    }

    // copy over argv
    process->argv = malloc(process->arg_count * sizeof(const char*));
    if (!process->argv) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    for (size_t i = 0; i < process->arg_count; i++) {
        size_t arg_size = strlen(argv[i]);
        char *arg = malloc(arg_size + 1);
        if (!arg) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(arg, argv[i]);
        process->argv[i] = arg;
    }

    // find unique envc
    process->env_count = 0;
    for (size_t i = 0; i < elrond->env_count; i++) {
        if (elrond_match(elrond->envs[i].pattern, process->name)) {
            const char *var = elrond->envs[i].var;
            const char *val = elrond_lookup_env(elrond, process->name, var);
            if (!(val >= var && val < var+strlen(var)+1)) {
                continue;
            }
            process->env_count += 1;
        }
    }

    // copy over envp
    process->envp = malloc(process->env_count * sizeof(const char*));
    if (!process->envp) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    size_t j = 0;
    for (size_t i = 0; i < elrond->env_count; i++) {
        if (elrond_match(elrond->envs[i].pattern, process->name)) {
            const char *var = elrond->envs[i].var;
            const char *val = elrond_lookup_env(elrond, process->name, var);
            if (!(val >= var && val < var+strlen(var)+1)) {
                continue;
            }

            process->envp[j] = var;
            j += 1;
        }
    }

    // get cspace_size
    elrond_info_t info;
    bool found = elrond_lookup_info(elrond,
            process->name, "cspace_size", &info);
    if (found) {
        // note all word-sized data is forced inline
        if (info.type != ELROND_TYPE_INLINED
                || info.size != sizeof(size_t)) {
            ZF_LOGE("%s cspace_size is not a size_t?", process->name);
            return -EINVAL;
        }

        process->cspace_size = info.u.word;
    } else {
        process->cspace_size = ELROND_DEFAULT_CSPACE_SIZE;
    }

    // get stack_size
    found = elrond_lookup_info(elrond,
            process->name, "stack_size", &info);
    if (found) {
        // note all word-sized data is forced inline
        if (info.type != ELROND_TYPE_INLINED
                || info.size != sizeof(size_t)) {
            ZF_LOGE("%s stack_size is not a size_t?", process->name);
            return -EINVAL;
        }

        process->stack_size = info.u.word;
    } else {
        process->stack_size = ELROND_DEFAULT_STACK_SIZE;
    }

    // get heap_size
    found = elrond_lookup_info(elrond,
            process->name, "heap_size", &info);
    if (found) {
        // note all word-sized data is forced inline
        if (info.type != ELROND_TYPE_INLINED
                || info.size != sizeof(size_t)) {
            ZF_LOGE("%s heap_size is not a size_t?", process->name);
            return -EINVAL;
        }

        process->heap_size = info.u.word;
    } else {
        process->heap_size = ELROND_DEFAULT_HEAP_SIZE;
    }

    // get priority
    found = elrond_lookup_info(elrond,
            process->name, "priority", &info);
    if (found) {
        // note all word-sized data is forced inline
        if (info.type != ELROND_TYPE_INLINED
                || info.size != sizeof(uint8_t)) {
            ZF_LOGE("%s priority is not a uint8_t?", process->name);
            return -EINVAL;
        }

        process->priority = info.u.word;
    } else {
        process->priority = ELROND_DEFAULT_PRIORITY;
    }

    // get cspace_root
    found = elrond_lookup_info(elrond,
            process->name, "cspace_root", &info);
    if (found) {
        if (info.type != ELROND_TYPE_CAP) {
            ZF_LOGE("%s cspace_root is not a capability?", process->name);
            return -EINVAL;
        }

        process->cspace_root = info.u.cap;
    }

    // get cspace_guard
    found = elrond_lookup_info(elrond,
            process->name, "cspace_guard", &info);
    if (found) {
        // note all word-sized data is forced inline
        if (info.type != ELROND_TYPE_INLINED
                || info.size != sizeof(seL4_Word)) {
            ZF_LOGE("%s cspace_guard is not a word?", process->name);
            return -EINVAL;
        }

        process->cspace_guard = info.u.word;
    } else {
        process->cspace_guard = api_make_guard_skip_word(
                seL4_WordBits - nlog2(process->cspace_size));
    }

    // get asid_pool
    found = elrond_lookup_info(elrond,
            process->name, "asid_pool", &info);
    if (found) {
        if (info.type != ELROND_TYPE_CAP) {
            ZF_LOGE("%s asid_pool is not a capability?", process->name);
            return -EINVAL;
        }

        process->asid_pool = info.u.cap;
    }

    // get vspace_root
    found = elrond_lookup_info(elrond,
            process->name, "vspace_root", &info);
    if (found) {
        if (info.type != ELROND_TYPE_CAP) {
            ZF_LOGE("%s vspace_root is not a capability?", process->name);
            return -EINVAL;
        }

        process->vspace_root = info.u.cap;
    }

    // get fault_ep
    found = elrond_lookup_info(elrond,
            process->name, "fault_ep", &info);
    if (found) {
        if (info.type != ELROND_TYPE_CAP) {
            ZF_LOGE("%s fault_ep is not a capability?", process->name);
            return -EINVAL;
        }

        process->fault_ep = info.u.cap;
    }

    // get main_call_ep
    found = elrond_lookup_info(elrond,
            process->name, "main_call_ep", &info);
    if (found) {
        if (info.type != ELROND_TYPE_CAP) {
            ZF_LOGE("%s main_call_ep is not a capability?", process->name);
            return -EINVAL;
        }

        process->main_call_ep = info.u.cap;
    }

    // get main_ntfn_ep
    found = elrond_lookup_info(elrond,
            process->name, "main_ntfn_ep", &info);
    if (found) {
        if (info.type != ELROND_TYPE_CAP) {
            ZF_LOGE("%s main_ntfn_ep is not a capability?", process->name);
            return -EINVAL;
        }

        process->main_ntfn_ep = info.u.cap;
    }

    // get main_call_count
    found = elrond_lookup_info(elrond,
            process->name, "main_call_count", &info);
    if (found) {
        // note all word-sized data is forced inline
        if (info.type != ELROND_TYPE_INLINED
                || info.size != sizeof(size_t)) {
            ZF_LOGE("%s main_call_count is not a size_t?", process->name);
            return -EINVAL;
        }

        process->main_call_count = info.u.word;
    } else {
        process->main_call_count = 0;
    }

    // get main_ntfn_count
    found = elrond_lookup_info(elrond,
            process->name, "main_ntfn_count", &info);
    if (found) {
        // note all word-sized data is forced inline
        if (info.type != ELROND_TYPE_INLINED
                || info.size != sizeof(size_t)) {
            ZF_LOGE("%s main_ntfn_count is not a size_t?", process->name);
            return -EINVAL;
        }

        process->main_ntfn_count = info.u.word;
    } else {
        process->main_ntfn_count = 0;
    }

    return 0;
}

// setup a process's cspace
static int elrond_process_prepare_cspace(elrond_t *elrond,
        elrond_process_t *process) {
    // create cspace_root if not defined
    if (!process->cspace_root) {
        vka_object_t cspace_root_object;
        int err = vka_alloc_cnode_object(elrond->vka,
                nlog2(process->cspace_size),
                &cspace_root_object);
        if (err) {
            ZF_LOGE("Cound not allocate cspace_root");
            return -ENOMEM;
        }

        process->cspace_root = cspace_root_object.cptr;
    }

    // slot 3: cspace_root
    cspacepath_t src;
    cspacepath_t dst;
    vka_cspace_make_path(elrond->vka, process->cspace_root, &src);
    dst = (cspacepath_t){
        .root = process->cspace_root,
        .capPtr = ELROND_CAP_CSPACE_ROOT,
        .capDepth = nlog2(process->cspace_size),
    };

    int err = vka_cnode_mint(&dst, &src, seL4_AllRights, process->cspace_guard);
    if (err) {
        ZF_LOGE("Cound not mint cspace_root cap");
        return -EACCES;
    }

    // slot 2: ASID pool cap
    vka_cspace_make_path(elrond->vka, elrond->asid_pool, &src);
    dst = (cspacepath_t){
        .root = process->cspace_root,
        .capPtr = ELROND_CAP_ASID_POOL,
        .capDepth = nlog2(process->cspace_size),
    };

    err = vka_cnode_copy(&dst, &src, seL4_AllRights);
    if (err) {
        ZF_LOGE("Cound not copy ASID pool cap");
        return -EACCES;
    }

    // create vspace_root if not defined
    if (!process->vspace_root) {
        vka_object_t vspace_root_object;
        int err = vka_alloc_vspace_root(elrond->vka, &vspace_root_object);
        if (err) {
            ZF_LOGE("Cound not allocate vspace_root");
            return -ENOMEM;
        }

        // assign ASID pool
        err = seL4_ARCH_ASIDPool_Assign(elrond->asid_pool,
                vspace_root_object.cptr);
        if (err) {
            ZF_LOGE("Could not assign ASID pool");
            return -EACCES;
        }

        process->vspace_root = vspace_root_object.cptr;
    }

    // create vspace
    //
    // NOTE we currently just leak our allocations, if we wanted to
    // be able to clean up processs we would need to track allocations
    // through the callback here to be freed later
    err = sel4utils_get_vspace(
            elrond->vspace, &process->vspace, &process->vspace_data,
            elrond->vka, process->vspace_root,
            NULL, NULL);
    if (err) {
        ZF_LOGE("Could not create vspace");
        return -EACCES;
    }

    // slot 4: vspace_root
    vka_cspace_make_path(elrond->vka, process->vspace_root, &src);
    dst = (cspacepath_t){
        .root = process->cspace_root,
        .capPtr = ELROND_CAP_VSPACE_ROOT,
        .capDepth = nlog2(process->cspace_size),
    };

    err = vka_cnode_copy(&dst, &src, seL4_AllRights);
    if (err) {
        ZF_LOGE("Cound not copy page directory cap");
        return -EACCES;
    }

    // slot 5: fault endpoint cap
    if (process->fault_ep) {
        vka_cspace_make_path(elrond->vka, process->fault_ep, &src);
        dst = (cspacepath_t){
            .root = process->cspace_root,
            .capPtr = ELROND_CAP_FAULT_EP,
            .capDepth = nlog2(process->cspace_size),
        };

        err = vka_cnode_copy(&dst, &src, seL4_AllRights);
        if (err) {
            ZF_LOGE("Cound not copy fault endpoint cap");
            return -EACCES;
        }
    }

    // create call endpoint cap
    if (!process->main_call_ep) {
        vka_object_t main_call_ep;
        int err = vka_alloc_endpoint(elrond->vka, &main_call_ep);
        if (err) {
            ZF_LOGE("Could not allocate main call endpoint");
            return -ENOMEM;
        }

        process->main_call_ep = main_call_ep.cptr;
    }

    // slot 6: call endpoint cap
    vka_cspace_make_path(elrond->vka, process->main_call_ep, &src);
    dst = (cspacepath_t){
        .root = process->cspace_root,
        .capPtr = ELROND_CAP_MAIN_CALL_EP,
        .capDepth = nlog2(process->cspace_size),
    };

    err = vka_cnode_copy(&dst, &src, seL4_AllRights);
    if (err) {
        ZF_LOGE("Cound not copy call endpoint cap");
        return -EACCES;
    }

    // create notification endpoint cap
    if (!process->main_ntfn_ep) {
        vka_object_t main_ntfn_ep;
        int err = vka_alloc_notification(elrond->vka, &main_ntfn_ep);
        if (err) {
            ZF_LOGE("Could not allocate main notification endpoint");
            return -ENOMEM;
        }

        process->main_ntfn_ep = main_ntfn_ep.cptr;
    }

    // slot 7: notification endpoint cap
    vka_cspace_make_path(elrond->vka, process->main_ntfn_ep, &src);
    dst = (cspacepath_t){
        .root = process->cspace_root,
        .capPtr = ELROND_CAP_MAIN_NTFN_EP,
        .capDepth = nlog2(process->cspace_size),
    };

    err = vka_cnode_copy(&dst, &src, seL4_AllRights);
    if (err) {
        ZF_LOGE("Cound not copy notification endpoint cap");
        return -EACCES;
    }

    process->cap_count = ELROND_RESERVED_CAP_COUNT;
    return 0;
}


// load a process's initial vspace from a given elf file
static int elrond_process_load(elrond_t *elrond,
        elrond_process_t *process,
        const void *elf,
        size_t elf_size) {
    // load the elf file
    int err = elf_newFile(elf, elf_size, &process->elf);
    if (err) {
        ZF_LOGE("Corrupted elf file");
        return -ENOEXEC;
    }

    process->entry_point = (uintptr_t)sel4utils_elf_load(
            &process->vspace, elrond->vspace, elrond->vka, elrond->vka,
            &process->elf);
    if (!process->entry_point) {
        ZF_LOGE("Could not load elf");
        return -ENOMEM;
    }

    process->sysinfo = sel4utils_elf_get_vsyscall(&process->elf);
    process->elf_phdr_count = sel4utils_elf_num_phdrs(&process->elf);
    process->elf_phdrs = malloc(process->elf_phdr_count * sizeof(Elf_Phdr));
    if (!process->elf_phdrs) {
        ZF_LOGE("Could not allocate elf phdr array");
        return -ENOMEM;
    }
    sel4utils_elf_read_phdrs(&process->elf,
            process->elf_phdr_count, process->elf_phdrs);

    // create a heap region
    if (process->heap_size > 0) {
        process->heap = (uintptr_t)vspace_new_pages(
                &process->vspace, seL4_AllRights,
                alignup(process->heap_size, PAGE_SIZE_4K) / PAGE_SIZE_4K,
                PAGE_BITS_4K);
        if (!process->heap) {
            ZF_LOGE("Could not allocate heap");
            return -ENOMEM;
        }
    }

    // create a thread, after elf-loading to avoid clobbering
    sel4utils_thread_config_t thread_config = {
        .cspace = process->cspace_root,
        .cspace_root_data = process->cspace_guard,
        .fault_endpoint = process->fault_ep ? ELROND_CAP_FAULT_EP : 0,
        .create_reply = true,
        .custom_stack_size = true,
        .stack_size = BYTES_TO_4K_PAGES(process->stack_size),
        .sched_params.auth = elrond->tcb,
        .sched_params.priority = process->priority,
    };
    err = sel4utils_configure_thread_config(
            elrond->vka,
            elrond->vspace,
            &process->vspace,
            thread_config,
            &process->main_thread);
    if (err) {
        ZF_LOGE("Could not create main_thread");
        return -ENOMEM;
    }

    #ifdef CONFIG_DEBUG_BUILD
    seL4_DebugNameThread(process->main_thread.tcb.cptr, process->name);
    #endif

    // go ahead and bind the notification endpoint to the process's TCB,
    // the process can always unbind this if it needs to
    err = seL4_TCB_BindNotification(
            process->main_thread.tcb.cptr,
            process->main_ntfn_ep);
    if (err) {
        ZF_LOGE("Cound not bind notification to TCB");
        return -EACCES;
    }

    // slot 1: TCB cap
    cspacepath_t src;
    cspacepath_t dst;
    vka_cspace_make_path(elrond->vka, process->main_thread.tcb.cptr, &src);
    dst = (cspacepath_t){
        .root = process->cspace_root,
        .capPtr = ELROND_CAP_MAIN_TCB,
        .capDepth = nlog2(process->cspace_size),
    };

    err = vka_cnode_copy(&dst, &src, seL4_AllRights);
    if (err) {
        ZF_LOGE("Cound not copy TCB cap");
        return -EACCES;
    }

    return 0;
}

// prepare extra info
extern int elrond_process_prepare_shared(
        elrond_t *elrond, elrond_process_t *process,
        elrond_shared_t *shared,
        uintptr_t *vaddr);

static int elrond_process_prepare_info(elrond_t *elrond,
        elrond_process_t *process) {
    size_t section_count = elf_getNumSections(&process->elf);

    // find infos and deduplicate
    elrond_info_t *infos = NULL;
    size_t info_count = 0;
    size_t info_capacity = 0;
    for (size_t i = 0; i < section_count; i++) {
        // does prefix match?
        const char *section_name = elf_getSectionName(&process->elf, i);
        size_t section_name_len = strlen(section_name);
        if (section_name_len < ELROND_INFO_PREFIX_LEN
                || memcmp(section_name,
                    ELROND_INFO_PREFIX,
                    ELROND_INFO_PREFIX_LEN) != 0) {
            continue;
        }
        const char *section_suffix = section_name + ELROND_INFO_PREFIX_LEN;
        size_t section_suffix_len = section_name_len - ELROND_INFO_PREFIX_LEN;

        // have we already seen this info?
        //
        // In theory there should only be a single section for each info in
        // the elf, but elfs do allow multiple sections with the same name so
        // we can't completely rely on this.
        for (size_t j = 0; j < info_count; j++) {
            if (strcmp(infos[j].name, section_suffix) == 0) {
                continue;
            }
        }

        // do we have requested info?
        //
        // note we have a number of implicit infos to try if we can't find
        // the info in the user provided overrides
        elrond_info_t info;
        bool found = elrond_lookup_info(elrond,
                process->name, section_suffix, &info);
        if (found) {
            // use this info
        } else if (strcmp(section_suffix, "cspace_size") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->cspace_size;
            info.size = sizeof(size_t);
        } else if (strcmp(section_suffix, "stack_size") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->stack_size;
            info.size = sizeof(size_t);
        } else if (strcmp(section_suffix, "heap_size") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->heap_size;
            info.size = sizeof(size_t);
        } else if (strcmp(section_suffix, "priority") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->priority;
            info.size = sizeof(uint8_t);
        } else if (strcmp(section_suffix, "main_tcb") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = ELROND_CAP_MAIN_TCB;
            info.size = sizeof(seL4_CPtr);
        } else if (strcmp(section_suffix, "asid_pool") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = ELROND_CAP_ASID_POOL;
            info.size = sizeof(seL4_CPtr);
        } else if (strcmp(section_suffix, "cspace_root") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = ELROND_CAP_CSPACE_ROOT;
            info.size = sizeof(seL4_CPtr);
        } else if (strcmp(section_suffix, "vspace_root") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = ELROND_CAP_VSPACE_ROOT;
            info.size = sizeof(seL4_CPtr);
        } else if (strcmp(section_suffix, "fault_ep") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->fault_ep
                    ? ELROND_CAP_FAULT_EP
                    : seL4_CapNull;
            info.size = sizeof(seL4_CPtr);
        } else if (strcmp(section_suffix, "main_call_ep") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = ELROND_CAP_MAIN_CALL_EP;
            info.size = sizeof(seL4_CPtr);
        } else if (strcmp(section_suffix, "main_ntfn_ep") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = ELROND_CAP_MAIN_NTFN_EP;
            info.size = sizeof(seL4_CPtr);
        } else if (strcmp(section_suffix, "main_call_count") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->main_call_count;
            info.size = sizeof(size_t);
        } else if (strcmp(section_suffix, "main_ntfn_count") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->main_ntfn_count;
            info.size = sizeof(size_t);
        } else if (strcmp(section_suffix, "cspace_guard") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->cspace_guard;
            info.size = sizeof(size_t);
        } else if (strcmp(section_suffix, "sysinfo") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->sysinfo;
            info.size = sizeof(uintptr_t);
        } else if (strcmp(section_suffix, "ipc_buffer") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = (uintptr_t)process->main_thread.ipc_buffer_addr;
            info.size = sizeof(uintptr_t);
        } else if (strcmp(section_suffix, "heap") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_INLINED;
            info.u.word = process->heap;
            info.size = sizeof(uintptr_t);
        } else if (strcmp(section_suffix, "initial_cap_count") == 0) {
            info.name = section_suffix;
            info.type = ELROND_TYPE_CAP_COUNT;
            info.size = sizeof(size_t);
        } else {
            // not found? just move on, if the info was in the bss
            // section it should be zeros now.
            continue;
        }

        elrond_info_t *x = elrond_append(
                (void**)&infos,
                sizeof(elrond_info_t),
                &info_count,
                &info_capacity);
        if (!x) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        *x = info;
    }

    // resolve caps/calls/notifications/mappings
    for (size_t i = 0; i < info_count; i++) {
        if (infos[i].type == ELROND_TYPE_CAP) {
            if (process->cap_count+1 > process->cspace_size) {
                ZF_LOGE("Out of caps");
                return -ENOMEM;
            }

            // copy over cap, note that seL4_CapNull is seL4_CapNull
            seL4_CPtr cptr = seL4_CapNull;
            if (infos[i].u.cap) {
                cspacepath_t src;
                cspacepath_t dst;
                vka_cspace_make_path(elrond->vka, infos[i].u.cap, &src);
                dst = (cspacepath_t){
                    .root = process->cspace_root,
                    .capPtr = process->cap_count,
                    .capDepth = nlog2(process->cspace_size),
                };
                int err = vka_cnode_copy(&dst, &src, seL4_AllRights);
                if (err) {
                    ZF_LOGE("Could not copy cap %s", infos[i].name);
                    return err;
                }
                cptr = process->cap_count;
                process->cap_count += 1;
            }

            infos[i].type = ELROND_TYPE_INLINED;
            infos[i].u.word = cptr;
            infos[i].size = sizeof(seL4_CPtr);
        } else if (infos[i].type == ELROND_TYPE_BADGED) {
            if (process->cap_count+1 > process->cspace_size) {
                ZF_LOGE("Out of caps");
                return -ENOMEM;
            }

            // mint cap with badge, note that seL4_CapNull is seL4_CapNull
            seL4_CPtr cptr = seL4_CapNull;
            if (infos[i].u.badged.cap) {
                cspacepath_t src;
                cspacepath_t dst;
                vka_cspace_make_path(elrond->vka, infos[i].u.badged.cap, &src);
                dst = (cspacepath_t){
                    .root = process->cspace_root,
                    .capPtr = process->cap_count,
                    .capDepth = nlog2(process->cspace_size),
                };
                int err = vka_cnode_mint(&dst, &src,
                        seL4_NoRead, infos[i].u.badged.badge);
                if (err) {
                    ZF_LOGE("Could not mint badged cap %s", infos[i].name);
                    return err;
                }
                cptr = process->cap_count;
                process->cap_count += 1;
            }

            infos[i].type = ELROND_TYPE_INLINED;
            infos[i].u.word = cptr;
            infos[i].size = sizeof(seL4_CPtr);
        } else if (infos[i].type == ELROND_TYPE_SHARED) {
            uintptr_t vaddr;
            seL4_CPtr pages;
            int err = elrond_process_prepare_shared(elrond, process,
                    infos[i].u.shared,
                    &vaddr);
            if (err) {
                ZF_LOGE("Could not map shared %s", infos[i].name);
                return err;
            }

            infos[i].type = ELROND_TYPE_INLINED;
            infos[i].u.word = vaddr;
            infos[i].size = sizeof(uintptr_t);
        }
    }

    // resolve dependent constants
    for (size_t i = 0; i < info_count; i++) {
        if (infos[i].type == ELROND_TYPE_CAP_COUNT) {
            infos[i].type = ELROND_TYPE_INLINED;
            infos[i].u.word = process->cap_count;
            infos[i].size = sizeof(size_t);
        }
    }

    // write to vspace
    for (size_t i = 0; i < section_count; i++) {
        // does prefix match?
        const char *section_name = elf_getSectionName(&process->elf, i);
        if (memcmp(section_name,
                ELROND_INFO_PREFIX,
                ELROND_INFO_PREFIX_LEN) != 0) {
            continue;
        }
        const char *section_suffix = section_name + ELROND_INFO_PREFIX_LEN;

        // get info
        elrond_info_t *info;
        for (size_t j = 0; j < info_count; j++) {
            if (strcmp(infos[j].name, section_suffix) == 0) {
                info = &infos[j];
                goto found;
            }
        }

        // this happens twice if we don't have info, see above
        continue;

        found:;
        // we must have fixed any dependent infos here
        assert(info->type == ELROND_TYPE_DATA
                || info->type == ELROND_TYPE_INLINED);

        const uintptr_t *vaddrs = elf_getSection(&process->elf, i);
        size_t vaddr_count = elf_getSectionSize(&process->elf, i)
                / sizeof(uintptr_t);
        for (size_t j = 0; j < vaddr_count; j++) {
            int err = elrond_process_write(elrond, process,
                    vaddrs[j],
                    (info->type == ELROND_TYPE_DATA
                        ? info->u.data
                        : info->u.bytes),
                    info->size);
            if (err) {
                ZF_LOGE("Could not write info %s to vaddr %lu size %lu",
                        section_name, vaddrs[j], info->size);
                return err;
            }
        }
    }

    process->infos = infos;
    process->info_count = info_count;
    return 0;
}

// prepare the process's stack with argv/envp/auxv
static int elrond_process_prepare_stack(elrond_t *elrond,
        elrond_process_t *process,
        uintptr_t *sp) {
    // setup stack
    uintptr_t sp_ = (uintptr_t)process->main_thread.stack_top
            - sizeof(seL4_Word);

    // copy over elf headers
    int err = elrond_process_push(elrond, process, &sp_,
            process->elf_phdrs, process->elf_phdr_count*sizeof(Elf_Phdr));
    if (err) {
        return err;
    }
    uintptr_t at_phdr = sp_;

    // get auxv
    int auxc = 7;
    Elf_auxv_t auxv[7];
    auxv[0].a_type = AT_PAGESZ;
    auxv[0].a_un.a_val = PAGE_SIZE_4K;
    auxv[1].a_type = AT_PHDR;
    auxv[1].a_un.a_val = at_phdr;
    auxv[2].a_type = AT_PHNUM;
    auxv[2].a_un.a_val = process->elf_phdr_count;
    auxv[3].a_type = AT_PHENT;
    auxv[3].a_un.a_val = sizeof(Elf_Phdr);
    auxv[4].a_type = AT_SEL4_IPC_BUFFER_PTR;
    auxv[4].a_un.a_val = (uintptr_t)process->main_thread.ipc_buffer_addr;
    auxv[5].a_type = AT_SEL4_TCB;
    auxv[5].a_un.a_val = ELROND_CAP_MAIN_TCB;
    auxv[6].a_type = AT_SYSINFO;
    auxv[6].a_un.a_val = process->sysinfo;

    // copy over env
    if ((int)process->env_count != process->env_count) {
        ZF_LOGE("envc overflow");
        return -EOVERFLOW;
    }

    size_t envc = process->env_count;
    uintptr_t envp_sp = sp_;
    for (int i = 0; i < envc; i++) {
        int err = elrond_process_push(elrond, process, &sp_,
                (char*)process->envp[envc-1-i],
                strlen(process->envp[envc-1-i]) + 1);
        if (err) {
            ZF_LOGE("Could not push envp");
            return err;
        }
        sp_ = aligndown(sp_, 4);
    }

    // copy over args
    if ((int)process->arg_count != process->arg_count) {
        ZF_LOGE("argc overflow");
        return -EOVERFLOW;
    }

    size_t argc = process->arg_count;
    uintptr_t argv_sp = sp_;
    for (int i = 0; i < argc; i++) {
        int err = elrond_process_push(elrond, process, &sp_,
                (char*)process->argv[argc-1-i],
                strlen(process->argv[argc-1-i]) + 1);
        if (err) {
            ZF_LOGE("Could not push argv");
            return err;
        }
        sp_ = aligndown(sp_, 4);
    }

    // adjust stack pointer, making sure it's aligned to a double word boundary
    size_t to_push = 5*sizeof(seL4_Word)    // constants
            + auxc*sizeof(Elf_auxv_t)       // aux
            + envc*sizeof(char*)            // env
            + argc*sizeof(char*);           // args
    uintptr_t hypothetical_sp = sp_ - to_push;
    sp_ = sp_ - (hypothetical_sp - ALIGN_DOWN(
            hypothetical_sp, STACK_CALL_ALIGNMENT));

    // initial stack frame
    //
    // keep in mind these are pushed onto the stack "backwards"!

    // push auxv
    err = elrond_process_push(elrond, process, &sp_,
            &(Elf_auxv_t){0}, sizeof(Elf_auxv_t));
    if (err) {
        ZF_LOGE("Could not push auxv");
        return err;
    }
    err = elrond_process_push(elrond, process, &sp_,
            auxv, auxc*sizeof(Elf_auxv_t));
    if (err) {
        ZF_LOGE("Could not push auxv");
        return err;
    }

    // push envc
    err = elrond_process_push(elrond, process, &sp_,
            &(uintptr_t){0}, sizeof(uintptr_t));
    if (err) {
        ZF_LOGE("Could not push envp");
        return err;
    }
    for (size_t i = 0; i < envc; i++) {
        envp_sp -= strlen(process->envp[envc-1-i]) + 1;
        err = elrond_process_push(elrond, process, &sp_,
                &envp_sp, sizeof(uintptr_t));
        if (err) {
            ZF_LOGE("Could not push envp");
            return err;
        }
        envp_sp = aligndown(envp_sp, 4);
    }

    // push argv and argc
    err = elrond_process_push(elrond, process, &sp_,
            &(uintptr_t){0}, sizeof(char*));
    if (err) {
        ZF_LOGE("Could not push argv");
        return err;
    }
    for (size_t i = 0; i < argc; i++) {
        argv_sp -= strlen(process->argv[argc-1-i]) + 1;
        err = elrond_process_push(elrond, process, &sp_,
                &argv_sp, sizeof(uintptr_t));
        if (err) {
            ZF_LOGE("Could not push argv");
            return err;
        }
        argv_sp = aligndown(argv_sp, 4);
    }
    err = elrond_process_push(elrond, process, &sp_,
            &argc, sizeof(size_t));
    if (err) {
        ZF_LOGE("Could not push argc");
        return err;
    }

    *sp = sp_;
    return 0;
}

// spawn a process
int elrond_process_spawnv(elrond_t *elrond, elrond_process_t *process,
        const char *name, const void *elf, size_t elf_size,
        const char **argv) {
    if (!elf) {
        ZF_LOGE("\"%s\" elf not found?", name);
        return -ENOENT;
    }

    // keep track of processes
    char *name_ = "";
    if (name) {
        name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);
    }

    elrond_process_t **process_ = elrond_append(
            (void**)&elrond->processs,
            sizeof(elrond_process_t*),
            &elrond->process_count,
            &elrond->process_capacity);
    if (!process_) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }
    *process_ = process;

    memset(process, 0, sizeof(elrond_process_t));
    process->name = name_;

    // lookup config
    int err = elrond_process_configure(elrond, process, argv);
    if (err) {
        ZF_LOGE("Could not configure process %s", name);
        return err;
    }

    // prepare cspace
    err = elrond_process_prepare_cspace(elrond, process);
    if (err) {
        ZF_LOGE("Could not prepare cspace %s", name);
        return err;
    }

    // load elf
    err = elrond_process_load(elrond, process, elf, elf_size);
    if (err) {
        ZF_LOGE("Could not load process %s", name);
        return err;
    }

    // prepare custom info sections
    err = elrond_process_prepare_info(elrond, process);
    if (err) {
        ZF_LOGE("Cound not prepare info %s", name);
        return err;
    }

    // prepare stack with argv/envp/auxv
    uintptr_t sp;
    err = elrond_process_prepare_stack(elrond, process, &sp);
    if (err) {
        ZF_LOGE("Cound not prepare stack %s", name);
        return err;
    }

    // create initial context
    seL4_UserContext context = {0};
    err = sel4utils_arch_init_context(
            (void*)process->entry_point, (void*)sp, &context);
    if (err) {
        return -EACCES;
    }
    process->main_thread.initial_stack_pointer = (void*)sp;

    // write registers and resume thread
    err = seL4_TCB_WriteRegisters(process->main_thread.tcb.cptr, true,
            0, sizeof(context) / sizeof(seL4_Word), &context);
    if (err) {
        ZF_LOGE("Could not resume process %s", name);
        return -EACCES;
    }

    return 0;
}

int elrond_process_spawn(elrond_t *elrond, elrond_process_t *process,
        const char *name, const void *elf, size_t elf_size) {
    return elrond_process_spawnv(elrond, process, name, elf, elf_size,
            (const char*[]){name, NULL});
}

int elrond_process_spawnl(elrond_t *elrond, elrond_process_t *process,
        const char *name, const void *elf, size_t elf_size,
        const char *arg, ...) {
    size_t argc = 1;
    va_list args;
    va_start(args, arg);
    while (va_arg(args, const char*)) {
        argc += 1;
    }
    va_end(args);

    const char *argv[argc+1];
    va_start(args, arg);
    argv[0] = arg;
    for (size_t i = 1; i < argc; i++) {
        argv[i] = va_arg(args, const char*);
    }
    argv[argc] = NULL;
    va_end(args);

    return elrond_process_spawnv(elrond, process, name, elf, elf_size, argv);
}

int elrond_spawnv(elrond_t *elrond,
        const char *name, const void *elf, size_t elf_size,
        const char **argv) {
    // allocate process, NOTE if we wanted to clean up memory we would need
    // to save this somewhere
    elrond_process_t *process = malloc(sizeof(elrond_process_t));
    if (!process) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    return elrond_process_spawnv(elrond, process, name, elf, elf_size, argv);
}

int elrond_spawn(elrond_t *elrond,
        const char *name, const void *elf, size_t elf_size) {
    return elrond_spawnv(elrond, name, elf, elf_size,
            (const char*[]){name, NULL});
}

int elrond_spawnl(elrond_t *elrond,
        const char *name, const void *elf, size_t elf_size,
        const char *arg, ...) {
    size_t argc = 1;
    va_list args;
    va_start(args, arg);
    while (va_arg(args, const char*)) {
        argc += 1;
    }
    va_end(args);

    const char *argv[argc+1];
    va_start(args, arg);
    argv[0] = arg;
    for (size_t i = 1; i < argc; i++) {
        argv[i] = va_arg(args, const char*);
    }
    argv[argc] = NULL;
    va_end(args);

    return elrond_spawnv(elrond, name, elf, elf_size, argv);
}



// this is just for debugging
void elrond_debugdump(elrond_t *elrond, const char *indent) {
    indent = indent ? indent : "";
    char new_indent[4+strlen(indent)+1];
    sprintf(new_indent, "    %s", indent);
    printf("%s<elrond %p>:\n", indent, elrond);
    printf("%s  env:\n", indent);
    for (size_t i = 0; i < elrond->env_count; i++) {
        printf("%s    <env %s \"%s\">\n", indent,
                elrond->envs[i].var,
                elrond->envs[i].pattern);
    }
    printf("%s  infos:\n", indent);
    for (size_t i = 0; i < elrond->info_count; i++) {
        if (elrond->infos[i].type != ELROND_TYPE_INLINED
                && elrond->infos[i].type != ELROND_TYPE_DATA) {
            continue;
        }

        printf("%s    <info %s=", indent, elrond->infos[i].name);
        size_t k = 0;
        for (size_t k = 0; k < elrond->infos[i].size && k < 16; k++) {
            printf("%c%02x",
                    (k == 0 ? '[' : ' '),
                    (elrond->infos[i].type == ELROND_TYPE_DATA
                        ? elrond->infos[i].u.data
                        : elrond->infos[i].u.bytes)[k]);
        }
        if (elrond->infos[i].size > 16) {
            printf("...");
        }
        printf("] \"%s\">\n", elrond->infos[i].pattern);
    }
    printf("%s  caps:\n", indent);
    for (size_t i = 0; i < elrond->info_count; i++) {
        if (elrond->infos[i].type != ELROND_TYPE_CAP) {
            continue;
        }

        printf("%s    <cap %s %ld \"%s\">\n", indent,
                elrond->infos[i].name,
                elrond->infos[i].u.cap,
                elrond->infos[i].pattern);
    }
    printf("%s  badged:\n", indent);
    for (size_t i = 0; i < elrond->info_count; i++) {
        if (elrond->infos[i].type != ELROND_TYPE_BADGED) {
            continue;
        }

        printf("%s    <badged %s %ld 0x%016lx \"%s\">\n", indent,
                elrond->infos[i].name,
                elrond->infos[i].u.badged.cap,
                elrond->infos[i].u.badged.badge,
                elrond->infos[i].pattern);
    }
    printf("%s  shared:\n", indent);
    for (size_t i = 0; i < elrond->info_count; i++) {
        if (elrond->infos[i].type != ELROND_TYPE_SHARED) {
            continue;
        }

        printf("%s    <shared %s %lu %lux%lu %s%s%s \"%s\">\n", indent,
                elrond->infos[i].name,
                elrond->infos[i].u.shared  
                    ? elrond->infos[i].u.shared->size
                    : 0,
                elrond->infos[i].u.shared
                    ?  elrond->infos[i].u.shared->page_count
                    : 0,
                elrond->infos[i].u.shared
                    ? elrond->infos[i].u.shared->page_size
                    : 0,
                elrond->infos[i].u.shared
                    && seL4_CapRights_get_capAllowRead(
                        elrond->infos[i].u.shared->rights)
                    ? "r" : "",
                elrond->infos[i].u.shared
                    && seL4_CapRights_get_capAllowWrite(
                        elrond->infos[i].u.shared->rights)
                    ? "w" : "",
                elrond->infos[i].u.shared
                    && elrond->infos[i].u.shared->cacheable
                    ? "c" : "",
                elrond->infos[i].pattern);
    }
    printf("%s  eps:\n", indent);
    for (size_t i = 0; i < elrond->info_count; i++) {
        if (elrond->infos[i].type != ELROND_TYPE_EP) {
            continue;
        }

        printf("%s    <ep %s %ld (%ld) %ld (%ld) \"%s\">%s\n", indent,
                elrond->infos[i].name,
                elrond->infos[i].u.ep->call_ep,
                elrond->infos[i].u.ep->call_count,
                elrond->infos[i].u.ep->ntfn_ep,
                elrond->infos[i].u.ep->ntfn_count,
                elrond->infos[i].pattern,
                (elrond->infos[i].u.ep->call_count > 0
                    || elrond->infos[i].u.ep->ntfn_count > 0)
                    ? ":"
                    : "");
        if (elrond->infos[i].u.ep->call_count > 0) {
            printf("%s      calls:\n", indent);
            for (size_t j = 0; j < elrond->infos[i].u.ep->call_count; j++) {
                printf("%s        <call %s 0x%016lx>\n", indent,
                        elrond->infos[i].u.ep->calls[j],
                        ELROND_CALL_BADGE(j));
            }
        }
        if (elrond->infos[i].u.ep->ntfn_count > 0) {
            printf("%s      ntfns:\n", indent);
            for (size_t j = 0; j < elrond->infos[i].u.ep->ntfn_count; j++) {
                printf("%s        <ntfn %s 0x%016lx>\n", indent,
                        elrond->infos[i].u.ep->ntfns[j],
                        ELROND_NTFN_BADGE(j));
            }
        }
    }
    printf("%s  processes:\n", indent);
    for (size_t i = 0; i < elrond->process_count; i++) {
        elrond_process_debugdump(elrond, elrond->processs[i], new_indent);
    }
}

void elrond_process_debugdump(elrond_t *elrond, elrond_process_t *process,
        const char *indent) {
    indent = indent ? indent : "";
    printf("%s<process %p %s %lu>:\n", indent,
            process,
            process->name,
            process->elf.elfSize);
    printf("%s  entry_point: %p\n", indent, (void*)process->entry_point);
    printf("%s  sysinfo: %p\n", indent, (void*)process->sysinfo);
    printf("%s  cspace_size: %lu\n", indent, process->cspace_size);
    printf("%s  stack_size: %lu\n", indent, process->stack_size);
    printf("%s  heap_size: %lu\n", indent, process->heap_size);
    printf("%s  priority: %hhu\n", indent, process->priority);
    printf("%s  argv:\n", indent);
    for (size_t j = 0; j < process->arg_count; j++) {
        printf("%s    %s\n", indent, process->argv[j]);
    }
    printf("%s  envp:\n", indent);
    for (size_t j = 0; j < process->env_count; j++) {
        printf("%s    %s\n", indent, process->envp[j]);
    }
    printf("%s  info:\n", indent);
    for (size_t j = 0; j < process->info_count; j++) {
        if (process->infos[j].type != ELROND_TYPE_INLINED
                && process->infos[j].type != ELROND_TYPE_DATA) {
            printf("%s    %s=(malformed info)\n", indent,
                    process->infos[j].name);
            continue;
        }

        printf("%s    %s=", indent, process->infos[j].name);
        size_t k = 0;
        for (size_t k = 0;
                k < process->infos[j].size && k < 16;
                k++) {
            printf("%c%02x",
                    (k == 0 ? '[' : ' '),
                    (process->infos[j].type == ELROND_TYPE_DATA
                        ? process->infos[j].u.data
                        : process->infos[j].u.bytes)[k]);
        }
        if (process->infos[j].size > 16) {
            printf("...");
        }
        printf("]\n");
    }
}
