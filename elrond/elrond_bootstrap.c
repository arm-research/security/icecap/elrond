//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include "elrond.h"

#ifdef CONFIG_ELROND_BOOTSTRAP

#include <sel4platsupport/bootinfo.h>
#include <simple/simple.h>
#include <simple-default/simple-default.h>
#include <allocman/allocman.h>
#include <allocman/bootstrap.h>
#include <allocman/vka.h>


// various memory pools needed for bootstrapping
static uint8_t elrond_bootstrap_static_pool[CONFIG_ELROND_BOOTSTRAP_STATIC_POOL_SIZE];
__attribute__((aligned(PAGE_SIZE_4K)))
static uint8_t elrond_bootstrap_morecore_pool[CONFIG_ELROND_BOOTSTRAP_HEAP_SIZE];

// other bootstrap state
static vka_t elrond_bootstrap_vka;
static vspace_t elrond_bootstrap_vspace;
static sel4utils_alloc_data_t elrond_bootstrap_vspace_data;

// morecore variables defined over in libsel4muslcsys
extern uint8_t *morecore_area;
extern size_t morecore_size;


// bootstrap the root task of a system and create an elrond instance
//
// note that vka and vspace allocators can be accessed through elrond_vka
// and elrond_vspace
int elrond_bootstrap(elrond_t *elrond, const char *root_name) {
    // name the root task's thread
    #ifdef CONFIG_DEBUG_BUILD
    seL4_DebugNameThread(seL4_CapInitThreadTCB, root_name);
    #endif

    // get kernel-provided bootinfo
    seL4_BootInfo *bootinfo = platsupport_get_bootinfo();
    if (!bootinfo) {
        ZF_LOGE("Could not get bootinfo");
        return -EFAULT;
    }
    simple_t simple;
    simple_default_init_bootinfo(&simple, bootinfo);

    #ifdef CONFIG_ELROND_BOOTSTRAP_DEBUG
    simple_print(&simple);
    #endif

    // create a one-time static allocator
    allocman_t *allocman = bootstrap_use_current_simple(
            &simple,
            CONFIG_ELROND_BOOTSTRAP_STATIC_POOL_SIZE,
            elrond_bootstrap_static_pool);
    if (!allocman) {
        ZF_LOGE("Could not bootstrap static memory pool");
        return -EFAULT;
    }
    allocman_make_vka(&elrond_bootstrap_vka, allocman);

    // create the root task's vspace
    int err = sel4utils_bootstrap_vspace_with_bootinfo_leaky(
            &elrond_bootstrap_vspace,
            &elrond_bootstrap_vspace_data,
            simple_get_pd(&simple),
            &elrond_bootstrap_vka,
            bootinfo);
    if (err) {
        ZF_LOGE("Could not bootstrap vspace");
        return -EFAULT;
    }

    // setup libsel4muslcsys's morecore area, this is what backs musl's malloc
    //
    // suprisingly this _is_ needed by vspace_reserve_range, I'm increasingly
    // unsure of why we need the bootstrap virtual pool at all...
    morecore_area = elrond_bootstrap_morecore_pool;
    morecore_size = CONFIG_ELROND_BOOTSTRAP_HEAP_SIZE;

    // fill allocman with virtual memory
    void *virtual_pool;
    reservation_t resv =  vspace_reserve_range(
            &elrond_bootstrap_vspace,
            CONFIG_ELROND_BOOTSTRAP_VIRTUAL_POOL_SIZE,
            seL4_AllRights, true,
            &virtual_pool);
    if (!resv.res) {
        ZF_LOGE("Could not reserve virtual memory pool (%lu bytes)\n",
                (size_t)CONFIG_ELROND_BOOTSTRAP_VIRTUAL_POOL_SIZE);
        return -EFAULT;
    }

    bootstrap_configure_virtual_pool(
            allocman,
            virtual_pool,
            CONFIG_ELROND_BOOTSTRAP_VIRTUAL_POOL_SIZE,
            simple_get_pd(&simple));

    // create elrond instance
    return elrond_create(elrond,
            &elrond_bootstrap_vka,
            &elrond_bootstrap_vspace,
            seL4_CapInitThreadTCB,
            seL4_CapInitThreadASIDPool);
}







#endif
