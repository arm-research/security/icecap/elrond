//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <elrond.h>
#include <vka/capops.h>


// defined in elrond.c
extern void *elrond_append(
        void **entries,
        size_t entry_size,
        size_t *entry_count,
        size_t *entry_capacity);


// create an endpoint from badged capabilities
//
// any calls/notifications created on this endpoint will redirect
// to the badged capabilities
int elrond_ep_frombadged(elrond_ep_t *ep,
        seL4_CPtr call_ep,
        seL4_CPtr ntfn_ep) {
    // save endpoint state
    memset(ep, 0, sizeof(elrond_ep_t));
    ep->call_ep = call_ep;
    ep->ntfn_ep = ntfn_ep;
    ep->call_count = -1;
    ep->ntfn_count = -1;
    return 0;
}

// create an empty endpoint
int elrond_add_ep(elrond_t *elrond, const char *pattern,
        const char *name, elrond_ep_t **ep) {
    // allocate endpoint
    elrond_ep_t *ep_ = malloc(sizeof(elrond_ep_t));
    if (!ep_) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }
    memset(ep_, 0, sizeof(elrond_ep_t));

    // create call/notification endpoint pair
    vka_object_t call_ep;
    int err = vka_alloc_endpoint(elrond->vka, &call_ep);
    if (err) {
        ZF_LOGE("Could not allocate call endpoint");
        return -ENOMEM;
    }

    vka_object_t ntfn_ep;
    err = vka_alloc_notification(elrond->vka, &ntfn_ep);
    if (err) {
        ZF_LOGE("Could not allocate notification endpoint");
        return -ENOMEM;
    }

    // save endpoint state
    ep_->call_ep = call_ep.cptr;
    ep_->ntfn_ep = ntfn_ep.cptr;
    ep_->call_count = 0;
    ep_->ntfn_count = 0;

    // add to info
    char *name_ = "";
    if (name) {
        name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);
    }

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = sizeof(seL4_CPtr);
    x->type = ELROND_TYPE_EP;
    x->u.ep = ep_;

    // save for root task
    if (ep) {
        *ep = ep_;
    }

    return 0;
}


// null endpoint
static const elrond_ep_t elrond_ep_null = {
    .call_ep = seL4_CapNull,
    .call_count = -1,
    .ntfn_ep = seL4_CapNull,
    .ntfn_count = -1,
};

// add endpoints or calls/notifications/wakeups on a given endpoint
//
// - if ep is NULL, the resulting endpoints will be seL4_CapNull
// - if cap is not NULL, the resulting badged cap will be minted
//   into the root process's cspace and returned here
int elrond_add_call(elrond_t *elrond, const char *pattern,
        const char *name, elrond_ep_t *ep, seL4_CPtr *cap) {
    name = name ? name : "";
    ep = ep ? ep : (elrond_ep_t*)&elrond_ep_null;

    // endpoint already badged?
    seL4_Word badge_ = 0;
    if (ep->call_count >= 0) {
        // endpoint already has matching badge?
        for (size_t i = 0; i < ep->call_count; i++) {
            if (strcmp(ep->calls[i], name) == 0) {
                badge_ = ELROND_CALL_BADGE(i);
                goto found;
            }
        }

        // allocate call badge
        if (ep->call_count >= ELROND_CALL_MAX) {
            ZF_LOGE("Too many calls on endpoint (%lu/%lu)",
                    ep->call_count, ELROND_CALL_MAX);
            return -EOVERFLOW;
        }

        char *name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);

        const char **x = elrond_append(
                (void**)&ep->calls,
                sizeof(const char*),
                &ep->call_count,
                &ep->call_capacity);
        if (!x) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        *x = name_;

        badge_ = ELROND_CALL_BADGE(ep->call_count-1);
    }

    found:;
    // add to info
    char *name_ = malloc(strlen(name)+1);
    if (!name_) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }
    strcpy(name_, name);

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = sizeof(seL4_CPtr);
    // either already badged or needs to be badged
    if (!badge_) {
        x->type = ELROND_TYPE_CAP;
        x->u.cap = ep->call_ep;
    } else {
        x->type = ELROND_TYPE_BADGED;
        x->u.badged.cap = ep->call_ep;
        x->u.badged.badge = badge_;
    }

    // mint into root task?
    if (cap) {
        if (!badge_) {
            *cap = ep->ntfn_ep;
        } else {
            cspacepath_t src;
            cspacepath_t dst;
            vka_cspace_make_path(elrond->vka, ep->call_ep, &src);

            int err = vka_cspace_alloc_path(elrond->vka, &dst);
            if (err) {
                ZF_LOGE("Could not allocate call cap");
                return -ENOMEM;
            }

            err = vka_cnode_mint(&dst, &src, seL4_NoRead, badge_);
            if (err) {
                ZF_LOGE("Could not mint call cap");
                return -EACCES;
            }

            *cap = dst.capPtr;
        }
    }

    return 0;
}

int elrond_add_ntfn(elrond_t *elrond, const char *pattern,
        const char *name, elrond_ep_t *ep, seL4_CPtr *cap) {
    name = name ? name : "";
    ep = ep ? ep : (elrond_ep_t*)&elrond_ep_null;

    // endpoint already badged?
    seL4_Word badge_ = 0;
    if (ep->ntfn_count >= 0) {
        // endpoint already has matching badge?
        for (size_t i = 0; i < ep->ntfn_count; i++) {
            if (strcmp(ep->ntfns[i], name) == 0) {
                badge_ = ELROND_NTFN_BADGE(i);
                goto found;
            }
        }

        // allocate ntfn badge
        if (ep->ntfn_count >= ELROND_NTFN_MAX) {
            ZF_LOGE("Too many ntfns on endpoint (%lu/%lu)",
                    ep->ntfn_count, ELROND_NTFN_MAX);
            return -EOVERFLOW;
        }

        char *name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);

        const char **x = elrond_append(
                (void**)&ep->ntfns,
                sizeof(const char*),
                &ep->ntfn_count,
                &ep->ntfn_capacity);
        if (!x) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        *x = name_;

        badge_ = ELROND_NTFN_BADGE(ep->ntfn_count-1);
    }

    found:;
    // add to info
    char *name_ = malloc(strlen(name)+1);
    if (!name_) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }
    strcpy(name_, name);

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = sizeof(seL4_CPtr);
    // either already badged or needs to be badged
    if (!badge_) {
        x->type = ELROND_TYPE_CAP;
        x->u.cap = ep->ntfn_ep;
    } else {
        x->type = ELROND_TYPE_BADGED;
        x->u.badged.cap = ep->ntfn_ep;
        x->u.badged.badge = badge_;
    }

    // mint into root task?
    if (cap) {
        if (!badge_) {
            *cap = ep->ntfn_ep;
        } else {
            cspacepath_t src;
            cspacepath_t dst;
            vka_cspace_make_path(elrond->vka, ep->ntfn_ep, &src);

            int err = vka_cspace_alloc_path(elrond->vka, &dst);
            if (err) {
                ZF_LOGE("Could not allocate ntfn cap");
                return -ENOMEM;
            }

            err = vka_cnode_mint(&dst, &src, seL4_NoRead, badge_);
            if (err) {
                ZF_LOGE("Could not mint ntfn cap");
                return -EACCES;
            }

            *cap = dst.capPtr;
        }
    }

    return 0;
}

int elrond_add_wakeup(elrond_t *elrond, const char *pattern,
         const char *name, elrond_ep_t *ep, seL4_CPtr *cap) {
    name = name ? name : "";
    ep = ep ? ep : (elrond_ep_t*)&elrond_ep_null;

    // endpoint already badged?
    seL4_Word badge_ = 0;
    if (ep->ntfn_count >= 0) {
        badge_ = ELROND_WAKEUP_BADGE;
    }

    // add to info
    char *name_ = malloc(strlen(name)+1);
    if (!name_) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }
    strcpy(name_, name);

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = sizeof(seL4_CPtr);
    // either already badged or needs to be badged
    if (!badge_) {
        x->type = ELROND_TYPE_CAP;
        x->u.cap = ep->ntfn_ep;
    } else {
        x->type = ELROND_TYPE_BADGED;
        x->u.badged.cap = ep->ntfn_ep;
        x->u.badged.badge = badge_;
    }

    // mint into root task?
    if (cap) {
        if (!badge_) {
            *cap = ep->ntfn_ep;
        } else {
            cspacepath_t src;
            cspacepath_t dst;
            vka_cspace_make_path(elrond->vka, ep->ntfn_ep, &src);

            int err = vka_cspace_alloc_path(elrond->vka, &dst);
            if (err) {
                ZF_LOGE("Could not allocate ntfn cap");
                return -ENOMEM;
            }

            err = vka_cnode_mint(&dst, &src, seL4_NoRead, badge_);
            if (err) {
                ZF_LOGE("Could not mint ntfn cap");
                return -EACCES;
            }

            *cap = dst.capPtr;
        }
    }

    return 0;
}


// for debugging
void elrond_ep_debugdump(elrond_t *elrond, elrond_ep_t *ep,
        const char *indent) {
    indent = indent ? indent : "";
    printf("%s<ep %p %ld (%ld) %ld (%ld)>%s\n", indent,
        ep,
        ep->call_ep,
        ep->call_count,
        ep->ntfn_ep,
        ep->ntfn_count,
        (ep->call_count > 0 || ep->ntfn_count > 0)
            ? ":"
            : "");
    if (ep->call_count > 0) {
        printf("%s  calls:\n", indent);
        for (size_t j = 0; j < ep->call_count; j++) {
            printf("%s    <call %s 0x%016lx>\n", indent,
                    ep->calls[j],
                    ELROND_CALL_BADGE(j));
        }
    }
    if (ep->ntfn_count > 0) {
        printf("%s  ntfns:\n", indent);
        for (size_t j = 0; j < ep->ntfn_count; j++) {
            printf("%s    <ntfn %s 0x%016lx>\n", indent,
                    ep->ntfns[j],
                    ELROND_NTFN_BADGE(j));
        }
    }
}


