//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#ifndef ELROND_EP_H
#define ELROND_EP_H

#include <sel4/sel4.h>
#include <sys/types.h>


typedef struct elrond elrond_t;

// state for endpoints
typedef struct elrond_ep {
    seL4_CPtr call_ep;
    seL4_CPtr ntfn_ep;

    const char **calls;
    ssize_t call_count;
    size_t call_capacity;
    const char **ntfns;
    ssize_t ntfn_count;
    size_t ntfn_capacity;
} elrond_ep_t;


// create an endpoint from badged capabilities
//
// any calls/notifications created on this endpoint will redirect
// to the badged capabilities
int elrond_ep_frombadged(elrond_ep_t *ep,
        seL4_CPtr call_ep, seL4_CPtr ntfn_ep);


// some state access
static inline bool elrond_ep_call_isbadged(
        elrond_t *elrond, elrond_ep_t *ep) {
    return ep->call_count < 0;
}

static inline seL4_CPtr elrond_ep_call_ep(
        elrond_t *elrond, elrond_ep_t *ep) {
    return ep->call_ep;
}

static inline ssize_t elrond_ep_call_count(
        elrond_t *elrond, elrond_ep_t *ep) {
    return ep->call_count;
}

static inline bool elrond_ep_ntfn_isbadged(
        elrond_t *elrond, elrond_ep_t *ep) {
    return ep->ntfn_count < 0;
}

static inline seL4_CPtr elrond_ep_ntfn_ep(
        elrond_t *elrond, elrond_ep_t *ep) {
    return ep->ntfn_ep;
}

static inline ssize_t elrond_ep_ntfn_count(
        elrond_t *elrond, elrond_ep_t *ep) {
    return ep->ntfn_count;
}


// for debugging
void elrond_ep_debugdump(elrond_t *elrond, elrond_ep_t *ep,
        const char *indent);


#endif
