//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#ifndef ELROND_PROCESS_H
#define ELROND_PROCESS_H

#include <sel4/sel4.h>
#include <sys/types.h>


// these are the same as elrond_spawn*, but allow you to get a handle to
// the underlying process
int elrond_process_spawn(elrond_t *elrond, elrond_process_t *process,
        const char *name, const void *elf, size_t elf_size);
int elrond_process_spawnv(elrond_t *elrond, elrond_process_t *process,
        const char *name, const void *elf, size_t elf_size,
        const char **argv);
int elrond_process_spawnl(elrond_t *elrond, elrond_process_t *process,
        const char *name, const void *elf, size_t elf_size,
        const char *arg0, ...);


// some state access
static inline vspace_t *elrond_process_vspace(
        elrond_t *elrond, elrond_process_t *process) {
    return &process->vspace;
}

static inline seL4_CPtr elrond_process_cspace_root(
        elrond_t *elrond, elrond_process_t *process) {
    return process->cspace_root;
}

static inline size_t elrond_process_cspace_size(
        elrond_t *elrond, elrond_process_t *process) {
    return process->cspace_size;
}

static inline size_t elrond_process_stack_size(
        elrond_t *elrond, elrond_process_t *process) {
    return process->stack_size;
}

static inline size_t elrond_process_heap_size(
        elrond_t *elrond, elrond_process_t *process) {
    return process->heap_size;
}

static inline size_t elrond_process_priority(
        elrond_t *elrond, elrond_process_t *process) {
    return process->priority;
}


// for debugging
void elrond_process_debugdump(elrond_t *elrond, elrond_process_t *process,
        const char *indent);


#endif
