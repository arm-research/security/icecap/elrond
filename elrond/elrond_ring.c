//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include "elrond_ring.h"


// utils
static inline size_t nlog2(size_t a) {
    return 8*sizeof(size_t) - __builtin_clzl(a-1);
}

static inline size_t npw2(size_t a) {
    return 1 << nlog2(a);
}

static inline size_t aligndown(size_t a, size_t alignment) {
    return a - (a % alignment);
}

static inline size_t alignup(size_t a, size_t alignment) {
    return aligndown(a + alignment-1, alignment);
}

static inline size_t min(size_t a, size_t b) {
    return a < b ? a : b;
}

static inline size_t max(size_t a, size_t b) {
    return a > b ? a : b;
}


// create a ring from parts
int elrond_ring_fromparts(elrond_ring_t *ring,
        void *control, 
        void *buffer,
        size_t size,
        seL4_CPtr ntfn) {
    ring->control = control;
    ring->buffer = buffer;
    ring->size = size;
    ring->ntfn = ntfn;
}

// add a ring buffer to an elrond instance
//
// - either or both endpoints can be NULL, in which case
//   no notifications will be sent on state change. Note the ring
//   buffer is still usable if you are polling.
// - if recv_ring or send_ring is not NULL, the ring will be mapping
//   into the root task and returned there
int elrond_add_ring(elrond_t *elrond, const char *pattern,
        const char *name, size_t size,
        elrond_ep_t *sender_ep, elrond_ring_t *recv_ring,
        elrond_ep_t *recver_ep, elrond_ring_t *send_ring) {
    name = name ? name : "";

    // round up to nearest power of two and at least a page size, this
    // enables more efficient bitwise masking
    size = npw2(alignup(size, PAGE_SIZE_4K));

    // create memory for all our names
    char *name_ = malloc(strlen(name)+max(max(max(max(
            strlen("_send_ntfn"),
            strlen("_recv_ntfn")),
            strlen("_control")),
            strlen("_buffer")),
            strlen("_size"))+1);
    if (!name_) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    // create the notifications
    sprintf(name_, "%s_send_ntfn", name);
    int err = elrond_add_ntfn(elrond, pattern,
            name_, recver_ep, send_ring ? &send_ring->ntfn : NULL);
    if (err) {
        ZF_LOGE("Could not create send notification for ring %s", name);
        return err;
    }

    sprintf(name_, "%s_recv_ntfn", name);
    seL4_CPtr recv_ntfn;
    err = elrond_add_ntfn(elrond, pattern,
            name_, sender_ep, &recv_ntfn);
    if (err) {
        ZF_LOGE("Could not create recv notification for ring %s", name);
        return err;
    }

    // start the ring with a recv_ntfn pending so send always starts
    if (recv_ntfn) {
        seL4_Signal(recv_ntfn);
    }

    if (recv_ring) {
        recv_ring->ntfn = recv_ntfn;
    } else {
        if (recv_ntfn) {
            cspacepath_t recv_ntfn_path;
            vka_cspace_make_path(elrond_vka(elrond), recv_ntfn,
                    &recv_ntfn_path);
            vka_cnode_delete(&recv_ntfn_path);
            vka_cspace_free(elrond_vka(elrond), recv_ntfn);
        }
    }

    // create shared control block
    sprintf(name_, "%s_control", name);
    void *control;
    err = elrond_add_shared(elrond, pattern,
            name_, sizeof(elrond_ring_control_t), NULL,
            (recv_ring || send_ring) ? &control : NULL);
    if (err) {
        ZF_LOGE("Could not create control for ring");
        return err;
    }
    if (recv_ring) {
        recv_ring->control = control;
    }
    if (send_ring) {
        send_ring->control = control;
    }

    // now the somewhat complicated part, we want to create a single buffer
    // that's mirrored into two neighboring regions
    size_t page_count = size >> PAGE_BITS_4K;
    vka_t *vka = elrond_vka(elrond);

    seL4_CPtr *pages = malloc(2*page_count*sizeof(seL4_CPtr));
    if (!pages) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    for (size_t i = 0; i < page_count; i++) {
        vka_object_t frame;
        int err = vka_alloc_frame(vka, PAGE_BITS_4K, &frame);
        if (err) {
            ZF_LOGE("Unable to allocate frame");
            return -ENOMEM;
        }

        pages[i] = frame.cptr;
        pages[i+page_count] = frame.cptr;
    }

    sprintf(name_, "%s_buffer", name);
    void *buffer;
    err = elrond_add_pages(elrond, pattern,
            name_, 2*size,
            pages, PAGE_SIZE_4K,
            seL4_AllRights, true,
            &buffer);
    if (err) {
        ZF_LOGE("Could not create buffer for ring");
        return err;
    }
    if (recv_ring) {
        recv_ring->buffer = buffer;
    }
    if (send_ring) {
        send_ring->buffer = buffer;
    }

    // and finally the size
    sprintf(name_, "%s_size", name);
    err = elrond_add_info(elrond, pattern,
            name_, &size, sizeof(size_t));
    if (err) {
        ZF_LOGE("Could not add size for ring %s", name);
        return err;
    }
    if (recv_ring) {
        recv_ring->size = size;
    }
    if (send_ring) {
        send_ring->size = size;
    }

    // cleanup
    free(pages);
    free(name_);

    return 0;
}



// operations to interact with ring buffers in the root task

// core send operation
size_t elrond_ring_nbbeginsend(elrond_ring_t *ring, void **data) {
    // Note that recv/sent are wrapping around at 2xsize, but can only
    // ever be size distant. This avoids the empty/full ambiguity
    size_t recv = __atomic_load_n(&ring->control->recv, __ATOMIC_ACQUIRE);
    size_t sent = __atomic_load_n(&ring->control->sent, __ATOMIC_RELAXED);
    ssize_t used = sent - recv;
    if (used < 0) {
        used += 2*ring->size;
    }

    *data = &ring->buffer[sent & (ring->size-1)];
    return ring->size - used;
}

void elrond_ring_endsend(elrond_ring_t *ring, size_t size) {
    // update sent
    size_t sent = __atomic_load_n(&ring->control->sent, __ATOMIC_RELAXED);
    sent = (sent + size) & ((2*ring->size)-1);
    __atomic_store_n(&ring->control->sent, sent, __ATOMIC_RELEASE);

    // signal notification object
    if (ring->ntfn) {
        seL4_Signal(ring->ntfn);
    }
}

// core recv operation
size_t elrond_ring_nbbeginrecv(elrond_ring_t *ring, void **data) {
    // Note that recv/sent are wrapping around at 2xsize, but can only
    // ever be size distant. This avoids the empty/full ambiguity
    size_t recv = __atomic_load_n(&ring->control->recv, __ATOMIC_RELAXED);
    size_t sent = __atomic_load_n(&ring->control->sent, __ATOMIC_ACQUIRE);
    ssize_t used = sent - recv;
    if (used < 0) {
        used += 2*ring->size;
    }

    *data = &ring->buffer[recv & (ring->size-1)];
    return used;
}

void elrond_ring_endrecv(elrond_ring_t *ring, size_t size) {
    // update recv
    size_t recv = __atomic_load_n(&ring->control->recv, __ATOMIC_RELAXED);
    recv = (recv + size) & ((2*ring->size)-1);
    __atomic_store_n(&ring->control->recv, recv, __ATOMIC_RELEASE);

    // signal notification object
    if (ring->ntfn) {
        seL4_Signal(ring->ntfn);
    }
}


// composite operations
size_t elrond_ring_sendavail(elrond_ring_t *ring) {
    return elrond_ring_nbbeginsend(ring, &(void*){NULL});
}

size_t elrond_ring_recvavail(elrond_ring_t *ring) {
    return elrond_ring_nbbeginrecv(ring, &(void*){NULL});
}

size_t elrond_ring_nbsend(elrond_ring_t *ring, const void *data, size_t size) {
    void *send;
    size_t send_size = elrond_ring_nbbeginsend(ring, &send);
    if (send_size <= 0) {
        return send_size;
    }

    // send at most size
    send_size = min(send_size, size);
    memcpy(send, data, send_size);
    elrond_ring_endsend(ring, send_size);
    return send_size;
}

size_t elrond_ring_nbrecv(elrond_ring_t *ring, void *data, size_t size) {
    void *recv;
    size_t recv_size = elrond_ring_nbbeginrecv(ring, &recv);
    if (recv_size <= 0) {
        return recv_size;
    }

    // recv at most size
    recv_size = min(recv_size, size);
    memcpy(data, recv, recv_size);
    elrond_ring_endrecv(ring, recv_size);
    return recv_size;
}


//// for debugging
void elrond_ring_debugdump(elrond_ring_t *ring, const char *indent) {
    indent = indent ? indent : "";
    printf("%s<ring %p %lu %lu %lu>\n", indent,
            ring,
            ring->size,
            ring->control
                ? __atomic_load_n(&ring->control->sent, __ATOMIC_ACQUIRE)
                : 0,
            ring->control
                ? __atomic_load_n(&ring->control->recv, __ATOMIC_ACQUIRE)
                : 0);
}
