//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#ifndef ELROND_RING_H
#define ELROND_RING_H

#include <elrond.h>

typedef struct elrond_ring_control {
    __attribute__((aligned(64)))
    size_t sent;
    __attribute__((aligned(64)))
    size_t recv;
} elrond_ring_control_t;

typedef struct elrond_ring {
    elrond_ring_control_t *control;
    uint8_t *buffer;
    size_t size;
    seL4_CPtr ntfn;
} elrond_ring_t;


// create a ring from parts
int elrond_ring_fromparts(elrond_ring_t *ring,
        void *control, 
        void *buffer,
        size_t size,
        seL4_CPtr ntfn);

// add a ring buffer to an elrond instance
//
// - either or both endpoints can be NULL, in which case
//   no notifications will be sent on state change. Note the ring
//   buffer is still usable if you are polling.
int elrond_add_ring(elrond_t *elrond, const char *pattern,
        const char *name, size_t size,
        elrond_ep_t *sender_ep, elrond_ring_t *recv_ring,
        elrond_ep_t *recver_ep, elrond_ring_t *send_ring);

// accessors
static inline void *elrond_ring_control(elrond_ring_t *ring) {
    return ring->control;
}

static inline void *elrond_ring_buffer(elrond_ring_t *ring) {
    return ring->buffer;
}

static inline size_t elrond_ring_size(elrond_ring_t *ring) {
    return ring->size;
}

static inline seL4_CPtr elrond_ring_ntfn(elrond_ring_t *ring) {
    return ring->ntfn;
}


// operations to interact with ring buffers in the root task
size_t elrond_ring_nbsend(elrond_ring_t *ring, const void *data, size_t size);
size_t elrond_ring_nbrecv(elrond_ring_t *ring, void *data, size_t size);
size_t elrond_ring_sendavail(elrond_ring_t *ring);
size_t elrond_ring_recvavail(elrond_ring_t *ring);

size_t elrond_ring_nbbeginsend(elrond_ring_t *ring, void **data);
size_t elrond_ring_nbbeginrecv(elrond_ring_t *ring, void **data);
void elrond_ring_endsend(elrond_ring_t *ring, size_t size);
void elrond_ring_endrecv(elrond_ring_t *ring, size_t size);

// for debugging
void elrond_ring_debugdump(elrond_ring_t *ring, const char *indent);


#endif
