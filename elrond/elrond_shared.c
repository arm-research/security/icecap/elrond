//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <elrond.h>
#include <vka/capops.h>
#include <sel4utils/mapping.h>


// utils
static inline size_t nlog2(size_t a) {
    return 8*sizeof(size_t) - __builtin_clzl(a-1);
}

static inline size_t npw2(size_t a) {
    return 1 << nlog2(a);
}

static inline size_t aligndown(size_t a, size_t alignment) {
    return a - (a % alignment);
}

static inline size_t alignup(size_t a, size_t alignment) {
    return aligndown(a + alignment-1, alignment);
}

static inline size_t min(size_t a, size_t b) {
    return a < b ? a : b;
}

static inline size_t max(size_t a, size_t b) {
    return a > b ? a : b;
}

// defined in elrond.c
extern void *elrond_append(
        void **entries,
        size_t entry_size,
        size_t *entry_count,
        size_t *entry_capacity);


// add shared memory or pages
//
// - pages here can actually be NULL, and any entry can be seL4_CapNull, these
//   will be backed by new zeroed pages allowing you to just configure the
//   attributes or leave holes in the pages
// - if init_data is NULL, the memory will be zeroed
// - if vaddr is not NULL, the resuling memory will be mapped
//   into the root process's vspace and returned there
int elrond_add_shared(elrond_t *elrond, const char *pattern,
        const char *name, size_t size, const void *init_data,
        void **vaddr) {
    // this mostly just defers to elrond_add_pages, but we need
    // to write init_data if it's provided
    size_t page_count = alignup(size, PAGE_SIZE_4K) >> PAGE_BITS_4K;
    seL4_CPtr *pages = NULL;
    if (init_data) {
        pages = malloc(page_count * sizeof(seL4_CPtr));
        if (!pages) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }

        const uint8_t *data_ = init_data;
        size_t size_ = size;
        size_t j = 0;
        while (size_ > 0) {
            // write at most one frame at a time
            size_t delta = min(PAGE_SIZE_4K, size_);

            // allocate a frame
            vka_object_t frame;
            int err = vka_alloc_frame(elrond->vka, PAGE_BITS_4K, &frame);
            if (err) {
                ZF_LOGE("Unable to allocate frame");
                return -ENOMEM;
            }

            // map it into our vspace
            void *mapping = sel4utils_dup_and_map(elrond->vka, elrond->vspace,
                    frame.cptr, PAGE_BITS_4K);
            if (!mapping) {
                ZF_LOGE("Could not map frame");
                return -EACCES;
            }

            // copy data
            memcpy(mapping, data_, delta);

            // unmap our frame
            sel4utils_unmap_dup(elrond->vka, elrond->vspace,
                    mapping, PAGE_BITS_4K);

            data_ += delta;
            size_ -= delta;
            pages[j] = frame.cptr;
            j += 1;
        }
    }

    int err = elrond_add_pages(elrond, pattern,
            name, size,
            pages, PAGE_SIZE_4K,
            seL4_AllRights, true,
            vaddr);

    // cleanup
    free(pages);

    return err;
}

int elrond_add_pages(elrond_t *elrond, const char *pattern,
        const char *name, size_t size,
        const seL4_CPtr *pages, size_t page_size,
        seL4_CapRights_t rights, bool cacheable,
        void **vaddr) {
    // create shared
    elrond_shared_t *shared = malloc(sizeof(elrond_shared_t));
    if (!shared) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }
    memset(shared, 0, sizeof(elrond_shared_t));

    // fill out any NULL (zeroed) pages
    size_t page_bits = nlog2(page_size);
    size_t page_count = alignup(size, page_size) >> page_bits;
    seL4_CPtr *pages_ = malloc(page_count * sizeof(seL4_CPtr));
    if (!pages_) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    if (pages) {
        memcpy(pages_, pages, page_count * sizeof(seL4_CPtr));
    } else {
        memset(pages_, 0, page_count * sizeof(seL4_CPtr));
    }

    for (size_t i = 0; i < page_count; i++) {
        if (!pages_[i]) {
            vka_object_t frame;
            int err = vka_alloc_frame(elrond->vka, page_bits, &frame);
            if (err) {
                ZF_LOGE("Unable to allocate frame");
                return -ENOMEM;
            }
            pages_[i] = frame.cptr;
        }
    }

    shared->size = size;
    shared->pages = pages_;
    shared->page_size = page_size;
    shared->page_count = page_count;
    shared->rights = rights;
    shared->cacheable = cacheable;

    // add to info
    char *name_ = "";
    if (name) {
        name_ = malloc(strlen(name)+1);
        if (!name_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(name_, name);
    }

    char *pattern_ = "";
    if (pattern) {
        pattern_ = malloc(strlen(pattern)+1);
        if (!pattern_) {
            ZF_LOGE("Out of memory");
            return -ENOMEM;
        }
        strcpy(pattern_, pattern);
    }

    elrond_info_t *x = elrond_append(
            (void**)&elrond->infos,
            sizeof(elrond_info_t),
            &elrond->info_count,
            &elrond->info_capacity);
    if (!x) {
        ZF_LOGE("Out of memory");
        return -ENOMEM;
    }

    x->pattern = pattern_;
    x->name = name_;
    x->size = sizeof(void*);
    x->type = ELROND_TYPE_SHARED;
    x->u.shared = shared;

    // map into the root task's vspace?
    if (vaddr) {
        size_t page_size = shared->page_size;
        size_t page_bits = nlog2(page_size);
        // map into root vspace?
        void *vaddr_;
        reservation_t resv = vspace_reserve_range_aligned(elrond->vspace,
                shared->page_count * page_size,
                page_bits,
                shared->rights,
                shared->cacheable,
                &vaddr_);
        if (!resv.res) {
            ZF_LOGE("Could not allocate shared %lu bytes (%lux%lu pages)",
                    shared->size,
                    shared->page_count,
                    page_size);
            return -ENOMEM;
        }

        for (size_t j = 0; j < shared->page_count; j++) {
            cspacepath_t src;
            cspacepath_t dst;
            vka_cspace_make_path(elrond->vka, shared->pages[j], &src);
            int err = vka_cspace_alloc_path(elrond->vka, &dst);
            if (err) {
                ZF_LOGE("Could not allocate cap");
                return -ENOMEM;
            }

            err = vka_cnode_copy(&dst, &src, shared->rights);
            if (err) {
                ZF_LOGE("Could not copy cap");
                return -EACCES;
            }

            err = vspace_map_pages_at_vaddr(elrond->vspace,
                    &dst.capPtr,
                    NULL,
                    (void*)((uintptr_t)vaddr_ + (j * PAGE_SIZE_4K)),
                    1,
                    page_bits,
                    resv);
            if (err) {
                ZF_LOGE("Could not map page");
                return -EACCES;
            }
        }

        *vaddr = vaddr_;
    }

    return 0;
}



// elrond.c uses this
int elrond_process_prepare_shared(elrond_t *elrond,
        elrond_process_t *process,
        elrond_shared_t *shared,
        uintptr_t *vaddr) {
    // null shared?
    if (!shared) {
        *vaddr = (uintptr_t)NULL;
        return 0;
    }

    // map pages into vspace
    size_t page_size = shared->page_size;
    size_t page_bits = nlog2(page_size);
    size_t page_count = shared->page_count;

    if (page_count > process->cspace_size - process->cap_count) {
        ZF_LOGE("Out of caps\n");
        return -ENOMEM;
    }
    seL4_CPtr pages_ = process->cap_count;

    uintptr_t vaddr_;
    reservation_t resv = vspace_reserve_range_aligned(&process->vspace,
            page_count * page_size,
            page_bits,
            shared->rights,
            shared->cacheable,
            (void**)&vaddr_);
    if (!resv.res) {
        ZF_LOGE("Could not allocate shared %lu bytes (%lux%lu pages)",
                shared->size,
                page_count,
                page_size);
        return -ENOMEM;
    }

    cspacepath_t tmp;
    int err = vka_cspace_alloc_path(elrond->vka, &tmp);
    if (err) {
        ZF_LOGE("Could not allocate cap");
        return -ENOMEM;
    }

    for (size_t j = 0; j < page_count; j++) {
        cspacepath_t src;
        cspacepath_t dst;
        vka_cspace_make_path(elrond->vka,
                shared->pages[j], &src);

        err = vka_cnode_copy(&tmp, &src, shared->rights);
        if (err) {
            ZF_LOGE("Could not copy cap");
            return -EACCES;
        }

        err = vspace_map_pages_at_vaddr(&process->vspace,
                &tmp.capPtr,
                NULL,
                (void*)(vaddr_ + (j * PAGE_SIZE_4K)),
                1,
                page_bits,
                resv);
        if (err) {
            ZF_LOGE("Could not map page");
            return -EACCES;
        }

        dst = (cspacepath_t){
            .root = process->cspace_root,
            .capPtr = pages_ + j,
            .capDepth = nlog2(process->cspace_size),
        };
        vka_cnode_move(&dst, &tmp);
    }
    process->cap_count += page_count;

    // cleanup
    vka_cspace_free_path(elrond->vka, tmp);
    vspace_free_reservation(&process->vspace, resv);

    *vaddr = vaddr_;
    return 0;
}

