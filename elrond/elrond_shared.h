//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#ifndef ELROND_SHARED_H
#define ELROND_SHARED_H

#include <sel4/sel4.h>
#include <sys/types.h>
#include <stdbool.h>


typedef struct elrond elrond_t;

// state for shared memory
typedef struct elrond_shared {
    size_t size;
    const seL4_CPtr *pages;
    size_t page_size;
    size_t page_count;
    seL4_CapRights_t rights;
    bool cacheable;
} elrond_shared_t;


#endif
