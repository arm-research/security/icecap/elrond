
set(ELROND_HELPERS_PATH "${CMAKE_CURRENT_LIST_DIR}" CACHE STRING "")
mark_as_advanced(ELROND_HELPERS_PATH)

function(elrond_helpers_import_project)
    add_subdirectory(${ELROND_HELPERS_PATH} elrond_helpers)
endfunction()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(
    elrond_helpers
    DEFAULT_MSG
    ELROND_HELPERS_PATH
)
