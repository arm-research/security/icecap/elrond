# Elrond-helpers

Elrond-helpers is an optional convenience library for processes loaded by
[Elrond][elrond].
It provides a small set of utilities to help interact with [seL4][seL4]
and [musl libc][musl] leveraging Elrond's ability to configure processes
at load time.

## Elrond info sections

First things first.
As described in [elrond/README.md][elrond-info], processes can be provisioned
with extra configuration info during load time by including special info
sections in their binary:

``` c
elrond_add_info(&elrond, "task*",
        "test_info", &(uint32_t){0x12345678}, sizeof(uint32_t));
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

``` c
uint32_t test_info = 0;
__attribute__((section(".note.elrond.info.test_info,\"\",@note//")))
static const void *__elrond_info_test_info = &test_info;

int main(void) {
    // prints: test_info: 0x12345678
    printf("test_info: 0x%08x\n", test_info);
}
```

Elrond-helpers wraps up this gobbledygook in the [`ELROND_INFO`][ELROND_INFO]
macro:

``` c
#include <elrond_helpers.h>

uint32_t test_info = 0;
ELROND_INFO("test_info", &test_info);

int main(void) {
    // prints: test_info: 0x12345678
    printf("test_info: 0x%08x\n", test_info);
}
```

## Callbacks

Elrond provides a common "main" endpoint for each process, which acts as a
combined call and notification endpoint bound to the main thread.
See [elrond/README.md][elrond-ipc].
Elrond-helpers provides a way to interact with this endpoint asynchronously
in the form of the [`ELROND_CALL_CB`][ELROND_CALL_CB] and
[`ELROND_NTFN_CB`][ELROND_NTFN_CB] macros.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_call1;
seL4_CPtr task1_call2;
seL4_CPtr task1_ntfn1;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_call(&elrond, "task1", "task1_call1", task1_ep, &task1_call1);
elrond_add_call(&elrond, "task1", "task1_call2", task1_ep, &task1_call2);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_SetMR(0, 42);
seL4_SetMR(0, 43);
seL4_MessageInfo_t msg = seL4_Call(task1_call1,
        seL4_MessageInfo_new(0, 0, 0, 2));
assert(seL4_MessageInfo_get_length(msg) == 1);
// prints: root: recved: 85
printf("root: recved: %ld\n", seL4_GetMR(0));

seL4_SetMR(0, 44);
seL4_Send(task1_call2, seL4_MessageInfo_new(0, 0, 0, 1));

seL4_Signal(task1_ntfn1);
```

``` c
#include <elrond_helpers.h>

void task1_call1(seL4_MessageInfo_t msg) {
    assert(seL4_MessageInfo_get_length(msg) == 2);
    seL4_Word a = seL4_GetMR(0);
    seL4_Word b = seL4_GetMR(1);
    // prints: task1: recved task1_call1: 42 43
    printf("task1: recved task1_call1: %ld %ld\n", a, b);

    // reply is optional
    seL4_SetMR(0, a + b);
    seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 1));
}
ELROND_CALL_CB("task1_call1", task1_call1);

void task1_call2(seL4_MessageInfo_t msg) {
    assert(seL4_MessageInfo_get_length(msg) == 1);
    // prints: task1: recved task1_call2: 44
    printf("task1: recved task1_call2: %ld\n", seL4_GetMR(0));
}
ELROND_CALL_CB("task1_call2", task1_call2);

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);
```

These work by placing call/notification's callback + badge together in special
sections named `__elrond_call_cbs` and `__elrond_ntfn_cbs`.
[`elrond_poll`][elrond_poll] tries to recieve from the main endpoint, and if
successful, looks at these sections to find the relevant call/notification
callback.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_ntfn1;
seL4_CPtr task1_wakeup;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_add_wakeup(&elrond, "task1", "task1_wakeup", task1_ep, &task1_wakeup);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_Signal(task1_ntfn1);
seL4_Signal(task1_wakeup);
```

``` c
#include <elrond_helpers.h>

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);

int main(void) {
    // prints: task1: started
    printf("task1: started\n");
    while (1) {
        elrond_poll();
    }
}
```

[`elrond_yield`][elrond_yield] does the same thing as `elrond_poll`, but also
calls [`seL4_Yield`][seL4_Yield] if there is no work to be done.

``` c
#include <elrond_helpers.h>

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);

int main(void) {
    // prints: task1: started
    printf("task1: started\n");
    while (1) {
        elrond_yield();
    }
}
```

[`elrond_wait`][elrond_wait] is the blocking version of `elrond_poll`, and
does not return until some call/notification is recieved.
Note that `elrond_wait` does return when it recieves a wakeup notification,
which can make `elrond_wait` quite useful for thread coordination and for
efficient polling of shared resources.

``` c
#include <elrond_helpers.h>

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);

int main(void) {
    // prints: task1: started
    printf("task1: started\n");
    int counter = 1;
    while (1) {
        elrond_wait();
        // prints: task1: woken up 1
        // prints: task1: woken up 2
        // stops printing
        printf("task1: woken up %d\n", counter);
        counter += 1;
    }
}
```

And, last but not least, [`elrond_spin`][elrond_spin] loops while calling
`elrond_wait`, resigning the current thread to only process callbacks.

``` c
#include <elrond_helpers.h>

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);

int main(void) {
    // prints: task1: started
    printf("task1: started\n");
    elrond_spin();
}
```

There is also [`elrond_abort`][elrond_abort], which suspends the current
thread.
But this does not process callbacks, and actually doesn't really have anything
to do with callbacks.

``` c
#include <elrond_helpers.h>

void task1_ntfn1(void) {
    // does not print
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);

int main(void) {
    // prints: task1: started
    printf("task1: started\n");
    elrond_abort();
    elrond_spin();
}
```

As a convenience, Elrond-helpers provides a weakly-linked main that calls
`elrond_spin` and does nothing else.

``` c
#include <elrond_helpers.h>

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);
```

All calls/notifications handled this way go through the weakly-linked
[`__elrond_cb`][__elrond_cb] function.
This can be overwritten to replace callback handling with your own custom
logic.

If overwritten, [`__elrond_cb0`][__elrond_cb0] still provides the original
looks-at-`__elrond_call_cbs`/`__elrond_ntfn_cbs` logic.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_call1;
seL4_CPtr task1_call2;
seL4_CPtr task1_ntfn1;
seL4_CPtr task1_wakeup;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_call(&elrond, "task1", "task1_call1", task1_ep, &task1_call1);
elrond_add_call(&elrond, "task1", "task1_call2", task1_ep, &task1_call2);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_add_wakeup(&elrond, "task1", "task1_wakeup", task1_ep, &task1_wakeup);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_SetMR(0, 42);
seL4_SetMR(0, 43);
seL4_MessageInfo_t msg = seL4_Call(task1_call1,
        seL4_MessageInfo_new(0, 0, 0, 2));
assert(seL4_MessageInfo_get_length(msg) == 1);
// prints: root: recved: 85
printf("root: recved: %ld\n", seL4_GetMR(0));

seL4_SetMR(0, 44);
seL4_Send(task1_call2, seL4_MessageInfo_new(0, 0, 0, 1));

seL4_Signal(task1_ntfn1);
seL4_Signal(task1_wakeup);
```

``` c
#include <elrond_helpers.h>

void task1_call1(seL4_MessageInfo_t msg) {
    assert(seL4_MessageInfo_get_length(msg) == 2);
    seL4_Word a = seL4_GetMR(0);
    seL4_Word b = seL4_GetMR(1);
    // prints: task1: recved task1_call1: 42 43
    printf("task1: recved task1_call1: %ld %ld\n", a, b);

    // reply is optional
    seL4_SetMR(0, a + b);
    seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 1));
}
ELROND_CALL_CB("task1_call1", task1_call1);

void task1_call2(seL4_MessageInfo_t msg) {
    assert(seL4_MessageInfo_get_length(msg) == 1);
    // prints: task1: recved task1_call2: 44
    printf("task1: recved task1_call2: %ld\n", seL4_GetMR(0));
}
ELROND_CALL_CB("task1_call2", task1_call2);

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);

extern void __elrond_cb0(seL4_Word badge, seL4_MessageInfo_t msg);
void __elrond_cb(seL4_Word badge, seL4_MessageInfo_t msg) {
    // prints: task1: recved cb: 0x0000000000000001 2
    // prints: task1: recved cb: 0x0000000000000002 1
    // prints: task1: recved cb: 0x8000000000000001 0
    // prints: task1: recved cb: 0x8000000000000000 0
    printf("task1: recved cb: 0x%016lx %ld\n",
            badge, seL4_MessageInfo_get_length(msg));

    __elrond_cb0(badge, msg);
}
```

## Ring buffers

Mentioned briefly in [Elrond.md][elrond-rings], Elrond-helpers includes a
simple single-producer, single-consumer ring buffer implementation as a small
proof-of-concept for building higer-level IPC mechanisms.

Found in `elrond_ring.h`,  ring buffers can be defined by the
`ELROND_RING_SENDER` and `ELROND_RING_RECVER` macros.
However, if the ring buffer is not configured in the root task,
[`elrond_ring_isdetached`][elrond_ring_isdetached] will return
true, and attempting to send/recv on the ring buffer will fault.

The simplest way to interact with these ring buffers is with the POSIX-like
[`elrond_ring_send`][elrond_ring_send] and [`elrond_ring_recv`][elrond_ring_recv]
functions:

``` c
elrond_ep_t *task1_ep;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);

elrond_add_ring(&elrond, "task1,task2",
        "ring", 4096,
        task1_ep, NULL,
        task2_ep, NULL);

elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_SENDER("ring", &ring);

int main(void) {
    char buf[256];
    strcpy(buf, "Chris was here!");
    elrond_ring_send(&ring, buf, strlen(buf));
}
```

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_RECVER("ring", &ring);

int main(void) {
    char buf[256];
    size_t recved = elrond_ring_recv(&ring, buf, sizeof(buf));
    // prints: task2: recved 15
    // prints: task2: Chris was here!
    printf("task2: recved %ld\n", recved);
    printf("task2: %.*s", (int)recved, buf);
}
```

However, Elrond's ring buffers are implemented with mirrored buffers, which
means it is always possible to get an uninterupted view of the data.

```
elrond_ring_t
.---------.
| control ----.
| buffer -----|---------------.
| size    |   |               |
| ntfn    |   |               |
'---------'   |          .----|------------------------------------.
              |          |.---|--------------------.               |
              v          ||   v                    v               v
virtual      .---------. ||  .-----------------------------.-----------------------------.
memory       | sent -----'|  |s here!              Chris wa|s here!              Chris wa|
             | recv ------'  '-----------------------------'-----------------------------'
             |         |      |                             |
             |         |      |                             |
             '---------'      |                             |
              |               |.----------------------------'
              v               vv
physical     .---------.     .-----------------------------.
memory       | sent    |     |s here!              Chris wa|
             | recv    |     '-----------------------------'
             |         |
             |         |
             '---------'
```

Accessing the underlying data directly with [`elrond_ring_beginsend`][elrond_ring_beginsend]
and [`elrond_ring_beginrecv`][elrond_ring_beginrecv] can avoid unnecessary
copies, and result in more efficient sends/recvs.

Note that if you use `elrond_ring_beginsend` and `elrond_ring_beginrecv`, you
also need to call [`elrond_ring_endsend`][elrond_ring_endsend]/[`elrond_ring_endrecv`][elrond_ring_endrecv]
to actually commit a send/recv and notify the other side of the ring buffer.

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_SENDER("ring", &ring);

int main(void) {
    size_t len = strlen("Chris was here!");
    while (1) {
        char *buf;
        size_t avail = elrond_ring_beginsend(&ring, (void**)&buf);
        if (avail >= len) {
            memcpy(buf, "Chris was here!", len);
            elrond_ring_endsend(&ring, len);
            break;
        }
    }
}
```

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_RECVER("ring", &ring);

int main(void) {
    char *buf;
    size_t recved = elrond_ring_beginrecv(&ring, (void**)&buf);
    // prints: task2: recved 15
    // prints: task2: Chris was here!
    printf("task2: recved %ld\n", recved);
    printf("task2: %.*s", (int)recved, buf);
    elrond_ring_endrecv(&ring, recved);
}
```

`elrond_ring_recv`/`elrond_ring_beginrecv` will block until there is some
data in the ring buffer, and `elrond_ring_send`/`elrond_ring_beginsend` will
block until there is free space in the ring buffer.

Elrond-helpers also provides non-blocking variants, `elrond_ring_nbrecv`,
`elrond_ring_nbbeginrecv`, `elrond_ring_nbsend`, `elrond_ring_nbbeginsend`,
which return 0 bytes immediately if no data/free space is available.

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_SENDER("ring", &ring);

int main(void) {
    size_t len = strlen("Chris was here!");
    while (1) {
        char *buf;
        size_t avail = elrond_ring_nbbeginsend(&ring, (void**)&buf);
        if (avail >= len) {
            memcpy(buf, "Chris was here!", len);
            elrond_ring_endsend(&ring, len);
            break;
        }

        elrond_yield();
    }
}
```

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_RECVER("ring", &ring);

int main(void) {
    while (1) {
        char *buf;
        size_t recved = elrond_ring_nbbeginrecv(&ring, (void**)&buf);
        if (recved > 0) {
            // prints: task2: recved 15
            // prints: task2: Chris was here!
            printf("task2: recved %ld\n", recved);
            printf("task2: %.*s", (int)recved, buf);
            elrond_ring_endrecv(&ring, recved);
        }

        elrond_yield();
    }
}
```

[`elrond_ring_recvavail`][elrond_ring_recvavail] and
[`elrond_ring_sendavail`][elrond_ring_sendavail] report the current number of
bytes ready to be recieved or sent, which can be useful for deciding when to
call the above functions.

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_SENDER("ring", &ring);

int main(void) {
    size_t len = strlen("Chris was here!");
    while (elrond_ring_sendavail(&ring) < len) {
        elrond_yield();
    }
    char *buf;
    elrond_ring_nbbeginsend(&ring, (void**)&buf);
    memcpy(buf, "Chris was here!", len);
    elrond_ring_endsend(&ring, len);
}
```

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring;
ELROND_RING_RECVER("ring", &ring);

int main(void) {
    while (1) {
        size_t recved = elrond_ring_recvavail(&ring);
        if (recved > 0) {
            char *buf;
            elrond_ring_nbbeginrecv(&ring, (void**)&buf);
            // prints: task2: recved 15
            // prints: task2: Chris was here!
            printf("task2: recved %ld\n", recved);
            printf("task2: %.*s", (int)recved, buf);
            elrond_ring_endrecv(&ring, recved);
        }

        elrond_yield();
    }
}
```

If you're not a fan of boilerplate, Elrond's ring buffers can be hooked into
the above callback mechanism with [`ELROND_RING_SEND_CB`][ELROND_RING_SEND_CB]
and [`ELROND_RING_RECV_CB`][ELROND_RING_RECV_CB], to be processed
asynchronously.

``` c
elrond_ep_t *task1_ep;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);

elrond_add_ring(&elrond, "task1,task2",
        "ring1", 4096,
        task1_ep, NULL,
        task2_ep, NULL);
elrond_add_ring(&elrond, "task1,task2",
        "ring2", 4096,
        task1_ep, NULL,
        task2_ep, NULL);

elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

elrond_ring_t ring1;
ELROND_RING_SENDER("ring1", &ring1);

uint8_t counter = 1;
size_t ring2_send_cb(void *buf, size_t size) {
    uint8_t *buf_ = buf;
    for (size_t i = 0; i < size; i++) {
        buf_[i] = counter;
        count += 1;
    }
    return size;
}
ELROND_RING_SEND_CB("ring2", ring2_send_cb);

int main(void) {
    elrond_ring_send(&ring1, "abcdefghijklmnopqrstuvwxyz", 26);
    elrond_spin();
}
```

``` c
#include <elrond_helpers.h>
#include <elrond_ring.h>

bool done = false;
size_t ring1_recv_cb(void *buf, size_t size) {
    if (!done && size >= 5) {
        char *buf_ = buf;
        // prints: task2: recved letters: a b c d e
        printf("task2: recved letters: %c %c %c %c %c\n",
                buf_[0], buf_[1], buf_[2], buf_[3], buf_[4]);
        done = true;
        return 5;
    }
    return 0;
}
ELROND_RING_RECV_CB("ring1", ring1_recv_cb);

elrond_ring_t ring2;
ELROND_RING_RECVER("ring2", &ring2);

int main(void) {
    uint8_t *buf;
    while (elrond_ring_beginrecv(&ring2, (void**)&buf) < 5) {}
    // prints: task2: recved numbers: 1 2 3 4 5
    printf("task2: recved numbers: %d %d %d %d %d\n",
            buf[0], buf[1], buf[2], buf[3], buf[4]);
    elrond_ring_endrecv(&ring2, 5);

    elrond_spin();
}
```

Note that Elrond's ring buffers internally call `elrond_wait`, which is why
the above example doesn't deadlock!

## musl syscalls

Elrond also provides an overridable implementation of [musl][musl] syscalls.

Note this was based heavily on [libsel4muslcsys][libsel4muslcsys], which
provides musl syscalls relying only on the seL4 APIs.

Elrond can still load and run libsel4muslcsys processes without modification,
and Elrond-helpers can link against libsel4muslcsys if syscalls are disabled
in [elrond_helpers/CMakeLists.txt](CMakeLists.txt), however this would miss out
on the following extensions.

The way musl finds the syscalls is a bit roundabout.
When musl's `__init_libc` is called, somewhere between `_start` and `main`,
musl expects to find a pointer to the syscall entry point in the
[auxiliary vector][aux_vectors] `AT_SYSINFO`.
The auxiliary vectors follow the env array as a part of `_start`'s initial
stack frame, and Elrond fills this out at load time as a part of setting up
the process.
For `AT_SYSINFO`, Elrond simply copies the value found in the special section
`__vsyscall` in the ELF file.
This is compatible with both Elrond-helpers and libsel4muslcsys.

If you're using Elrond-helpers' syscalls, `AT_SYSINFO` ends up pointing to
the weakly-linked [`__elrond_syscall0`][__elrond_syscall] function, which
can be overwritten for full control of musl's syscalls.

If overwritten, [`__elrond_syscall0`][__elrond_syscall0] still provides the
original syscall logic.

``` c
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

``` c
extern long __elrond_syscall0(long sysnum, va_list args);
long __elrond_syscall(long sysnum, va_list args) {
    // NOTE we really can't call printf here!
    // prints: task1: syscall no 96
    // prints: task1: syscall no 29
    // prints: task1: syscall no 66
    // prints: task1: syscall no 214
    // prints: task1: syscall no 222
    seL4_DebugPutString("example-task4: syscall no ");
    unsigned long x = sysnum;
    unsigned long npw10 = 1;
    while (x/npw10 >= 10) {
        npw10 *= 10;
    }
    while (npw10 > 0) {
        seL4_DebugPutChar('0' + (x/npw10));
        x %= npw10;
        npw10 /= 10;
    }
    seL4_DebugPutString("\n");

    return __elrond_syscall0(sysnum, args);
}

int main(void) {
    // prints: task1: Hello world!
    printf("task1: Hello world!\n");
}
```

Internally, `__elrond_syscall` uses a big lookup table containing every syscall
as a weakly-linked function.
These functions, named `__elrond_sys_<syscall>`, can be individually overwritten
to customize specific syscalls.
And, just like `__elrond_syscall`, each of the original syscall implementations
can be accessed through `__elrond_sys0_<syscall>`, even if overwritten.

``` c
extern long __elrond_sys0_writev(va_list args);
long __elrond_sys_writev(va_list args) {
    va_list args_;
    va_copy(args_, args);
    int fd = va_arg(args_, int);
    struct iovec *iov = va_arg(args_, struct iovec*);
    int iovcnt = va_arg(args_, int);
    va_end(args_);

    if (fd == 42) {
        long total = 0;
        for (int i = 0; i < iovcnt; i++) {
            const char *base = iov[i].iov_base;
            for (int j = 0; j < iov[i].iov_len; j++) {
                if (base[j] >= 'a' && base[j] <= 'z') {
                    putchar(base[j] + ('A' - 'a'));
                } else {
                    putchar(base[j]);
                }
            }
            total += iov[i].iov_len;
        }
        return total;
    } else {
        return __elrond_sys0_writev(args);
    }
}

int main(void) {
    // prints: TASK1: HELLO WORLD!
    dprintf(42, "task1: Hello world!\n");
}

```

Note that at this point, the original meaning of these "syscalls" has been lost.
At no point does musl actually leave userspace when invoking a "syscall", unless
the underlying syscall implementation calls an seL4 API.

Unfortunately, as far as I can tell, there is no authoritative documentation
over the expected behavior of each syscall.
If this actually exists somewhere please let me know.
These are the best methods I've found for figuring what the heck a syscall
should do:

1. Dig around in the [source code][musl-syscall-src] of the related C API in
   musl to try to figure out how the well document C API maps to the underlying
   syscall invocation.

2. Dig around in the [source code][linux-syscall-src] of the Linux kernel or
   other implementation to try to figure out what it's doing.

Anyways, back to Elrond-helpers.

Elrond-helpers implements a number of syscalls, such that common libc operations
can leverage Elrond configuration optionally provided by the root task.
The end result is a simple musl libc implementation that can be configured by
Elrond at load time.

### stdin/stdout/stderr

As the most important debugging tool, Elrond-helpers support redirecting stdin,
stderr, and stdout to optional ring buffers.

``` c
elrond_ep_t *root_ep;
elrond_add_ep(&elrond, NULL, "main", &root_ep);
elrond_ep_t *task1_ep;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);

elrond_ring_t stdin_ring;
elrond_add_ring(&elrond, "task1",
        "stdin", 4096,
        root_ep, NULL,
        task2_ep, &stdin_ring);

elrond_ring_t stdout_ring;
elrond_add_ring(&elrond, "task1",
        "stdout", 4096,
        task2_ep, &stdout_ring,
        root_ep, NULL);

elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

// prints: HHeelllloo  wwoorrlldd!!
size_t len = strlen("Hello world!\n");
size_t sent = elrond_ring_nbsend(&stdin_ring, "Hello world!\n", len);
assert(sent == len);
while (1) {
    seL4_Wait(elrond_ep_ntfn_ep(&elrond, root_ep, NULL));
    char *buf;
    size_t recved = elrond_ring_nbbeginrecv(&stdout_ring, (void**)&buf);
    printf("%.*s", (int)recved, buf);
    elrond_ring_endrecv(&stdout_ring, recved);
}
```

``` c
int main(void) {
    while (1) {
        char c = getchar();
        printf("%c%c", c, c);
    }
}
```

If these ring buffers are not provided, Elrond will fall back to use the
reliable [`seL4_DebugPutString`][seL4_DebugPutString].
Though note that `seL4_DebugPutString` is only available in debug builds
of seL4.

### heap

By default, Elrond gives every process a heap configured by
[`elrond_add_heap_size`][elrond_add_heap_size].
This heap is provided to the process as a single region of memory, which
Elrond-helpers hands out via `brk` and `mmap` using a simple one-time
allocator.

musl then uses a combination of `brk` and `mmap` to find memory for its malloc
implementation.

``` c
elrond_add_heap_size(&elrond, "task1", 1024);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

``` c
int main(void) {
    // prints: task1: malloc(100): 0x3272600
    printf("task1: malloc(100): %p\n", malloc(100));
    // prints: task1: malloc(10000): (null) 
    printf("task1: malloc(10000): %p\n", malloc(10000));
}
```

Note that a process's heap can be effectively disabled by setting its
heap_size to zero.
This can be used to save memory on no-malloc processes.

### yield/exit

Elrond-helpers also connects a number of syscalls to `elrond_yield`,
`elrond_abort`, etc.
This means any code relying on libc to yield will also give call/notification
callbacks a chance to run.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_ntfn1;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_Signal(task1_ntfn1);
```

``` c
#include <elrond_helpers.h>
#include <sched.h>

void task1_ntfn1(void) {
    // prints: task1: recved task1_ntfn1
    printf("task1: recved task1_ntfn1\n");
}
ELROND_NTFN_CB("task1_ntfn1", task1_ntfn1);

int main(void) {
    // prints: task1: started
    printf("task1: started\n");
    while (1) {
        sched_yield();
    }
}
```


[elrond]: ../elrond/README.md
[seL4]: https://sel4.systems
[musl]: https://musl.libc.org
[elrond-info]: ../elrond/README.md#elrond-info-sections
[elrond-ipc]: ../elrond/README.md#configurating-ipc
[elrond-rings]: ../elrond/README.md#ring-buffers
[ELROND_INFO]: ../REFERENCE.md#elrond-helper-macros
[ELROND_CALL_CB]: ../REFERENCE.md#elrond-helper-macros
[ELROND_NTFN_CB]: ../REFERENCE.md#elrond-helper-macros
[elrond_poll]: ../REFERENCE.md#elrond-helper-functions
[elrond_yield]: ../REFERENCE.md#elrond-helper-functions
[seL4_Yield]: https://docs.sel4.systems/projects/sel4/api-doc.html#yield
[elrond_wait]: ../REFERENCE.md#elrond-helper-functions
[elrond_spin]: ../REFERENCE.md#elrond-helper-functions
[elrond_abort]: ../REFERENCE.md#elrond-helper-functions
[__elrond_cb]: ../REFERENCE.md#elrond-helper-weak-functions
[__elrond_cb0]: ../REFERENCE.md#elrond-helper-weak-functions
[elrond_ring_isdetached]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_send]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_recv]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_beginsend]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_beginrecv]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_endsend]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_endrecv]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_sendavail]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[elrond_ring_recvavail]: ../REFERENCE.md#elrond-helper-ring-buffer-functions
[libsel4muslcsys]: https://github.com/seL4/seL4_libs/tree/master/libsel4muslcsys
[aux_vectors]: http://articles.manugarg.com/aboutelfauxiliaryvectors.html
[__elrond_syscall]: ../REFERENCE.md#elrond-helper-weak-functions
[__elrond_syscall0]: ../REFERENCE.md#elrond-helper-weak-functions
[musl-syscall-src]: https://git.musl-libc.org/cgit/musl/tree/src/linux/getrandom.c?id=4100279825c17807bdabf1c128ba4e49a1dea406#n4
[linux-syscall-src]: https://elixir.bootlin.com/linux/v5.18.14/source/drivers/char/random.c#L1249
[seL4_DebugPutString]: https://docs.sel4.systems/projects/sel4/api-doc.html#debugging-system-calls
[elrond_add_heap_size]: ../REFERENCE.md#elrond-functions
