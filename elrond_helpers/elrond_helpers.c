//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include "elrond_helpers.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

// dummy entries to make sure these symbols are always emitted
__attribute__((section("__elrond_call_cbs")))
elrond_cb_t __elrond_call_cb_null = {seL4_CapNull, NULL};
__attribute__((section("__elrond_ntfn_cbs")))
elrond_cb_t __elrond_ntfn_cb_null = {seL4_CapNull, NULL};

// little known feature! these are automatically generated for custom
// sections by the linker
extern elrond_cb_t __start___elrond_call_cbs;
extern elrond_cb_t __stop___elrond_call_cbs;
extern elrond_cb_t __start___elrond_ntfn_cbs;
extern elrond_cb_t __stop___elrond_ntfn_cbs;

seL4_CPtr elrond_main_tcb;
ELROND_INFO("main_tcb", &elrond_main_tcb);
seL4_CPtr elrond_call_ep;
ELROND_INFO("main_call_ep", &elrond_call_ep);
size_t elrond_call_count;
ELROND_INFO("main_call_count", &elrond_call_count);
size_t elrond_ntfn_count;
ELROND_INFO("main_ntfn_count", &elrond_ntfn_count);

static bool elrond_is_init = false;
static void (**elrond_call_cbs)(seL4_MessageInfo_t msg) = NULL;
static void (**elrond_ntfn_cbs)(void) = NULL;

// setup callbacks for fast dispatch
static void elrond_init(void) {
    // allocate new arrays, this is needed because all calls/notifications
    // may not be handled, this array needs to be sparse
    if (elrond_call_count > 0) {
        elrond_call_cbs = malloc(
                elrond_call_count * sizeof(void (*)(seL4_MessageInfo_t msg)));
        assert(elrond_call_cbs);
    }

    if (elrond_ntfn_count > 0) {
        elrond_ntfn_cbs = malloc(
                elrond_ntfn_count * sizeof(void (*)(seL4_MessageInfo_t msg)));
        assert(elrond_ntfn_cbs);
    }

    // To make calls/notifications more efficient, we first sort these
    // by their badges. Note that unused callbacks may be zero, we just move
    // these to the end and forget about them.
    for (size_t i = 0; i < elrond_call_count; i++) {
        seL4_Word badge = ELROND_CALL_BADGE(i);

        // find cb with expected badge?
        for (elrond_cb_t *cb_ = &__start___elrond_call_cbs;
                cb_ < &__stop___elrond_call_cbs;
                cb_++) {
            if (cb_->badge == badge) {
                elrond_call_cbs[i] = cb_->cb.call_cb;
                goto next_call;
            }
        }

        // no?
        elrond_call_cbs[i] = NULL;

        next_call:;
    }

    for (size_t i = 0; i < elrond_ntfn_count; i++) {
        seL4_Word badge = ELROND_NTFN_BADGE(i);

        // find cb with expected badge?
        for (elrond_cb_t *cb_ = &__start___elrond_ntfn_cbs;
                cb_ < &__stop___elrond_ntfn_cbs;
                cb_++) {
            if (cb_->badge == badge) {
                elrond_ntfn_cbs[i] = cb_->cb.ntfn_cb;
                goto next_ntfn;
            }
        }

        // no?
        elrond_ntfn_cbs[i] = NULL;

        next_ntfn:;
    }

    elrond_is_init = true;
}

void __elrond_cb0(seL4_Word badge, seL4_MessageInfo_t tag) {
    if (!(badge & ELROND_NTFN_MASK)) {
        // call?
        seL4_Word i = badge - 1;
        assert(i < elrond_call_count);

        if (elrond_call_cbs[i]) {
            elrond_call_cbs[i](tag);
        }
    } else {
        // notification? may be more than one
        seL4_Word bits = badge & ~ELROND_NTFN_MASK;
        size_t i = 0;

        while (bits) {
            i += __builtin_ctzl(bits);
            bits >>= __builtin_ctzl(bits);
            assert(i < elrond_ntfn_count);

            if (elrond_ntfn_cbs[i]) {
                elrond_ntfn_cbs[i]();
            }

            i += 1;
            bits >>= 1;
        }
    }
}
__attribute__((weak, alias("__elrond_cb0")))
void __elrond_cb(seL4_Word badge, seL4_MessageInfo_t tag);

// poll for any outstanding calls/notifications
void elrond_poll(void) {
    // need to init?
    if (!elrond_is_init) {
        elrond_init();
    }

    seL4_Word badge = 0;
    seL4_MessageInfo_t tag = seL4_NBRecv(elrond_call_ep, &badge);
    if (badge) {
        __elrond_cb(badge, tag);
    }
}

// give other threads a chance to run, and also poll calls/notifications
void elrond_yield(void) {
    // need to init?
    if (!elrond_is_init) {
        elrond_init();
    }

    seL4_Word badge = 0;
    seL4_MessageInfo_t tag = seL4_NBRecv(elrond_call_ep, &badge);
    if (badge) {
        __elrond_cb(badge, tag);
    } else {
        seL4_Yield();
    }
}

// wait until new calls/notifications
void elrond_wait(void) {
    // need to init?
    if (!elrond_is_init) {
        elrond_init();
    }

    seL4_Word badge = 0;
    seL4_MessageInfo_t tag = seL4_Recv(elrond_call_ep, &badge);
    if (badge) {
        __elrond_cb(badge, tag);
    }
}

// spin waiting for new calls/notifications
__attribute__((noreturn))
void elrond_spin(void) {
    while (1) {
        elrond_wait();
    }
}

// abort our process
__attribute__((noreturn))
void elrond_abort(void) {
    // try to suspend ourselves?
    if (elrond_main_tcb) {
        seL4_TCB_Suspend(elrond_main_tcb);
    }

    // nothing else to do, just spin
    //
    // NOTE we do NOT dispatch any calls/notifications here
    while (1) {
        seL4_Yield();
    }
}


#ifdef CONFIG_ELROND_MAIN
// this main is just a convenience if no main is defined
__attribute__((weak))
int main(void) {
    elrond_spin();
}
#endif

