//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include "elrond_ring.h"
#include <string.h>
#include <stdio.h>


// utils
static inline size_t nlog2(size_t a) {
    return 8*sizeof(size_t) - __builtin_clzl(a-1);
}

static inline size_t npw2(size_t a) {
    return 1 << nlog2(a);
}

static inline size_t aligndown(size_t a, size_t alignment) {
    return a - (a % alignment);
}

static inline size_t alignup(size_t a, size_t alignment) {
    return aligndown(a + alignment-1, alignment);
}

static inline size_t min(size_t a, size_t b) {
    return a < b ? a : b;
}

static inline size_t max(size_t a, size_t b) {
    return a > b ? a : b;
}


int elrond_ring_fromparts(elrond_ring_t *ring,
        void *control,
        void *buffer,
        size_t size,
        seL4_CPtr ntfn) {
    ring->control = control;
    ring->buffer = buffer;
    ring->size = size;
    ring->ntfn = ntfn;
}


// core send operation
size_t elrond_ring_nbbeginsend(elrond_ring_t *ring, void **data) {
    // Note that recv/sent are wrapping around at 2xsize, but can only
    // ever be size distant. This avoids the empty/full ambiguity
    size_t recv = __atomic_load_n(&ring->control->recv, __ATOMIC_ACQUIRE);
    size_t sent = __atomic_load_n(&ring->control->sent, __ATOMIC_RELAXED);
    ssize_t used = sent - recv;
    if (used < 0) {
        used += 2*ring->size;
    }

    *data = &ring->buffer[sent & (ring->size-1)];
    return ring->size - used;
}

void elrond_ring_endsend(elrond_ring_t *ring, size_t size) {
    // update sent
    size_t sent = __atomic_load_n(&ring->control->sent, __ATOMIC_RELAXED);
    sent = (sent + size) & ((2*ring->size)-1);
    __atomic_store_n(&ring->control->sent, sent, __ATOMIC_RELEASE);

    // signal notification object
    if (ring->ntfn) {
        seL4_Signal(ring->ntfn);
    }
}

// core recv operation
size_t elrond_ring_nbbeginrecv(elrond_ring_t *ring, void **data) {
    // Note that recv/sent are wrapping around at 2xsize, but can only
    // ever be size distant. This avoids the empty/full ambiguity
    size_t recv = __atomic_load_n(&ring->control->recv, __ATOMIC_RELAXED);
    size_t sent = __atomic_load_n(&ring->control->sent, __ATOMIC_ACQUIRE);
    ssize_t used = sent - recv;
    if (used < 0) {
        used += 2*ring->size;
    }

    *data = &ring->buffer[recv & (ring->size-1)];
    return used;
}

void elrond_ring_endrecv(elrond_ring_t *ring, size_t size) {
    // update recv
    size_t recv = __atomic_load_n(&ring->control->recv, __ATOMIC_RELAXED);
    recv = (recv + size) & ((2*ring->size)-1);
    __atomic_store_n(&ring->control->recv, recv, __ATOMIC_RELEASE);

    // signal notification object
    if (ring->ntfn) {
        seL4_Signal(ring->ntfn);
    }
}


// composite operations
size_t elrond_ring_sendavail(elrond_ring_t *ring) {
    return elrond_ring_nbbeginsend(ring, &(void*){NULL});
}

size_t elrond_ring_recvavail(elrond_ring_t *ring) {
    return elrond_ring_nbbeginrecv(ring, &(void*){NULL});
}

size_t elrond_ring_nbsend(elrond_ring_t *ring, const void *data, size_t size) {
    void *send;
    size_t send_size = elrond_ring_nbbeginsend(ring, &send);
    if (send_size <= 0) {
        return send_size;
    }

    // send at most size
    send_size = min(send_size, size);
    memcpy(send, data, send_size);
    elrond_ring_endsend(ring, send_size);
    return send_size;
}

size_t elrond_ring_nbrecv(elrond_ring_t *ring, void *data, size_t size) {
    void *recv;
    size_t recv_size = elrond_ring_nbbeginrecv(ring, &recv);
    if (recv_size <= 0) {
        return recv_size;
    }

    // recv at most size
    recv_size = min(recv_size, size);
    memcpy(data, recv, recv_size);
    elrond_ring_endrecv(ring, recv_size);
    return recv_size;
}


// blocking operations
size_t elrond_ring_beginsend(elrond_ring_t *ring, void **data) {
    while (1) {
        size_t send_size = elrond_ring_nbbeginsend(ring, data);
        if (send_size > 0) {
            return send_size;
        }

        // note that any notification can wake us up from here
        elrond_wait();
    }
}

size_t elrond_ring_beginrecv(elrond_ring_t *ring, void **data) {
    while (1) {
        size_t recv_size = elrond_ring_nbbeginrecv(ring, data);
        if (recv_size > 0) {
            return recv_size;
        }

        // note that any notification can wake us up from here
        elrond_wait();
    }
}

size_t elrond_ring_send(elrond_ring_t *ring, const void *data, size_t size) {
    void *send;
    ssize_t send_size = elrond_ring_beginsend(ring, &send);
    if (send_size <= 0) {
        return send_size;
    }

    // send at most size
    send_size = min(send_size, size);
    memcpy(send, data, send_size);
    elrond_ring_endsend(ring, send_size);
    return send_size;
}

size_t elrond_ring_recv(elrond_ring_t *ring, void *data, size_t size) {
    void *recv;
    size_t recv_size = elrond_ring_beginrecv(ring, &recv);
    if (recv_size <= 0) {
        return recv_size;
    }

    // recv at most size
    recv_size = min(recv_size, size);
    memcpy(data, recv, recv_size);
    elrond_ring_endrecv(ring, recv_size);
    return recv_size;
}


// for debugging
void elrond_ring_debugdump(elrond_ring_t *ring, const char *indent) {
    indent = indent ? indent : "";
    printf("%s<ring %p %lu %lu %lu>\n", indent,
            ring,
            ring->size,
            ring->control
                ? __atomic_load_n(&ring->control->sent, __ATOMIC_ACQUIRE)
                : 0,
            ring->control
                ? __atomic_load_n(&ring->control->recv, __ATOMIC_ACQUIRE)
                : 0);
}
