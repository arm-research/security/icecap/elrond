//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#ifndef ELROND_RING_H
#define ELROND_RING_H

#include <elrond_helpers.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>
#include <assert.h>


typedef struct elrond_ring_control {
    __attribute__((aligned(64)))
    size_t sent;
    __attribute__((aligned(64)))
    size_t recv;
} elrond_ring_control_t;

typedef struct elrond_ring {
    elrond_ring_control_t *control;
    uint8_t *buffer;
    size_t size;
    seL4_CPtr ntfn;
} elrond_ring_t;

#define ELROND_RING_SENDER(_name, _pointer) \
    ELROND_INFO(_name "_control", &(_pointer)->control); \
    ELROND_INFO(_name "_buffer", &(_pointer)->buffer); \
    ELROND_INFO(_name "_size", &(_pointer)->size); \
    ELROND_INFO(_name "_send_ntfn", &(_pointer)->ntfn);

#define ELROND_RING_RECVER(_name, _pointer) \
    ELROND_INFO(_name "_control", &(_pointer)->control); \
    ELROND_INFO(_name "_buffer", &(_pointer)->buffer); \
    ELROND_INFO(_name "_size", &(_pointer)->size); \
    ELROND_INFO(_name "_recv_ntfn", &(_pointer)->ntfn);

#define ELROND_RING_SEND_CB__(_name, _cb, _counter) \
    __attribute__((unused)) \
    static const size_t (*__elrond_ring_cb_##_counter)( \
            void *data, size_t size) = _cb; \
    static elrond_ring_t __elrond_ring_##_counter; \
    ELROND_RING_SENDER(_name, &__elrond_ring_##_counter) \
    static void __elrond_ring_handler_##_counter(void) { \
        void *_data; \
        size_t _size = elrond_ring_nbbeginsend( \
                &__elrond_ring_##_counter, &_data); \
        if (_size > 0) { \
            _size = (_cb)(_data, _size); \
        } \
        elrond_ring_endsend(&__elrond_ring_##_counter, _size); \
    } \
    ELROND_NTFN_CB(_name "_recv_ntfn", __elrond_ring_handler_##_counter)
#define ELROND_RING_SEND_CB_(_name, _cb, _counter) \
    ELROND_RING_SEND_CB__(_name, _cb, _counter)
#define ELROND_RING_SEND_CB(_name, _cb) \
    ELROND_RING_SEND_CB_(_name, _cb, __COUNTER__)

#define ELROND_RING_RECV_CB__(_name, _cb, _counter) \
    __attribute__((unused)) \
    static const size_t (*__elrond_ring_cb_##_counter)( \
            void *data, size_t size) = _cb; \
    static elrond_ring_t __elrond_ring_##_counter; \
    ELROND_RING_RECVER(_name, &__elrond_ring_##_counter) \
    static void __elrond_ring_handler_##_counter(void) { \
        void *_data; \
        size_t _size = elrond_ring_nbbeginrecv( \
                &__elrond_ring_##_counter, &_data); \
        if (_size > 0) { \
            _size  =(_cb)(_data, _size); \
        } \
        elrond_ring_endrecv(&__elrond_ring_##_counter, _size); \
    } \
    ELROND_NTFN_CB(_name "_send_ntfn", __elrond_ring_handler_##_counter)
#define ELROND_RING_RECV_CB_(_name, _cb, _counter) \
    ELROND_RING_RECV_CB__(_name, _cb, _counter)
#define ELROND_RING_RECV_CB(_name, _cb) \
    ELROND_RING_RECV_CB_(_name, _cb, __COUNTER__)


int elrond_ring_fromparts(elrond_ring_t *ring,
        void *control,
        void *buffer,
        size_t size,
        seL4_CPtr ntfn);

// accessors
static inline bool elrond_ring_isdetached(elrond_ring_t *ring) {
    return ring->control == NULL;
}

static inline void *elrond_ring_control(elrond_ring_t *ring) {
    return ring->control;
}

static inline void *elrond_ring_buffer(elrond_ring_t *ring) {
    return ring->buffer;
}

static inline size_t elrond_ring_size(elrond_ring_t *ring) {
    return ring->size;
}

static inline seL4_CPtr elrond_ring_ntfn(elrond_ring_t *ring) {
    return ring->ntfn;
}

size_t elrond_ring_send(elrond_ring_t *ring, const void *data, size_t size);
size_t elrond_ring_recv(elrond_ring_t *ring, void *data, size_t size);

size_t elrond_ring_nbsend(elrond_ring_t *ring, const void *data, size_t size);
size_t elrond_ring_nbrecv(elrond_ring_t *ring, void *data, size_t size);
size_t elrond_ring_sendavail(elrond_ring_t *ring);
size_t elrond_ring_recvavail(elrond_ring_t *ring);

size_t elrond_ring_beginsend(elrond_ring_t *ring, void **data);
size_t elrond_ring_beginrecv(elrond_ring_t *ring, void **data);
size_t elrond_ring_nbbeginsend(elrond_ring_t *ring, void **data);
size_t elrond_ring_nbbeginrecv(elrond_ring_t *ring, void **data);
void elrond_ring_endsend(elrond_ring_t *ring, size_t size);
void elrond_ring_endrecv(elrond_ring_t *ring, size_t size);

// for debugging
void elrond_ring_debugdump(elrond_ring_t *ring, const char *indent);


#endif
