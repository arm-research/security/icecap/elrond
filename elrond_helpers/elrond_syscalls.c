//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include "elrond_helpers.h"

#ifdef CONFIG_ELROND_SYSCALLS

#include "elrond_ring.h"
#include <stdarg.h>
#include <stddef.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/uio.h>
#include <sys/mman.h>
#include <errno.h>
#include <sel4runtime.h>


// intercept stdin/stdout/stderr
static elrond_ring_t stdin_ring;
ELROND_RING_RECVER("stdin", &stdin_ring);
static elrond_ring_t stdout_ring;
ELROND_RING_SENDER("stdout", &stdout_ring);
static elrond_ring_t stderr_ring;
ELROND_RING_SENDER("stderr", &stderr_ring);

// intercept readv/writev for stdin/stdout handling
long __elrond_sys0_readv(va_list args) {
    va_list args_;
    va_copy(args_, args);
    int fd = va_arg(args_, int);
    struct iovec *iov = va_arg(args_, struct iovec*);
    int iovcnt = va_arg(args_, int);
    va_end(args_);

    if (fd == STDIN_FILENO) {
        // intercept stdin
        if (!elrond_ring_isdetached(&stdin_ring)) {
            long res = 0;
            for (int i = 0; i < iovcnt; i++) {
                elrond_ring_recv(&stdin_ring,
                        iov[i].iov_base, iov[i].iov_len);
                res += iov[i].iov_len;
            }
            return res;
        } else {
            return 0;
        }
    } else {
        return -ENOSYS;
    }
}

#ifdef CONFIG_DEBUG_BUILD
bool stdout_showed_warning = false;
bool stderr_showed_warning = false;
#endif

long __elrond_sys0_writev(va_list args) {
    va_list args_;
    va_copy(args_, args);
    int fd = va_arg(args_, int);
    struct iovec *iov = va_arg(args_, struct iovec*);
    int iovcnt = va_arg(args_, int);
    va_end(args_);

    if (fd == STDOUT_FILENO) {
        // intercept stdout
        if (!elrond_ring_isdetached(&stdout_ring)) {
            long res = 0;
            for (int i = 0; i < iovcnt; i++) {
                elrond_ring_send(&stdout_ring,
                        iov[i].iov_base, iov[i].iov_len);
                res += iov[i].iov_len;
            }
            return res;
        } else {
            // fall back to seL4_DebugPutString in debug mode
            #ifdef CONFIG_DEBUG_BUILD
            if (!stdout_showed_warning) {
                seL4_DebugPutString("elrond:warning: "
                        "stdout falling back to seL4_DebugPutString\n");
                stdout_showed_warning = true;
            }
            #endif
            long res = 0;
            for (int i = 0; i < iovcnt; i++) {
                char *base = iov[i].iov_base;
                #ifdef CONFIG_DEBUG_BUILD
                for (int j = 0; j < iov[i].iov_len; j++) {
                    seL4_DebugPutChar(base[j]);
                }
                #endif
                res += iov[i].iov_len;
            }
            return res;
        }
    } else if (fd == STDERR_FILENO) {
        // intercept stderr
        if (!elrond_ring_isdetached(&stderr_ring)) {
            long res = 0;
            for (int i = 0; i < iovcnt; i++) {
                elrond_ring_send(&stderr_ring,
                        iov[i].iov_base, iov[i].iov_len);
                res += iov[i].iov_len;
            }
            return res;
        } else if (!elrond_ring_isdetached(&stdout_ring)) {
            // fall back to stdout if it is connected
            long res = 0;
            for (int i = 0; i < iovcnt; i++) {
                elrond_ring_send(&stdout_ring,
                        iov[i].iov_base, iov[i].iov_len);
                res += iov[i].iov_len;
            }
            return res;
        } else {
            // fall back to seL4_DebugPutString in debug mode
            #ifdef CONFIG_DEBUG_BUILD
            if (!stderr_showed_warning) {
                seL4_DebugPutString("elrond:warning: "
                        "stderr falling back to seL4_DebugPutString\n");
                stderr_showed_warning = true;
            }
            #endif
            long res = 0;
            for (int i = 0; i < iovcnt; i++) {
                char *base = iov[i].iov_base;
                #ifdef CONFIG_DEBUG_BUILD
                for (int j = 0; j < iov[i].iov_len; j++) {
                    seL4_DebugPutChar(base[j]);
                }
                #endif
                res += iov[i].iov_len;
            }
            return res;
        }
    } else {
        // forward syscall
        return -ENOSYS;
    }
}

long __elrond_sys0_ioctl(va_list args) {
    // called on stdout/stderr, just noop
    return 0;
}

long __elrond_sys0_read(va_list args) {
    // defer to readv
    int fd = va_arg(args, int);
    void *data = va_arg(args, void*);
    size_t size = va_arg(args, size_t);
    struct iovec iov = {.iov_base=data, .iov_len=size};
    return readv(fd, &iov, 1);
}

long __elrond_sys0_write(va_list args) {
    // defer to writev
    int fd = va_arg(args, int);
    void *data = va_arg(args, void*);
    size_t size = va_arg(args, size_t);
    struct iovec iov = {.iov_base=data, .iov_len=size};
    return writev(fd, &iov, 1);
}


// provide a simple brk/mmap/mremap for backing musl's heap
uint8_t *heap;
ELROND_INFO("heap", &heap);
#ifdef CONFIG_DEBUG_BUILD
size_t heap_size = -1;
#else
size_t heap_size = 0;
#endif
ELROND_INFO("heap_size", &heap_size);

size_t heap_off = 0;
#ifdef CONFIG_DEBUG_BUILD
bool heap_showed_warning = false;
#endif


// note that musl will call both brk and mmap for normal malloc calls
//
// brk allocates from the front, while mmap allocates from the back of
// our heap memory
//
long __elrond_sys0_brk(va_list args) {
    uint8_t *new_brk = va_arg(args, uint8_t*);

    // just querying current brk
    if (!new_brk) {
        return (long)(heap + heap_off);
    }

    // change brk
    #ifdef CONFIG_DEBUG_BUILD
    if (heap_size == (size_t)-1) {
        if (!heap_showed_warning) {
            // this warning is very useful for debugging because
            // malloc is often called as a part of debug prints
            //
            // NOTE that normally the head_size should be explicitly
            // set to zero if the heap is zero sized.
            //
            seL4_DebugPutString("elrond:warning: "
                    "brk called but heap is corrupted, bug?\n");
            heap_showed_warning = true;
        }
        return 0;
    }
    #endif

    if (new_brk < heap || new_brk > heap+heap_size) {
        return 0;
    }

    heap_off = new_brk - heap;
    return (long)(heap + heap_off);
}

static long __elrond_sys0_mmap_(void *addr, size_t len,
        int prot, int flags, int fd, int off) {
    if (!(flags & MAP_ANONYMOUS)) {
        // not implemented
        return -ENOMEM;
    }

    #ifdef CONFIG_DEBUG_BUILD
    if (heap_size == (size_t)-1) {
        if (!heap_showed_warning) {
            // this warning is very useful for debugging because
            // malloc is often called as a part of debug prints
            //
            // NOTE that normally the head_size should be explicitly
            // set to zero if the heap is zero sized.
            //
            seL4_DebugPutString("elrond:warning: "
                    "mmap called but heap is corrupted, bug?\n");
            heap_showed_warning = true;
        }
        return -ENOMEM;
    }
    #endif

    if (len > heap_size - heap_off) {
        return -ENOMEM;
    }

    heap_size -= len;
    return (long)(heap + heap_size);
}

long __elrond_sys0_mmap(va_list args) {
    void *addr = va_arg(args, void*);
    size_t len = va_arg(args, size_t);
    int prot = va_arg(args, int);
    int flags = va_arg(args, int);
    int fd = va_arg(args, int);
    off_t off = va_arg(args, off_t);
    return __elrond_sys0_mmap_(addr, len, prot, flags, fd, off);
}

long __elrond_sys0_mmap2(va_list args) {
    void *addr = va_arg(args, void*);
    size_t len = va_arg(args, size_t);
    int prot = va_arg(args, int);
    int flags = va_arg(args, int);
    int fd = va_arg(args, int);
    off_t off = va_arg(args, off_t);
    return __elrond_sys0_mmap_(addr, len, prot, flags, fd, off * 4096);
}

long __elrond_sys0_madvise(va_list args) {
    // do nothing
    return 0;
}


// yield, exit, etc
long __elrond_sys0_sched_yield(va_list args) {
    elrond_yield();
    return 0;
}

long __elrond_sys0_exit(va_list args) {
    elrond_abort();
}

long __elrond_sys0_exit_group(va_list args) {
    elrond_abort();
}

long __elrond_sys0_tkill(va_list args) {
    elrond_abort();
}

long __elrond_sys0_tgkill(va_list args) {
    elrond_abort();
}


// these are extra stubs that runtimes try to call for some reason,
// we can make most of these noops for now
//
// NOTE, quite a few of these would need to change to support
// multi-threading and thread-local storage
//
long __elrond_sys0_set_tid_address(va_list args) {
    // this is used as a thread id query
    return 1;
}

long __elrond_sys0_ppoll(va_list args) {
    return 0;
}

long __elrond_sys0_rt_sigaction(va_list args) {
    return 0;
}

long __elrond_sys0_rt_sigprocmask(va_list args) {
    return 0;
}

long __elrond_sys0_gettid(va_list args) {
    return 0;
}

long __elrond_sys0_getpid(va_list args) {
    return 0;
}

long __elrond_sys0_sigaltstack(va_list args) {
    return 0;
}



// should be generated by generate_default_syscalls.py
extern long __elrond_syscall_count;
extern long (*__elrond_syscalls[])(va_list args);

__attribute__((unused))
extern long __elrond_sys_default(va_list args);

// main syscall handler
long __elrond_syscall0(long sysnum, va_list args) {
    // optional debugging
    #if defined(CONFIG_DEBUG_BUILD) \
            && defined(CONFIG_ELROND_SYSCALLS_DEBUG_UNIMPLEMENTED)
    if (sysnum < 0
            || sysnum >= __elrond_syscall_count
            || __elrond_syscalls[sysnum] == __elrond_sys_default) {
        seL4_DebugPutString("elrond:warning: unimplemented syscall no ");
        unsigned long x = sysnum;
        unsigned long npw10 = 1;
        while (x/npw10 >= 10) {
            npw10 *= 10;
        }
        while (npw10 > 0) {
            seL4_DebugPutChar('0' + (x/npw10));
            x %= npw10;
            npw10 /= 10;
        }
        seL4_DebugPutString("\n");
        return -ENOSYS;
    }
    #endif

    #if defined(CONFIG_DEBUG_BUILD) \
            && defined(CONFIG_ELROND_SYSCALLS_DEBUG)
    seL4_DebugPutString("elrond:debug: syscall no ");
    unsigned long x = sysnum;
    unsigned long npw10 = 1;
    while (x/npw10 >= 10) {
        npw10 *= 10;
    }
    while (npw10 > 0) {
        seL4_DebugPutChar('0' + (x/npw10));
        x %= npw10;
        npw10 /= 10;
    }
    seL4_DebugPutString("\n");
    #endif

    // not in syscall table?
    if (sysnum < 0 || sysnum >= __elrond_syscall_count) {
        return -ENOSYS;
    }

    // dispatch syscall
    return __elrond_syscalls[sysnum](args);
}
__attribute__((weak, alias("__elrond_syscall0")))
long __elrond_syscall(long sysnum, va_list args);

// entry point for syscalls
static long __elrond_vsyscall_(long sysnum, ...) {
    // make it so we can actually pass va_list around
    va_list args;
    va_start(args, sysnum);
    long ret = __elrond_syscall(sysnum, args);
    va_end(args);
    return ret;
}

__attribute__((used, section("__vsyscall")))
void *__elrond_vsyscall = __elrond_vsyscall_;


// setup libc
extern void __init_libc(char const *const *envp, char const *name);

// this funkyness is to move the IPC buffer to the new TLS region, which
// changes underneath GCC
__attribute__((noinline))
static seL4_IPCBuffer *volatile *volatile_ipc_buffer(void) {
    return &__sel4_ipc_buffer;
}

__attribute__((used, constructor(200)))
void __elrond_init(void) {
    seL4_IPCBuffer *x = *volatile_ipc_buffer();
    __init_libc(sel4runtime_envp(), sel4runtime_argv()[0]);
    *volatile_ipc_buffer() = x;
}


#endif
