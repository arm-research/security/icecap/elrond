# Elrond-Rust-helpers

Elrond-Rust-helpers is an optional convenience library for [Rust][rust]
processes loaded by [Elrond][elrond].
It provides a small set of utilities to help interact with [seL4][seL4]
and [musl libc][musl] leveraging Elrond's ability to configure processes
at load time.

## C Dependencies

Elrond depends on [libsel4rustbindings][libsel4rustbindings] and
[Elrond-helpers][elrond_helpers].

libsel4rustbindings is a thin shim providing the seL4 C APIs in Rust
unmodified.
It's basically the direct output of [Rust bindgen][rust-bindgen] and
can be easily replaced with other generated seL4 Rust bindings.

Elrond-helpers is the C version of this library, and Elrond-Rust-helpers
depends on it as the core implementation of the following features.

In theory Elrond-Rust-helpers could be in pure Rust, but this just isn't
worth it:

1. Weak-linkage is not stable, as discussed [here][rust-weak], and
   Elrond-Rust-helpers uses weak-linkage heavily.

2. Elrond-Rust-helpers would need to somehow provide C bindings and
   header files, to make it possible to link against C libraries that
   depend on Elrond-helpers.
   This would get a bit complicated, and things would break if both
   Elrond-Rust-helpers and Elrond-helpers got linked into the final image.

3. It's duplicate work.

## The Linux-std hack

You may notice the following example are not marked as [`#![no_std]`][no_std],
as is common for experimental OSs like seL4.
This happens because std support is limited to a select few OSs by [a whitelist
hardcoded into the Rust codebase][std-whitelist].
So even though it is possible to build std for a custom target thanks to
[build-std][build-std], actually using std requires putting
`#![feature(restricted_std)]` in _every single dependency_, which sort of
defeats the purpose of building std in the first place.

This [blog post][toms-blog-post] has more details on the issues you hit when
you try to build std on a custom target the sanctioned way.

The workaround we use here is as dumb as it is effective.
We just pretend to be Linux.

Our full target triple (keyword triple) is `aarch64-sel4-linux-musl`.
This makes build-std happy, and honestly mostly does the right thing.

In order to support musl we provide most of the Linux syscall interface
anyways (described below), so even non-standard APIs should just work.
APIs that depend on hardware, such as time or getrandom, can be implemented
in the process by providing the relevant syscalls.

The only caveat is that the `target_os` cfg appears as `linux`, which might
cause problematic behavior in external crates.
`sel4` does appear in the `target_family` cfg if a distinction needs to be
made.

``` rust
#[cfg(target_os = "linux")]
#[cfg(target_family = "sel4")]
println!("I am Linux and seL4 apparently...");
```

## Elrond info sections

First things ~~first~~ third.

Ok you may have noticed this document's contents is copied from
[elrond_helpers/README.md][elrond_helpers].
After all Elrond-helpers and Elrond-Rust-helpers are the same API, just
for different programming languages.
Consider this a comparison of sorts.

Anyways, as described in [elrond/README.md][elrond-info], processes can be
provisioned with extra configuration info during load time by including special
info sections in their binary:

``` c
elrond_add_info(&elrond, "task*",
        "test_info", &(uint32_t){0x12345678}, sizeof(uint32_t));
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

``` c
uint32_t test_info = 0;
__attribute__((section(".note.elrond.info.test_info,\"\",@note//")))
static const void *__elrond_info_test_info = &test_info;

int main(void) {
    // prints: test_info: 0x12345678
    printf("test_info: 0x%08x\n", test_info);
}
```

We can do the same thing in Rust via the `#[link_section]` attribute:

``` rust
static mut TEST_INFO: u32 = 0;
#[link_section=".note.elrond.info.test_info"]
#[used]
static mut TEST_INFO_INFO: &'static u32 = unsafe { &TEST_INFO };

fn main() {
    // prints: test_info: 0x12345678
    println!("test_info: 0x{:08x}", unsafe { TEST_INFO });
}
```

Elrond-Rust-helpers wraps up this gobbledygook in the
[`elrond_helpers::info`][elrond_helpers::info] attribute:

``` rust
use elrond_helpers as elrond;

#[elrond::info("test_info")]
uint32_t test_info = 0;

fn main() {
    // prints: test_info: 0x12345678
    println!("test_info: 0x{:08x}", unsafe { TEST_INFO });
}
```

## Callbacks

Elrond provides a common "main" endpoint for each process, which acts as a
combined call and notification endpoint bound to the main thread.
See [elrond/README.md][elrond-ipc].
Elrond-helpers provides a way to interact with this endpoint asynchronously
in the form of the [`elrond_helpers::call_cb`][elrond_helpers::call_cb] and
[`elrond_helpers::ntfn_cb`][elrond_helpers::ntfn_cb] macros.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_call1;
seL4_CPtr task1_call2;
seL4_CPtr task1_ntfn1;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_call(&elrond, "task1", "task1_call1", task1_ep, &task1_call1);
elrond_add_call(&elrond, "task1", "task1_call2", task1_ep, &task1_call2);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_SetMR(0, 42);
seL4_SetMR(0, 43);
seL4_MessageInfo_t msg = seL4_Call(task1_call1,
        seL4_MessageInfo_new(0, 0, 0, 2));
assert(seL4_MessageInfo_get_length(msg) == 1);
// prints: root: recved: 85
printf("root: recved: %ld\n", seL4_GetMR(0));

seL4_SetMR(0, 44);
seL4_Send(task1_call2, seL4_MessageInfo_new(0, 0, 0, 1));

seL4_Signal(task1_ntfn1);
```

``` rust
#[!no_main]
use elrond_helpers as elrond;
use libsel4rustbindings::*;

#[elrond::call_cb("task1_call1")]
fn task1_call1(msg: seL4_MessageInfo_t) {
    unsafe {
        assert_eq!(seL4_MessageInfo_get_length(msg), 2);
        let a = seL4_GetMR(0);
        let b = seL4_GetMR(1);
        // prints: task1: recved task1_call1: 42 43
        println!("task1: recved task1_call1: {} {}", a, b);

        // reply is optional
        seL4_SetMR(0, a + b);
        seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 1));
    }
}

#[elrond::call_cb("task1_call2")]
fn task1_call2(msg: seL4_MessageInfo_t) {
    unsafe {
        assert(seL4_MessageInfo_get_length(msg) == 1);
        // prints: task1: recved task1_call2: 44
        println!("task1: recved task1_call2: {}", seL4_GetMR(0));
    }
}

#[elrond::ntfn_cb("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}
```

These work by placing call/notification's callback + badge together in special
sections named `__elrond_call_cbs` and `__elrond_ntfn_cbs`.
[`elrond_helpers::poll`][elrond_helpers::poll] tries to recieve from the main
endpoint, and if successful, looks at these sections to find the relevant
call/notification callback.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_ntfn1;
seL4_CPtr task1_wakeup;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_add_wakeup(&elrond, "task1", "task1_wakeup", task1_ep, &task1_wakeup);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_Signal(task1_ntfn1);
seL4_Signal(task1_wakeup);
```

``` rust
use elrond_helpers as elrond;

#[elrond::ntfn("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}

fn main() {
    // prints: task1: started
    println!("task1: started");
    loop {
        elrond::poll();
    }
}
```

[`elrond_helpers::yield`][elrond_helpers::yield] does the same thing as
`elrond_helpers::poll`, but also calls [`seL4_Yield`][seL4_Yield] if there is
no work to be done.

``` rust
use elrond_helpers as elrond;

#[elrond::ntfn("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}

fn main() {
    // prints: task1: started
    println!("task1: started");
    loop {
        elrond::yield();
    }
}
```

[`elrond_helpers::wait`][elrond_helpers::wait] is the blocking version of
`elrond_helpers::poll`, and does not return until some call/notification is
recieved.
Note that `elrond_helpers::wait` does return when it recieves a wakeup
notification, which can make `elrond_helpers::wait` quite useful for thread
coordination and for efficient polling of shared resources.

``` rust
use elrond_helpers as elrond;

#[elrond::ntfn("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}

fn main() {
    // prints: task1: started
    println!("task1: started");
    let mut counter = 1;
    loop {
        elrond::wait();
        // prints: task1: woken up 1
        // prints: task1: woken up 2
        // stops printing
        println!("task1: woken up {}", counter);
        counter += 1;
    }
}
```

And, last but not least, [`elrond_helpers::spin`][elrond_helpers::spin] loops
while calling `elrond_helpers::wait`, resigning the current thread to only
process callbacks.

``` c
use elrond_helpers as elrond;

#[elrond::ntfn("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}

fn main() {
    // prints: task1: started
    println!("task1: started");
    elrond::spin();
}
```

There is also [`elrond_helpers::abort`][elrond_helpers::abort], which suspends
the current thread.
But this does not process callbacks, and actually doesn't really have anything
to do with callbacks.

``` rust
use elrond_helpers as elrond;

#[elrond::ntfn("task1_ntfn1")]
fn task1_ntfn1() {
    // does not print
    println!("task1: recved task1_ntfn1");
}

#[allow(dead_code)]
fn main() {
    // prints: task1: started
    println!("task1: started");
    elrond::abort();
    elrond::spin();
}
```

As a convenience, Elrond-helpers provides a weakly-linked main that calls
`elrond_helpers::spin` and does nothing else.

``` c
#![no_main]
use elrond_helpers as elrond;

#[elrond::ntfn("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}
```

All calls/notifications handled this way go through the weakly-linked
[`__elrond_cb`][__elrond_cb] function.
This can be overwritten to replace callback handling with your own custom
logic.

If overwritten, [`__elrond_cb0`][__elrond_cb0] still provides the original
looks-at-`__elrond_call_cbs`/`__elrond_ntfn_cbs` logic.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_call1;
seL4_CPtr task1_call2;
seL4_CPtr task1_ntfn1;
seL4_CPtr task1_wakeup;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_call(&elrond, "task1", "task1_call1", task1_ep, &task1_call1);
elrond_add_call(&elrond, "task1", "task1_call2", task1_ep, &task1_call2);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_add_wakeup(&elrond, "task1", "task1_wakeup", task1_ep, &task1_wakeup);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_SetMR(0, 42);
seL4_SetMR(0, 43);
seL4_MessageInfo_t msg = seL4_Call(task1_call1,
        seL4_MessageInfo_new(0, 0, 0, 2));
assert(seL4_MessageInfo_get_length(msg) == 1);
// prints: root: recved: 85
printf("root: recved: %ld\n", seL4_GetMR(0));

seL4_SetMR(0, 44);
seL4_Send(task1_call2, seL4_MessageInfo_new(0, 0, 0, 1));

seL4_Signal(task1_ntfn1);
seL4_Signal(task1_wakeup);
```

``` c
#[!no_main]
use elrond_helpers as elrond;
use libsel4rustbindings::*;

#[elrond::call_cb("task1_call1")]
fn task1_call1(msg: seL4_MessageInfo_t) {
    unsafe {
        assert_eq!(seL4_MessageInfo_get_length(msg), 2);
        let a = seL4_GetMR(0);
        let b = seL4_GetMR(1);
        // prints: task1: recved task1_call1: 42 43
        println!("task1: recved task1_call1: {} {}", a, b);

        // reply is optional
        seL4_SetMR(0, a + b);
        seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 1));
    }
}

#[elrond::call_cb("task1_call2")]
fn task1_call2(msg: seL4_MessageInfo_t) {
    unsafe {
        assert(seL4_MessageInfo_get_length(msg) == 1);
        // prints: task1: recved task1_call2: 44
        println!("task1: recved task1_call2: {}", seL4_GetMR(0));
    }
}

#[elrond::ntfn_cb("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}

extern "C" {
    fn __elrond_cb0(badge: seL4_Word, msg: seL4_MessageInfo_t);
}

#[no_mangle]
extern "C" fn __elrond_cb(badge: seL4_Word, msg: seL4_MessageInfo_t) {
    unsafe {
        // prints: task1: recved cb: 0x0000000000000001 2
        // prints: task1: recved cb: 0x0000000000000002 1
        // prints: task1: recved cb: 0x8000000000000001 0
        // prints: task1: recved cb: 0x8000000000000000 0
        println!("task1: recved cb: 0x{:016x} {}",
                badge, seL4_MessageInfo_get_length(msg));

        __elrond_cb0(badge, msg)
    }
}
```

## Ring buffers

Mentioned briefly in [Elrond.md][elrond-rings], Elrond-Rust-helpers includes a
simple single-producer, single-consumer ring buffer implementation as a small
proof-of-concept for building higer-level IPC mechanisms.

Found in `elrond_helpers::ring`,  ring buffers can be defined with the
`elrond_helpers::ring::sender` and `elrond_helpers::ring::recver` attributes.
However, if the ring buffer is not configured in the root task,
[`Sender::is_detached`][Sender::is_detached]/[`Recver::is_detached`][Recver::is_detached]
will return true, and attempting to send/recv on the ring buffer will fault.

The simplest way to interact with these ring buffers is with the POSIX-like
[`Sender::send`][Sender::send] and [`Recver::recv`][Recver::recv]
functions:

``` c
elrond_ep_t *task1_ep;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);

elrond_add_ring(&elrond, "task1,task2",
        "ring", 4096,
        task1_ep, NULL,
        task2_ep, NULL);

elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` rust
use elrond_helpers as elrond;

#[elrond::ring::sender("ring")]
static mut RING: elrond::ring::Sender = elrond::ring::Sender::detached();

fn main() {
    let mut buf = [0u8; 256];
    write!(&mut buf[..], "Chris was here!").unwrap();
    unsafe { &mut RING }.send(&buf);
}
```

``` rust
use elrond_helpers as elrond;

#[elrond::ring::recver("ring")]
static mut RING: elrond::ring::Recver = elrond::ring::Recver::detached();

fn main() {
    let mut buf = [0u8; 256];
    let recved = unsafe { &mut RING }.recv(&mut buf);
    // prints: task2: recved 15
    // prints: task2: Chris was here!
    println!("task2: recved {}", recved);
    println!("task2: {}", str::from_utf8(&buf[..recved]));
}
```

However, Elrond's ring buffers are implemented with mirrored buffers, which
means it is always possible to get an uninterupted view of the data.

```
elrond_ring_t
.---------.
| control ----.
| buffer -----|---------------.
| size    |   |               |
| ntfn    |   |               |
'---------'   |          .----|------------------------------------.
              |          |.---|--------------------.               |
              v          ||   v                    v               v
virtual      .---------. ||  .-----------------------------.-----------------------------.
memory       | sent -----'|  |s here!              Chris wa|s here!              Chris wa|
             | recv ------'  '-----------------------------'-----------------------------'
             |         |      |                             |
             |         |      |                             |
             '---------'      |                             |
              |               |.----------------------------'
              v               vv
physical     .---------.     .-----------------------------.
memory       | sent    |     |s here!              Chris wa|
             | recv    |     '-----------------------------'
             |         |
             |         |
             '---------'
```

Accessing the underlying data directly with [`Sender::beginsend`][Sender::beginsend]
and [`Recver::beginrecv`][Recver::beginrecv] can avoid unnecessary copies, and
result in more efficient sends/recvs.

Note that if you use `Sender::beginsend` and `Recver::beginrecv`, you also need
to call [`Sender::endsend`][Sender::endsend]/[`Recver::endrecv`][Recver::endrecv]
to actually commit a send/recv and notify the other side of the ring buffer.

``` rust
use elrond_helpers as elrond;

#[elrond::ring::sender("ring")]
static mut RING: elrond::ring::Sender = elrond::ring::Sender::detached();

fn main() {
    let len = "Chris was here!".len();
    loop {
        let buf = unsafe { &mut RING }.beginsend();
        if buf.len() >= len {
            write!(buf, "Chris was here!").unwrap();
            unsafe { &mut RING }.endsend(len);
            break;
        }
    }
}
```

``` rust
use elrond_helpers as elrond;

#[elrond::ring::recver("ring")]
static mut RING: elrond::ring::Recver = elrond::ring::Recver::detached();

fn main() {
    let buf = unsafe { &mut RING }.beginrecv();
    // prints: task2: recved 15
    // prints: task2: Chris was here!
    println!("task2: recved {}", buf.len());
    println!("task2: {}", str::from_utf8(buf));
    unsafe { &mut RING }.endrecv(buf.len());
}
```

### A note on Rust lifetimes

`Sender::beginsend` and `Recver::beginrecv` are an interesting case study in
how additional memory safety can be enforced by Rust's lifetime analysis.

Here is the function signature of `Sender::beginsend`:

``` rust
fn beginsend<'a>(&'a mut self) -> &'a mut [u8]
```

Technically, the lifetime annotation could be omitted here.
There's only one argument with a lifetime, so Rust would assume the return
value uses the same lifetime.
But I find explicitly annotating non-trivial lifetimes to be useful for
understanding how lifetimes flow through a function signature.

In this case, the function signature is saying that the return value shares
a lifetime with the mutable reference to self.
This means that the return value, a window of the available memory in the
ring buffer, can't live longer than the ring buffer itself.

Additionally, Rust enforces that there is only ever one mutable reference
to a given variable active at a time.

So while this is perfectly legal, buf is dropped before we call
`Sender::endsend`, which requires a second mutable reference:

``` rust
let buf = ring.beginsend();
buf[0] = 42;
ring.endsend(1);
```

This is illegal, as the call to `Sender::endsend` would require two mutable
references to be active simultaneously:

``` rust
let buf = ring.beginsend();
buf[0] = 42;
ring.endsend(1);
buf[0] = 142;
```

Rust catches this and reports it as an error:

```
error[E0499]: cannot borrow `ring` as mutable more than once at a time
  --> src/main.rs:12:5
   |
10 |     let buf = ring.beginsend();
   |               ---------------- first mutable borrow occurs here
11 |     buf[0] = 42;
12 |     ring.endsend(1);
   |     ^^^^^^^^^^^^^^^ second mutable borrow occurs here
13 |     buf[0] = 142;
   |     ------ first borrow later used here
```

This is good!
If this code could compile it would lead to a weird race condition where the
recieving side of the ring buffer could get either value.

Anyways, back to ring buffers.

`Recver::recv`/`Recver::beginrecv` will block until there is some data in the
ring buffer, and `Sender::send`/`Sender::beginsend` will block until there is
free space in the ring buffer.

Elrond-Rust-helpers also provides non-blocking variants, `Recver::nbrecv`,
`Recver::nbbeginrecv`, `Sender::nbsend`, `Sender::nbbeginsend`,
which return 0 bytes immediately if no data/free space is available.

``` rust
use elrond_helpers as elrond;

#[elrond::ring::sender("ring")]
static mut RING: elrond::ring::Sender = elrond::ring::Sender::detached();

fn main() {
    let len = "Chris was here!".len();
    loop {
        let buf = unsafe { &mut RING }.nbbeginsend();
        if buf.len() >= len {
            write!(buf, "Chris was here!").unwrap();
            unsafe { &mut RING }.endsend(len);
            break;
        }

        elrond::yield();
    }
}
```

``` rust
use elrond_helpers as elrond;

#[elrond::ring::recver("ring")]
static mut RING: elrond::ring::Recver = elrond::ring::Recver::detached();

fn main() {
    loop {
        let buf = unsafe { &mut RING }.nbbeginrecv();
        if buf.len() > 0 {
            // prints: task2: recved 15
            // prints: task2: Chris was here!
            println!("task2: recved {}", buf.len());
            println!("task2: {}", str::from_utf8(buf));
            unsafe { &mut RING }.endrecv(buf.len());
        }

        elrond::yield();
    }
}
```

[`Recver::avail`][Recver::avail] and [`Sender::avail`][Sender::avail] report
the current number of bytes ready to be recieved or sent, which can be useful
for deciding when to call the above functions.

``` rust
use elrond_helpers as elrond;

#[elrond::ring::sender("ring")]
static mut RING: elrond::ring::Sender = elrond::ring::Sender::detached();

fn main() {
    let len = "Chris was here!".len();
    while unsafe { &RING }.avail() < len {
        elrond::yield();
    }
    let buf = unsafe { &mut RING }.nbbeginsend();
    write!(buf, "Chris was here!").unwrap();
    unsafe { &mut RING }.endsend(len);
}
```

``` rust
use elrond_helpers as elrond;

#[elrond::ring::recver("ring")]
static mut RING: elrond::ring::Recver = elrond::ring::Recver::detached();

fn main() {
    loop {
        if unsafe { &RING }.avail() > 0 {
            let buf = unsafe { &mut RING }.nbbeginrecv();
            // prints: task2: recved 15
            // prints: task2: Chris was here!
            println!("task2: recved {}", buf.len());
            println!("task2: {}", str::from_utf8(buf));
            unsafe { &mut RING }.endrecv(buf.len());
        }

        elrond::yield();
    }
}
```

As probably expected by those familiar with Rust, `elrond_helpers::ring::Sender`
and `elrond_helpers::ring::Recver` implement the [`Write`][Write] and
[`Read`][Read] traits that represent generic streams of bytes.
This means `elrond_helpers::ring::Sender` and `elrond_helpers::ring::Recver`
can be used as a target for higher-order io operations built on those traits.

``` rust
use elrond_helpers as elrond;
use std::io::Write;

#[elrond::ring::sender("ring")]
static mut RING: elrond::ring::Sender = elrond::ring::Sender::detached();

fn main() {
    write!(unsafe { &mut RING }, "Chris was here!").unwrap();
}
```

``` rust
use elrond_helpers as elrond;
use std::io::Read;

#[elrond::ring::recver("ring")]
static mut RING: elrond::ring::Recver = elrond::ring::Recver::detached();

fn main() {
    let mut buf = [0u8; 256];
    let recved = unsafe { &mut RING }.read(&mut buf).unwrap();
    // prints: task2: recved 15
    // prints: task2: Chris was here!
    println!("task2: recved {}", recved);
    println!("task2: {}", str::from_utf8(&buf[..recved]));
}
```

If you're not a fan of boilerplate, Elrond's ring buffers can be hooked into
the above callback mechanism with [`elrond_helpers::ring::send_cb`][elrond_helpers::ring::send_cb]
and [`elrond_helpers::ring::recv_cb`][elrond_helpers::ring::recv_cb], to be
processed asynchronously.

``` c
elrond_ep_t *task1_ep;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_ep_t *task2_ep;
elrond_add_ep(&elrond, "task2", "main", &task2_ep);

elrond_add_ring(&elrond, "task1,task2",
        "ring1", 4096,
        task1_ep, NULL,
        task2_ep, NULL);
elrond_add_ring(&elrond, "task1,task2",
        "ring2", 4096,
        task1_ep, NULL,
        task2_ep, NULL);

elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
elrond_spawn(&elrond, "task2", task2_elf, task2_elf_size);
```

``` rust
use elrond_helpers as elrond;
use std::sync::atomic::AtomicU8;
use std::sync::atomic::Ordering;

#[elrond::ring::sender("ring1")]
static mut RING1: elrond::ring::Sender = elrond::ring::Sender::detached();

static COUNTER: AtomicU8 = AtomicU8::new(1);
#[elrond::ring::send_cb("ring2")]
fn ring2_send_cb(buf: &mut [u8]) -> usize {
    for i in 0..buf.len() {
        buf[i] = COUNTER.fetch_add(1, Ordering::Relaxed);
    }
    buf.len()
}

fn main() {
    unsafe { &mut RING1 }.send(b"abcdefghijklmnopqrstuvwxyz");
    elrond::spin();
}
```

``` rust
use elrond_helpers as elrond;
use std::io::Read;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;

static DONE: AtomicBool = AtomicBool::new(false);
#[elrond::ring::recv_cb("ring1")]
fn ring1_recv_cb(buf: &mut [u8]) -> usize {
    if !DONE.load(Ordering::Relaxed) && buf.len() >= 5 {
        // prints: task2: recved letters: a b c d e
        println!("task2: recved letters: {} {} {} {} {}",
            buf[0] as char,
            buf[1] as char,
            buf[2] as char,
            buf[3] as char,
            buf[4] as char,
        );
        DONE.store(true, Ordering::Relaxed);
        5
    } else {
        0
    }
}

#[elrond::ring::recver("ring2")]
static mut RING2: elrond::ring::Recver = elrond::ring::Recver::detached();

fn main() {
    let mut buf = [0u8; 5];
    unsafe { &mut RING2 }.read_exact(&mut buf).unwrap();
    // prints: task2: recved numbers: 1 2 3 4 5
    println!("task2: recved numbers: {} {} {} {} {}",
        buf[0], buf[1], buf[2], buf[3], buf[4]
    );
    elrond::spin();
}
```

Note that Elrond's ring buffers internally call `elrond::wait`, which is why
the above example doesn't deadlock!

## musl syscalls

Elrond also provides an overridable implementation of [musl][musl] syscalls.

Note this was based heavily on [libsel4muslcsys][libsel4muslcsys], which
provides musl syscalls relying only on the seL4 APIs.

Elrond can still load and run libsel4muslcsys processes without modification,
and Elrond-helpers can link against libsel4muslcsys if syscalls are disabled
in [elrond_helpers/CMakeLists.txt](CMakeLists.txt), however this would miss out
on the following extensions.

The way musl finds the syscalls is a bit roundabout.
When musl's `__init_libc` is called, somewhere between `_start` and `main`,
musl expects to find a pointer to the syscall entry point in the
[auxiliary vector][aux_vectors] `AT_SYSINFO`.
The auxiliary vectors follow the env array as a part of `_start`'s initial
stack frame, and Elrond fills this out at load time as a part of setting up
the process.
For `AT_SYSINFO`, Elrond simply copies the value found in the special section
`__vsyscall` in the ELF file.
This is compatible with both Elrond-helpers and libsel4muslcsys.

If you're using Elrond-helpers' syscalls, `AT_SYSINFO` ends up pointing to
the weakly-linked [`__elrond_syscall0`][__elrond_syscall] function, which
can be overwritten for full control of musl's syscalls.

If overwritten, [`__elrond_syscall0`][__elrond_syscall0] still provides the
original syscall logic.

``` c
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

``` rust
#![feature(c_variadic)]
use std::ffi::VaList;

extern "C" {
    fn __elrond_syscall0(sysnum: i64, args: VaList) -> i64;
}

#[no_mangle]
unsafe extern "C" fn __elrond_syscall(sysnum: i64, args: VaList) -> i64 {
    // NOTE we really can't call printf here!
    // prints: task1: syscall no 96
    // prints: task1: syscall no 29
    // prints: task1: syscall no 66
    // prints: task1: syscall no 214
    // prints: task1: syscall no 222
    seL4_DebugPutString("example-task4: syscall no ");
    let mut x = sysnum;
    let mut npw10 = 1;
    while x/npw10 >= 10 {
        npw10 *= 10;
    }
    while npw10 > 0 {
        seL4_DebugPutChar(b'0' + (x/npw10));
        x %= npw10;
        npw10 /= 10;
    }
    seL4_DebugPutString("\n");

    __elrond_syscall0(sysnum, args)
}

fn main() {
    // prints: task1: Hello world!
    println!("task1: Hello world!");
}
```

Internally, `__elrond_syscall` uses a big lookup table containing every syscall
as a weakly-linked function.
These functions, named `__elrond_sys_<syscall>`, can be individually overwritten
to customize specific syscalls.
And, just like `__elrond_syscall`, each of the original syscall implementations
can be accessed through `__elrond_sys0_<syscall>`, even if overwritten.

``` rust
#![feature(c_variadic)]
use std::ffi::VaList;

#[repr(C)]
struct IoVec {
    iov_base: *mut u8,
    iov_len: usize,
}

impl<'a> From<IoVec> for &'a [u8] {
    fn from(iov: IoVec) -> &'a [u8] {
        unsafe {
            std::slice::from_raw_parts(iov.iov_base, iov.iov_len)
        }
    }
}

extern "C" {
    fn __elrond_sys0_writev(args: VaList) -> i64;
}

#[no_mangle]
unsafe extern "C" fn __elrond_sys_writev(mut args: VaList) -> i64 {
    let (
        fd: i32,
        iov: &[IoVec],
    ) = args.with_copy(|| (
        args.arg::<i32>(),
        std::slice::from_raw_parts(
            args.arg::<*const IoVec>(),
            args.arg::<i32>() as usize,
        )
    ));

    if fd == 42 {
        let mut total = 0;
        for c in iov.iter().flat_map(|iov| <&[u8]>::from(iov).iter()) {
            print!("{}", (c as char).to_ascii_uppercase());
            total += 1;
        }
        total
    } else {
        __elrond_sys0_writev(args)
    }
}

fn main() {
    let mut f = unsafe { File::from_raw_fd(42) };
    // prints: TASK1: HELLO WORLD!
    writeln!(&mut f, "task1: Hello world!");
}
```

Note that at this point, the original meaning of these "syscalls" has been lost.
At no point does musl actually leave userspace when invoking a "syscall", unless
the underlying syscall implementation calls an seL4 API.

Unfortunately, as far as I can tell, there is no authoritative documentation
over the expected behavior of each syscall.
If this actually exists somewhere please let me know.
These are the best methods I've found for figuring what the heck a syscall
should do:

1. Dig around in the [source code][musl-syscall-src] of the related C API in
   musl to try to figure out how the well document C API maps to the underlying
   syscall invocation.

2. Dig around in the [source code][linux-syscall-src] of the Linux kernel or
   other implementation to try to figure out what it's doing.

Anyways, back to Elrond-Rust-helpers.

Elrond-Rust-helpers implements a number of syscalls, such that common libc
operations can leverage Elrond configuration optionally provided by the root
task.
The end result is a simple musl libc implementation that can be configured by
Elrond at load time.

### stdin/stdout/stderr

As the most important debugging tool, Elrond-Rust-helpers support redirecting
stdin, stderr, and stdout to optional ring buffers.

``` c
elrond_ep_t *root_ep;
elrond_add_ep(&elrond, NULL, "main", &root_ep);
elrond_ep_t *task1_ep;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);

elrond_ring_t stdin_ring;
elrond_add_ring(&elrond, "task1",
        "stdin", 4096,
        root_ep, NULL,
        task2_ep, &stdin_ring);

elrond_ring_t stdout_ring;
elrond_add_ring(&elrond, "task1",
        "stdout", 4096,
        task2_ep, &stdout_ring,
        root_ep, NULL);

elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

// prints: HHeelllloo  wwoorrlldd!!
size_t len = strlen("Hello world!\n");
size_t sent = elrond_ring_nbsend(&stdin_ring, "Hello world!\n", len);
assert(sent == len);
while (1) {
    seL4_Wait(elrond_ep_ntfn_ep(&elrond, root_ep, NULL));
    char *buf;
    size_t recved = elrond_ring_nbbeginrecv(&stdout_ring, (void**)&buf);
    printf("%.*s", (int)recved, buf);
    elrond_ring_endrecv(&stdout_ring, recved);
}
```

``` rust
fn main() {
    loop {
        let mut c = 0;
        std::io::stdin()
            .read_exact(std::slice::from_mut(&mut c))
            .expect("Failed to read stdin");
        print!("{}{}", c as char, c as char);
    }
}
```

If these ring buffers are not provided, Elrond will fall back to use the
reliable [`seL4_DebugPutString`][seL4_DebugPutString].
Though note that `seL4_DebugPutString` is only available in debug builds
of seL4.

### heap

By default, Elrond gives every process a heap configured by
[`elrond_add_heap_size`][elrond_add_heap_size].
This heap is provided to the process as a single region of memory, which
Elrond-helpers hands out via `brk` and `mmap` using a simple one-time
allocator.

musl then uses a combination of `brk` and `mmap` to find memory for its malloc
implementation.

``` c
elrond_add_heap_size(&elrond, "task1", 1024);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);
```

``` rust
fn main() {
    // prints: task1: malloc(100): 0x3272600
    println!("task1: malloc(100): {}",
        std::alloc::alloc(std::alloc::Layout::from_size_align(100, 1))
    );
    // prints: task1: malloc(10000): (null) 
    println!("task1: malloc(10000): {}",
        std::alloc::alloc(std::alloc::Layout::from_size_align(10000, 1))
    );
}
```

Note that a process's heap can be effectively disabled by setting its
heap_size to zero.
This can be used to save memory on no-malloc processes.

### yield/exit

Elrond-helpers also connects a number of syscalls to `elrond_helpers::yield`,
`elrond_helpers::abort`, etc.
This means any code relying on libc to yield will also give call/notification
callbacks a chance to run.

``` c
elrond_ep_t *task1_ep;
seL4_CPtr task1_ntfn1;
elrond_add_ep(&elrond, "task1", "main", &task1_ep);
elrond_add_ntfn(&elrond, "task1", "task1_ntfn1", task1_ep, &task1_ntfn1);
elrond_spawn(&elrond, "task1", task1_elf, task1_elf_size);

seL4_Signal(task1_ntfn1);
```

``` rust
use elrond_helpers as elrond;

#[elrond::ntfn("task1_ntfn1")]
fn task1_ntfn1() {
    // prints: task1: recved task1_ntfn1
    println!("task1: recved task1_ntfn1");
}

fn main() {
    // prints: task1: started
    println!("task1: started");
    loop {
        std::thread::yield_now();
    }
}
```


[rust]: https://www.rust-lang.org
[elrond]: ../elrond/README.md
[seL4]: https://sel4.systems
[musl]: https://musl.libc.org
[libsel4rustbindings]: ../libsel4rustbindings
[elrond_helpers]: ../elrond_helpers/README.md
[rust-bindgen]: https://github.com/rust-lang/rust-bindgen
[rust-weak]: https://github.com/rust-lang/rust/issues/29603
[no_std]: https://doc.rust-lang.org/stable/reference/names/preludes.html#the-no_std-attribute
[elrond_helpers::wait]: ../REFERENCE.md#elrond-rust-helper-functions
[std-whitelist]: https://github.com/rust-lang/rust/blob/038f9e6bef9c8fcf122d93a8a33ac546f5606eb3/library/std/build.rs
[build-std]: https://doc.rust-lang.org/nightly/cargo/reference/unstable.html#build-std
[toms-blog-post]: http://blog.timhutt.co.uk/std-embedded-rust/index.html
[elrond-info]: ../elrond/README.md#elrond-info-sections
[elrond_helpers::info]: ../REFERENCE.md#elrond-rust-helper-macros
[elrond-ipc]: ../elrond/README.md#configurating-ipc
[elrond_helpers::call_cb]: ../REFERENCE.md#elrond-rust-helper-macros
[elrond_helpers::ntfn_cb]: ../REFERENCE.md#elrond-rust-helper-macros
[elrond_helpers::poll]: ../REFERENCE.md#elrond-rust-helper-functions
[elrond_helpers::yield]: ../REFERENCE.md#elrond-rust-helper-functions
[seL4_Yield]: https://docs.sel4.systems/projects/sel4/api-doc.html#yield
[elrond_helpers::wait]: ../REFERENCE.md#elrond-rust-helper-functions
[elrond_helpers::spin]: ../REFERENCE.md#elrond-rust-helper-functions
[elrond_helpers::abort]: ../REFERENCE.md#elrond-rust-helper-functions
[__elrond_cb]: ../REFERENCE.md#elrond-helper-weak-functions
[__elrond_cb0]: ../REFERENCE.md#elrond-helper-weak-functions
[elrond-rings]: ../elrond/README.md#ring-buffers
[Sender::is_detached]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Recver::is_detached]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Sender::send]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Recver::recv]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Sender::beginsend]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Recver::beginrecv]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Sender::endsend]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Recver::endrecv]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Recver::avail]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Sender::avail]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-functions
[Write]: https://doc.rust-lang.org/std/io/trait.Write.html
[Read]: https://doc.rust-lang.org/std/io/trait.Read.html
[elrond_helpers::ring::send_cb]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-macros
[elrond_helpers::ring::recv_cb]: ../REFERENCE.md#elrond-rust-helper-ring-buffer-macros
[libsel4muslcsys]: https://github.com/seL4/seL4_libs/tree/master/libsel4muslcsys
[aux_vectors]: http://articles.manugarg.com/aboutelfauxiliaryvectors.html
[__elrond_syscall]: ../REFERENCE.md#elrond-helper-weak-functions
[__elrond_syscall0]: ../REFERENCE.md#elrond-helper-weak-functions
[musl-syscall-src]: https://git.musl-libc.org/cgit/musl/tree/src/linux/getrandom.c?id=4100279825c17807bdabf1c128ba4e49a1dea406#n4
[linux-syscall-src]: https://elixir.bootlin.com/linux/v5.18.14/source/drivers/char/random.c#L1249
[seL4_DebugPutString]: https://docs.sel4.systems/projects/sel4/api-doc.html#debugging-system-calls
[elrond_add_heap_size]: ../REFERENCE.md#elrond-functions

