//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

extern crate proc_macro;

use syn;
use syn::spanned::Spanned;
use quote;
use proc_macro2::*;

use std::sync::atomic::AtomicU32;
use std::sync::atomic::Ordering;

static COUNTER: AtomicU32 = AtomicU32::new(0);

fn crate_() -> syn::Path {
    syn::parse_quote! { ::elrond_helpers }
}


/// generate a .note.elrond.info section
fn gen_info(name: &str, ref_: &syn::Expr) -> TokenStream {
    let info_section = format!(".note.elrond.info.{}", name);
    let info_name = Ident::new(
        &format!("__elrond_info_{}", COUNTER.fetch_add(1, Ordering::Relaxed)),
        Span::call_site()
    );

    quote::quote! {
        #[used]
        #[link_section=#info_section]
        static #info_name: ::core::sync::atomic::AtomicPtr<u8> = unsafe {
            let ref_: &'static _ = &#ref_;
            ::core::sync::atomic::AtomicPtr::new(
                ref_ as *const _ as *const u8 as *mut u8
            )
        };
    }
}

struct InfoRefArgs {
    name: syn::LitStr,
    ref_: syn::Expr,
}

impl syn::parse::Parse for InfoRefArgs {
    fn parse(
        input: syn::parse::ParseStream
    ) -> Result<Self, syn::parse::Error> {
        let name = input.parse::<syn::LitStr>()?;
        input.parse::<syn::Token![,]>()?;
        // require &mut on the expression, we don't actually perform borrow
        // checking, but use this to hint that the value will change.
        input.parse::<syn::Token![&]>()?;
        input.parse::<syn::Token![mut]>()?;
        let ref_ = input.parse::<syn::Expr>()?;

        // trailing comma
        if input.peek(syn::Token![,]) {
            input.parse::<syn::Token![,]>()?;
        }
        
        Ok(InfoRefArgs{name, ref_})
    }
}

/// The info_ref macro, offers more control for writing into structs
///
/// ``` rust
/// static mut test_info: u32 = 0;
/// elrond::info_ref!("test_info", &test_info);
/// ```
///
#[proc_macro]
pub fn info_ref(
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let args = syn::parse_macro_input!(input as InfoRefArgs);

    let output = gen_info(&args.name.value(), &args.ref_);
    
    output.into()
}


/// The info macro
///
/// ``` rust
/// #[elrond::info("test_info")]
/// static mut test_info: u32 = 0;
/// ```
///
#[proc_macro_attribute]
pub fn info(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let name = syn::parse_macro_input!(args as syn::LitStr);
    let item = syn::parse_macro_input!(input as syn::ItemStatic);

    // info needs to be static mut, otherwise Rust may optimize
    // out the value
    if item.mutability.is_none() {
        return syn::Error::new(item.span(),
            "elrond info must be static mut, otherwise rustc will \
            optimize it away"
        ).into_compile_error().into();
    }

    let item_name = &item.ident;
    let ref_ = syn::parse_quote! { #item_name };

    let info = gen_info(&name.value(), &ref_);

    // make sure we don't forget the original item!
    let output = quote::quote! {
        #item
        #info
    };

    output.into()
}


/// The call_cb macro
///
/// ``` rust
/// #[elrond::call_cb("test_call_cb")]
/// fn test_call_cb(msg: seL4_MessageInfo_t) {
///     // blablabla
/// }
/// ```
///
#[proc_macro_attribute]
pub fn call_cb(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let name = syn::parse_macro_input!(args as syn::LitStr);
    let fn_ = syn::parse_macro_input!(input as syn::ItemFn);

    let crate_ = crate_();
    let fn_name = &fn_.sig.ident;
    let counter = COUNTER.fetch_add(1, Ordering::Relaxed);
    let cb_name = Ident::new(
        &format!("__elrond_info_{}", counter),
        Span::call_site()
    );
    let info = format!("{}_badge", name.value());

    let output = quote::quote! {
        #fn_

        #[used]
        #[link_section="__elrond_call_cbs"]
        static mut #cb_name: #crate_::ElrondCb = #crate_::ElrondCb {
            badge: 0,
            cb: #crate_::ElrondCbCb{call_cb: #fn_name},
        };

        #crate_::info_ref!(#info, &mut #cb_name.badge);
    };

    output.into()
}


/// The ntfn_cb macro
///
/// ``` rust
/// #[elrond::ntfn_cb("test_ntfn_cb")]
/// fn test_ntfn_cb() {
///     // blablabla
/// }
/// ```
///
#[proc_macro_attribute]
pub fn ntfn_cb(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let name = syn::parse_macro_input!(args as syn::LitStr);
    let fn_ = syn::parse_macro_input!(input as syn::ItemFn);

    let crate_ = crate_();
    let fn_name = &fn_.sig.ident;
    let counter = COUNTER.fetch_add(1, Ordering::Relaxed);
    let cb_name = Ident::new(
        &format!("__elrond_info_{}", counter),
        Span::call_site()
    );
    let info = format!("{}_badge", name.value());

    let output = quote::quote! {
        #fn_

        #[used]
        #[link_section="__elrond_ntfn_cbs"]
        static mut #cb_name: #crate_::ElrondCb = #crate_::ElrondCb {
            badge: 0,
            cb: #crate_::ElrondCbCb{ntfn_cb: #fn_name},
        };

        #crate_::info_ref!(#info, &mut #cb_name.badge);
    };

    output.into()
}


/// The ring::sender macro
///
/// ``` rust
/// #[elrond::ring::sender("test_ring")]
/// static mut test_ring: elrond::ring::Sender
///     = elrond::ring::Sender::detached();
/// ```
///
#[proc_macro_attribute]
pub fn sender(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let name = syn::parse_macro_input!(args as syn::LitStr);
    let ring = syn::parse_macro_input!(input as syn::ItemStatic);

    // info needs to be static mut, otherwise Rust may optimize
    // out the value
    if ring.mutability.is_none() {
        return syn::Error::new(ring.span(),
            "elrond info must be static mut, otherwise rustc will \
            optimize it away"
        ).into_compile_error().into();
    }

    let crate_ = crate_();
    let ring_name = &ring.ident;
    let ntfn_name = format!("{}_send_ntfn", name.value());
    let control_name = format!("{}_control", name.value());
    let buffer_name = format!("{}_buffer", name.value());
    let size_name = format!("{}_size", name.value());

    let output = quote::quote! {
        #ring

        #crate_::info_ref!(#ntfn_name, &mut #ring_name.ntfn);
        #crate_::info_ref!(#control_name, &mut #ring_name.control);
        #crate_::info_ref!(#buffer_name, &mut #ring_name.buffer);
        #crate_::info_ref!(#size_name, &mut #ring_name.size);
    };

    output.into()
}

/// The ring::recver macro
///
/// ``` rust
/// #[elrond::ring::recver("test_ring")]
/// static mut test_ring: elrond::ring::Recver
///     = elrond::ring::Recver::detached();
/// ```
///
#[proc_macro_attribute]
pub fn recver(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let name = syn::parse_macro_input!(args as syn::LitStr);
    let ring = syn::parse_macro_input!(input as syn::ItemStatic);

    // info needs to be static mut, otherwise Rust may optimize
    // out the value
    if ring.mutability.is_none() {
        return syn::Error::new(ring.span(),
            "elrond info must be static mut, otherwise rustc will \
            optimize it away"
        ).into_compile_error().into();
    }

    let crate_ = crate_();
    let ring_name = &ring.ident;
    let ntfn_name = format!("{}_recv_ntfn", name.value());
    let control_name = format!("{}_control", name.value());
    let buffer_name = format!("{}_buffer", name.value());
    let size_name = format!("{}_size", name.value());

    let output = quote::quote! {
        #ring

        #crate_::info_ref!(#ntfn_name, &mut #ring_name.ntfn);
        #crate_::info_ref!(#control_name, &mut #ring_name.control);
        #crate_::info_ref!(#buffer_name, &mut #ring_name.buffer);
        #crate_::info_ref!(#size_name, &mut #ring_name.size);
    };

    output.into()
}

/// The send_cb macro
///
/// ``` rust
/// #[elrond::ring::send_cb("test_ring")]
/// fn send_cb(ring: &mut elrond::ring::Sender, data: &mut [u8]) {
///     // blablabla
/// }
/// ```
/// 
#[proc_macro_attribute]
pub fn send_cb(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let name = syn::parse_macro_input!(args as syn::LitStr);
    let fn_ = syn::parse_macro_input!(input as syn::ItemFn);

    let crate_ = crate_();
    let counter = COUNTER.fetch_add(1, Ordering::Relaxed);
    let fn_name = &fn_.sig.ident;
    let ntfn_name = format!("{}_recv_ntfn", name.value());
    let ring_name = Ident::new(
        &format!("__elrond_ring_{}", counter),
        Span::call_site()
    );
    let handler_name = Ident::new(
        &format!("__elrond_ring_handler_{}", counter),
        Span::call_site()
    );

    let output = quote::quote! {
        #fn_

        #[#crate_::ring::sender(#name)]
        static mut #ring_name: #crate_::ring::Sender
            = #crate_::ring::Sender::detached();

        #[#crate_::ntfn_cb(#ntfn_name)]
        fn #handler_name() {
            let _: fn(data: &mut [u8]) -> usize = #fn_name;
            let ring = unsafe { &mut #ring_name };
            let len = {
                let mut buf = ring.nbbeginsend();
                if buf.len() > 0 {
                    #fn_name(buf)
                } else {
                    0
                }
            };
            ring.endsend(len);
        }
    };

    output.into()
}

/// The ring::recv_cb macro
///
/// ``` rust
/// #[elrond::ring::recv_cb("test_ring")]
/// fn recv_cb(ring: &mut elrond::ring::Recver, data: &mut [u8]) {
///     // blablabla
/// }
/// ```
/// 
#[proc_macro_attribute]
pub fn recv_cb(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream
) -> proc_macro::TokenStream {
    let name = syn::parse_macro_input!(args as syn::LitStr);
    let fn_ = syn::parse_macro_input!(input as syn::ItemFn);

    let crate_ = crate_();
    let counter = COUNTER.fetch_add(1, Ordering::Relaxed);
    let fn_name = &fn_.sig.ident;
    let ntfn_name = format!("{}_send_ntfn", name.value());
    let ring_name = Ident::new(
        &format!("__elrond_ring_{}", counter),
        Span::call_site()
    );
    let handler_name = Ident::new(
        &format!("__elrond_ring_handler_{}", counter),
        Span::call_site()
    );

    let output = quote::quote! {
        #fn_

        #[#crate_::ring::recver(#name)]
        static mut #ring_name: #crate_::ring::Recver
            = #crate_::ring::Recver::detached();

        #[#crate_::ntfn_cb(#ntfn_name)]
        fn #handler_name() {
            let _: fn(data: &mut [u8]) -> usize = #fn_name;
            let ring = unsafe { &mut #ring_name };
            let len = {
                let mut buf = ring.nbbeginrecv();
                if buf.len() > 0 {
                    #fn_name(buf)
                } else {
                    0
                }
            };
            ring.endrecv(len);
        }
    };

    output.into()
}

