//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use libsel4rustbindings::*;
use elrond_helpers_macros;


// other modules
pub mod ring;


// constants
pub const NTFN_MASK: seL4_Word      = 0x8000000000000000;
pub const CALL_MAX: seL4_Word       = 0x7fffffffffffffff;
pub const NTFN_MAX: seL4_Word       = 63;
#[allow(non_snake_case)]
pub const fn CALL_BADGE(x: seL4_Word) -> seL4_Word {
    x + 1
}
#[allow(non_snake_case)]
pub const fn NTFN_BADGE(x: seL4_Word) -> seL4_Word {
    NTFN_MASK | (1 << x)
}
pub const WAKEUP_BADGE: seL4_Word   = 0x8000000000000000;
pub const NULL_BADGE: seL4_Word     = 0x0000000000000000;

// predefined capability slots
pub const CAP_NULL: seL4_Word            = 0;
pub const CAP_MAIN_TCB: seL4_Word        = 1;
pub const CAP_ASID_POOL: seL4_Word       = 2;
pub const CAP_CSPACE_ROOT: seL4_Word     = 3;
pub const CAP_VSPACE_ROOT: seL4_Word     = 4;
pub const CAP_FAULT_EP: seL4_Word        = 5;
pub const CAP_MAIN_CALL_EP: seL4_Word    = 6;
pub const CAP_MAIN_NTFN_EP: seL4_Word    = 7;
pub const RESERVED_CAP_COUNT: seL4_Word  = 8;


// this type is used by the call_cb/ntfn_cb macros
#[doc(hidden)]
#[repr(C)]
pub union ElrondCbCb {
    pub call_cb: fn(seL4_MessageInfo_t),
    pub ntfn_cb: fn(),
}

#[doc(hidden)]
#[repr(C)]
pub struct ElrondCb {
    pub badge: seL4_Word,
    pub cb: ElrondCbCb,
}


// re-exported from elrond_helpers_macro, this is because proc_macros must
// be in their own crate
pub use elrond_helpers_macros::info;
pub use elrond_helpers_macros::info_ref;
pub use elrond_helpers_macros::call_cb;
pub use elrond_helpers_macros::ntfn_cb;


// normal elrond functions

/// poll for any outstanding calls/notifications
pub fn poll() {
    extern "C" {
        fn elrond_poll();
    }

    unsafe { elrond_poll() }
}

/// give other threads a chance to run, and also poll calls/notifications
pub fn yield_() {
    extern "C" {
        fn elrond_yield();
    }

    unsafe { elrond_yield() }
}

/// wait until new calls/notifications
pub fn wait() {
    extern "C" {
        fn elrond_wait();
    }

    unsafe { elrond_wait() }
}

/// spin waiting for new calls/notifications
pub fn spin() -> ! {
    extern "C" {
        fn elrond_spin() -> !;
    }

    unsafe { elrond_spin() }
}

/// abort our process
pub fn abort() -> ! {
    extern "C" {
        fn elrond_abort() -> !;
    }

    unsafe { elrond_abort() }
}
