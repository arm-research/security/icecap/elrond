//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use libsel4rustbindings::*;

use std::fmt;
use std::io;
use std::cmp::min;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;
use std::slice;
use std::ptr;

// re-exported from elrond_helpers_macro
pub use elrond_helpers_macros::sender;
pub use elrond_helpers_macros::recver;
pub use elrond_helpers_macros::send_cb;
pub use elrond_helpers_macros::recv_cb;


#[repr(align(64))]
#[derive(Debug)]
struct RingOff(AtomicUsize);

#[repr(C)]
#[derive(Debug)]
#[doc(hidden)]
pub struct RingControl {
    sent: RingOff,
    recv: RingOff,
}



/// Sending side of a Ring
pub struct Sender {
    #[doc(hidden)] pub control: *mut RingControl,
    #[doc(hidden)] pub buffer: *mut u8,
    #[doc(hidden)] pub size: usize,
    #[doc(hidden)] pub ntfn: seL4_CPtr,
}

impl fmt::Debug for Sender {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Sender")
            .field("control", &unsafe { self.control.as_ref() })
            .field("buffer", &self.buffer)
            .field("size", &self.size)
            .field("ntfn", &self.ntfn)
            .finish()
    }
}

impl Default for Sender {
    fn default() -> Self {
        Self::detached()
    }
}

impl Sender {
    // create detached
    #[inline]
    pub const fn detached() -> Self {
        Self {
            control: ptr::null_mut(),
            buffer: ptr::null_mut(),
            size: 0,
            ntfn: 0,
        }
    }

    // create from parts
    #[inline]
    pub const unsafe fn from_parts(
        control: *mut u8,
        buffer: *mut u8,
        size: usize,
        ntfn: seL4_CPtr
    ) -> Self {
        Self {
            control: control as *mut RingControl,
            buffer: buffer,
            size: size,
            ntfn: ntfn,
        }
    }

    // accessors
    #[inline]
    pub fn is_detached(&self) -> bool {
        self.control.is_null()
    }

    #[inline]
    pub fn control(&self) -> *mut u8 {
        self.control as *mut u8
    }

    #[inline]
    pub fn buffer(&self) -> *mut u8 {
        self.buffer
    }

    #[inline]
    pub fn size(&self) -> usize {
        self.size
    }

    #[inline]
    pub fn ntfn(&self) -> seL4_CPtr {
        self.ntfn
    }

    // ring operations
    pub fn send(&mut self, data: &[u8]) -> usize {
        let send = self.beginsend();
        if send.len() == 0 {
            return 0;
        }

        // send at most size
        let delta = min(send.len(), data.len());
        send[..delta].copy_from_slice(&data[..delta]);
        self.endsend(delta);
        delta
    }

    pub fn nbsend(&mut self, data: &[u8]) -> usize {
        let send = self.nbbeginsend();
        if send.len() == 0 {
            return 0;
        }

        // send at most size
        let delta = min(send.len(), data.len());
        send[..delta].copy_from_slice(&data[..delta]);
        self.endsend(delta);
        delta
    }

    pub fn avail(&self) -> usize {
        unsafe { &mut *(self as *const Self as *mut Self) }
            .nbbeginsend().len()
    }

    // here's the fun lifetime functions
    pub fn beginsend<'a>(&'a mut self) -> &'a mut [u8] {
        while self.avail() == 0 {
            // note that any notification can wake us up from here
            crate::wait();
        }

        self.nbbeginsend()
    }

    pub fn nbbeginsend<'a>(&'a mut self) -> &'a mut [u8] {
        // Note that recv/sent are wrapping around at 2xsize, but can only
        // ever be size distant. This avoids the empty/full ambiguity
        debug_assert!(!self.is_detached());
        let recv = unsafe { &*self.control }.recv.0.load(Ordering::Acquire);
        let sent = unsafe { &*self.control }.sent.0.load(Ordering::Relaxed);
        let mut used = (sent as isize) - (recv as isize);
        if used < 0 {
            used += 2*(self.size as isize);
        }

        unsafe {
            slice::from_raw_parts_mut(
                self.buffer.add(sent & (self.size-1)),
                self.size - (used as usize)
            )
        }
    }

    pub fn endsend(&mut self, size: usize) {
        // update sent
        debug_assert!(!self.control.is_null());
        let sent = unsafe { &*self.control }.sent.0.load(Ordering::Relaxed);
        let sent = (sent + size) & ((2*self.size)-1);
        unsafe { &*self.control }.sent.0.store(sent, Ordering::Release);

        // signal notification object
        if self.ntfn != 0 {
            unsafe { seL4_Signal(self.ntfn) };
        }
    }
}

// std io Read/Write traits
impl io::Write for Sender {
    fn write(&mut self, data: &[u8]) -> Result<usize, io::Error> {
        Ok(self.send(data))
    }

    fn flush(&mut self) -> Result<(), io::Error> {
        Ok(())
    }
}



/// Recving side of a Ring
pub struct Recver {
    #[doc(hidden)] pub control: *mut RingControl,
    #[doc(hidden)] pub buffer: *mut u8,
    #[doc(hidden)] pub size: usize,
    #[doc(hidden)] pub ntfn: seL4_CPtr,
}

impl fmt::Debug for Recver {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Recver")
            .field("control", &unsafe { self.control.as_ref() })
            .field("buffer", &self.buffer)
            .field("size", &self.size)
            .field("ntfn", &self.ntfn)
            .finish()
    }
}

impl Default for Recver {
    #[inline]
    fn default() -> Self {
        Self::detached()
    }
}

impl Recver {
    // create detached
    #[inline]
    pub const fn detached() -> Self {
        Self {
            control: ptr::null_mut(),
            buffer: ptr::null_mut(),
            size: 0,
            ntfn: 0,
        }
    }

    // create from parts
    #[inline]
    pub const unsafe fn from_parts(
        control: *mut u8,
        buffer: *mut u8,
        size: usize,
        ntfn: seL4_CPtr
    ) -> Self {
        Self {
            control: control as *mut RingControl,
            buffer: buffer,
            size: size,
            ntfn: ntfn,
        }
    }

    // accessors
    #[inline]
    pub fn is_detached(&self) -> bool {
        self.control.is_null()
    }

    #[inline]
    pub fn control(&self) -> *mut u8 {
        self.control as *mut u8
    }

    #[inline]
    pub fn buffer(&self) -> *mut u8 {
        self.buffer
    }

    #[inline]
    pub fn size(&self) -> usize {
        self.size
    }

    #[inline]
    pub fn ntfn(&self) -> seL4_CPtr {
        self.ntfn
    }

    // ring operations
    pub fn recv(&mut self, data: &mut [u8]) -> usize {
        let recv = self.beginrecv();
        if recv.len() == 0 {
            return 0;
        }

        // recv at most size
        let delta = min(recv.len(), data.len());
        data[..delta].copy_from_slice(&recv[..delta]);
        self.endrecv(delta);
        delta
    }

    pub fn nbrecv(&mut self, data: &mut [u8]) -> usize {
        let recv = self.nbbeginrecv();
        if recv.len() == 0 {
            return 0;
        }

        // recv at most size
        let delta = min(recv.len(), data.len());
        data[..delta].copy_from_slice(&recv[..delta]);
        self.endrecv(delta);
        delta
    }

    pub fn avail(&self) -> usize {
        unsafe { &mut *(self as *const Self as *mut Self) }
            .nbbeginrecv().len()
    }

    // here's the fun lifetime functions
    pub fn beginrecv<'a>(&'a mut self) -> &'a mut [u8] {
        while self.avail() == 0 {
            // note that any notification can wake us up from here
            crate::wait();
        }

        self.nbbeginrecv()
    }

    pub fn nbbeginrecv<'a>(&'a mut self) -> &'a mut [u8] {
        // Note that recv/sent are wrapping around at 2xsize, but can only
        // ever be size distant. This avoids the empty/full ambiguity
        debug_assert!(!self.is_detached());
        let recv = unsafe { &*self.control }.recv.0.load(Ordering::Relaxed);
        let sent = unsafe { &*self.control }.sent.0.load(Ordering::Acquire);
        let mut used = (sent as isize) - (recv as isize);
        if used < 0 {
            used += 2*(self.size as isize);
        }

        unsafe {
            slice::from_raw_parts_mut(
                self.buffer.add(recv & (self.size-1)),
                used as usize
            )
        }
    }

    pub fn endrecv(&mut self, size: usize) {
        // update recv
        debug_assert!(!self.control.is_null());
        let recv = unsafe { &*self.control }.recv.0.load(Ordering::Relaxed);
        let recv = (recv + size) & ((2*self.size)-1);
        unsafe { &*self.control }.recv.0.store(recv, Ordering::Release);

        // signal notification object
        if self.ntfn != 0 {
            unsafe { seL4_Signal(self.ntfn) };
        }
    }
}

// std io Read/Write traits
impl io::Read for Recver {
    fn read(&mut self, data: &mut [u8]) -> Result<usize, io::Error> {
        Ok(self.recv(data))
    }
}


