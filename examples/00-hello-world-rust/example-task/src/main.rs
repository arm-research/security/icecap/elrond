//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use elrond_helpers as elrond;
use libsel4rustbindings::*;


fn main() {
    // if you hit here, things are probably working
    println!("example-task: Hello world!");

    // don't segfault
    unsafe { seL4_TCB_Suspend(elrond::CAP_MAIN_TCB) };
}

