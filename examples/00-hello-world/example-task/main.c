//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>


int main(int argc, char **argv) {
    // if you hit here, things are probably working
    printf("example-task: Hello world!\n");

    // don't segfault
    seL4_TCB_Suspend(ELROND_CAP_MAIN_TCB);
}
