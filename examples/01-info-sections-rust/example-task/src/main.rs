//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use elrond_helpers as elrond;
use libsel4rustbindings::*;


#[elrond::info("test_info")]
static mut TEST_INFO: u32 = 0;


fn main() {
    // elrond info working?
    println!("example-task: test_info: 0x{:x}", unsafe { TEST_INFO });

    // don't segfault
    unsafe { seL4_TCB_Suspend(elrond::CAP_MAIN_TCB) };
}

