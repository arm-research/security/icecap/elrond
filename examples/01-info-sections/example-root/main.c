//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <sel4/sel4.h>
#include <vka/object.h>
#include <vspace/vspace.h>
#include <cpio/cpio.h>

#include <elrond.h>
#include <elrond_ring.h>


// CPIO archive embedded by build system
extern const uint8_t _cpio_archive[];
extern const uint8_t _cpio_archive_end[];


int main(void) {
    seL4_DebugPutString("booting...\n");

    // bootstrap the root task and elrond
    elrond_t elrond;
    int err = elrond_bootstrap(&elrond, "example-root");
    assert(!err);
    vka_t *vka = elrond_vka(&elrond);
    vspace_t *vspace = elrond_vspace(&elrond);
    printf("example-root: successfully bootstrapped\n");

    // find elf file in CPIO archive
    size_t task_elf_size = 0;
    char const *task_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task",
            &task_elf_size);

    // add an info section
    elrond_add_info(&elrond, "*",  "test_info", &(uint32_t){0x12345678}, 4);

    // spawn process
    elrond_spawn(&elrond, "example-task", task_elf, task_elf_size);
    seL4_Yield();

    // print for debugging
//    elrond_debugdump(&elrond, NULL);
//    seL4_DebugDumpScheduler();

    // don't segfault
    seL4_TCB_Suspend(seL4_CapInitThreadTCB);
}
