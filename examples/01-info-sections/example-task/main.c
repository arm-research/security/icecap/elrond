//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdint.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>


uint32_t test_info;
ELROND_INFO("test_info", &test_info);


int main(int argc, char **argv) {
    // elrond info working?
    printf("example-task: test_info: 0x%08x\n", test_info);

    // don't segfault                       
    seL4_TCB_Suspend(ELROND_CAP_MAIN_TCB);
}
