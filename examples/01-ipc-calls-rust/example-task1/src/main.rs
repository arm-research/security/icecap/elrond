//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use elrond_helpers as elrond;
use libsel4rustbindings::*;


#[elrond::info("test_call1")]
static mut TEST_CALL1: seL4_CPtr = 0;
#[elrond::info("test_call2")]
static mut TEST_CALL2: seL4_CPtr = 0;
#[elrond::info("test_ntfn1")]
static mut TEST_NTFN1: seL4_CPtr = 0;


fn main() {
    // calling with response
    println!("example-task1: calling {}+{}...", 42, 43);
    unsafe {
        seL4_SetMR(0, 42);
        seL4_SetMR(1, 43);
        let msg = seL4_Call(TEST_CALL1, seL4_MessageInfo_new(0, 0, 0, 2));
        assert_eq!(seL4_MessageInfo_get_length(msg), 1);
    }
    println!("example-task1: recved {}+{} = {}", 42, 43, unsafe { seL4_GetMR(0) });

    // sending without response
    println!("example-task1: sending {}...", unsafe { seL4_GetMR(0) });
    unsafe {
        seL4_SetMR(0, seL4_GetMR(0));
        seL4_Send(TEST_CALL2, seL4_MessageInfo_new(0, 0, 0, 1));
    }

    // sending asynchronous notification without response
    println!("example-task1: notifying...");
    unsafe {
        seL4_Signal(TEST_NTFN1);
    }

    // don't segfault
    unsafe { seL4_TCB_Suspend(elrond::CAP_MAIN_TCB) };
}

