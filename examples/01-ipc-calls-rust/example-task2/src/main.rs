//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

#![no_main]

use elrond_helpers as elrond;
use libsel4rustbindings::*;


#[elrond::call_cb("test_call1")]
fn test_call1(msg: seL4_MessageInfo_t) {
    println!("example-task2: test_call1!");
    unsafe { 
        println!("example-task2: recved {}+{}", seL4_GetMR(0), seL4_GetMR(1));
        assert_eq!(seL4_MessageInfo_get_length(msg), 2);
        seL4_SetMR(0, seL4_GetMR(0)+seL4_GetMR(1));
        seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 1));
    }
}

#[elrond::call_cb("test_call2")]
fn test_call2(_msg: seL4_MessageInfo_t) {
    println!("example-task2: test_call2!");
    unsafe {
        println!("example-task2: recved {}", seL4_GetMR(0));
    }
}

#[elrond::ntfn_cb("test_ntfn1")]
fn test_ntfn1() {
    println!("example-task2: test_ntfn1!");
}
