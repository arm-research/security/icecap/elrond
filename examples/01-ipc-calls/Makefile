

## Build the project
.PHONY: all build
all build:
	mkdir -p build
	$(strip cd build && \
		../init-build.sh \
			-DCMAKE_TOOLCHAIN_FILE=seL4/gcc.cmake \
			-DTRIPLE=aarch64-linux-gnu \
			-DAARCH64=TRUE \
			-DPLATFORM=qemu-arm-virt \
			-DSIMULATION=TRUE)
	$(strip cd build && \
		ninja)

## Show this help text
.PHONY: help
help:
	@$(strip awk '/^## / { \
		sub(/^## /,""); \
		getline t; \
		if (t ~ /^\.PHONY/) getline t; \
		gsub(/:.*/,"",t); \
		printf " "" %-26s %s\n",t,$$0}' \
		$(MAKEFILE_LIST))

## Run under QEMU
.PHONY: run
run:
	$(strip cd build && \
		qemu-system-aarch64 \
			-machine virt \
			-cpu cortex-a53 \
			-nographic \
			-m size=1024 \
			-kernel images/*)

## Run under QEMU with a debugger
.PHONY: debug
debug:
	fuser -k 1234/tcp || true
	$(strip cd build && \
		qemu-system-aarch64 \
			-machine virt \
			-cpu cortex-a53 \
			-nographic \
			-m size=1024 \
			-kernel images/* \
			-gdb tcp::1234 \
			-S &)
	$(strip gdb-multiarch \
			build/images/* \
			-ex "target remote :1234")
	fuser -k 1234/tcp || true

## Clean any generated files
.PHONY: clean
clean:
	rm -rf build

