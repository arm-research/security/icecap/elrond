
cmake_minimum_required(VERSION 3.7.2)

set(target example-root)
file(GLOB sources *.c)

include(cpio)
list(APPEND cpio_files
    $<TARGET_FILE:example-task1>
    $<TARGET_FILE:example-task2>
)
MakeCPIO(
    archive.o
    "${cpio_files}"
)

add_executable(${target} ${sources} archive.o)
target_link_libraries(${target}
    sel4
    muslc
    utils
    sel4muslcsys
    sel4platsupport
    sel4utils
    sel4debug
    sel4allocman
    elrond
)

