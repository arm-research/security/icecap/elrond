//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>


seL4_CPtr test_call1;
ELROND_INFO("test_call1", &test_call1);
seL4_CPtr test_call2;
ELROND_INFO("test_call2", &test_call2);
seL4_CPtr test_ntfn1;
ELROND_INFO("test_ntfn1", &test_ntfn1);


int main(int argc, char **argv) {
    // calling with response
    printf("example-task1: calling %d+%d...\n", 42, 43);
    seL4_SetMR(0, 42);
    seL4_SetMR(1, 43);
    seL4_MessageInfo_t msg = seL4_Call(test_call1, 
            seL4_MessageInfo_new(0, 0, 0, 2));
    assert(seL4_MessageInfo_get_length(msg) == 1);
    printf("example-task1: recved %d+%d = %ld\n", 42, 43, seL4_GetMR(0));

    // sending without response
    printf("example-task1: sending %ld...\n", seL4_GetMR(0));
    seL4_SetMR(0, seL4_GetMR(0));
    seL4_Send(test_call2, seL4_MessageInfo_new(0, 0, 0, 1));

    // sending asynchronous notification without response
    printf("example-task1: notifying...\n");
    seL4_Signal(test_ntfn1);

    // don't segfault                       
    seL4_TCB_Suspend(ELROND_CAP_MAIN_TCB);
}
