//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>


void test_call1(seL4_MessageInfo_t msg) {
    printf("example-task2: test_call1!\n");
    printf("example-task2: recved %ld+%ld\n", seL4_GetMR(0), seL4_GetMR(1));
    assert(seL4_MessageInfo_get_length(msg) == 2);
    seL4_SetMR(0, seL4_GetMR(0)+seL4_GetMR(1));
    seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 1));
}
ELROND_CALL_CB("test_call1", test_call1);

void test_call2(seL4_MessageInfo_t msg) {
    printf("example-task2: test_call2!\n");
    printf("example-task2: recved %ld\n", seL4_GetMR(0));
}
ELROND_CALL_CB("test_call2", test_call2);

void test_ntfn1(void) {
    printf("example-task2: test_ntfn1!\n");
}
ELROND_NTFN_CB("test_ntfn1", test_ntfn1);
