
cmake_minimum_required(VERSION 3.7.2)

set(target example-task1)

# HACK to work around Rust's random need to link against libunwind
# for no reason
file(WRITE ${CMAKE_BINARY_DIR}/libunwind.a "")

# defer to Cargo
add_custom_command(
    OUTPUT
    ${CMAKE_CURRENT_BINARY_DIR}/aarch64-sel4-linux-musl/debug/${target}
    WORKING_DIRECTORY
    ${CMAKE_CURRENT_SOURCE_DIR}
    COMMAND ${CMAKE_COMMAND} -E env
        "CARGO_TARGET_DIR=\"${CMAKE_BINARY_DIR}/rust\""
        "C_INCLUDE_DIRS=\"$<TARGET_PROPERTY:${target},INCLUDE_DIRECTORIES>\""
        "C_LIBRARIES=\"$<GENEX_EVAL:$<TARGET_PROPERTY:${target},TRANSITIVE_LINK_LIBRARIES>>\""
        "C_LINK_FLAGS=\"$<TARGET_PROPERTY:${target},TRANSITIVE_LINK_OPTIONS>\""
        cargo +nightly build
            --target=${CMAKE_SOURCE_DIR}/libsel4rustbindings/targets/aarch64-sel4-linux-musl.json
            -Zbuild-std=core,alloc,std,panic_abort
    COMMAND_EXPAND_LISTS
    USES_TERMINAL
)
add_custom_target(${target}-crate ALL
    DEPENDS
    ${CMAKE_CURRENT_BINARY_DIR}/aarch64-sel4-linux-musl/debug/${target}
)


# HACK cmake can't use custom commands to make "executables", which
# are apparently very special targets. We need this to be "executable"
# so that MakeCPIO works elsewhere in the build. So...
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/null.c "void __sel4_start_c(void) {}")
add_executable(${target} ${CMAKE_CURRENT_BINARY_DIR}/null.c)
add_dependencies(${target} ${target}-crate)
target_link_libraries(${target}
    sel4
    sel4runtime
    muslc
    utils
    elrond_helpers
)
add_custom_command(
    TARGET ${target}
    POST_BUILD
    COMMAND rm $<TARGET_FILE:${target}>
    COMMAND cp
        ${CMAKE_BINARY_DIR}/rust/aarch64-sel4-linux-musl/debug/${target}
        $<TARGET_FILE:${target}>
)


# NOTE There is no way to just "get" the link libraries, which is disapointing.
# This is flaky but the best we can do.
#
# https://gitlab.kitware.com/cmake/cmake/-/issues/12435
#
function(get_transitive_link_libraries target_)
    get_property(libraries TARGET ${target_} PROPERTY LINK_LIBRARIES)
    foreach(library ${libraries})
        if(TARGET ${library})
            # already visited?
            get_property(transitive_link_libraries
                TARGET ${target}
                PROPERTY TRANSITIVE_LINK_LIBRARIES
            )
            list(FIND transitive_link_libraries "$<TARGET_FILE:${library}>" visited)
            if(${visited} EQUAL -1)
                add_dependencies(${target}-crate ${library})
                get_property(library_type TARGET ${library} PROPERTY TYPE)
                if(library_type STREQUAL "STATIC_LIBRARY")
                    set_property(
                        TARGET ${target}
                        APPEND PROPERTY TRANSITIVE_LINK_LIBRARIES
                        "$<TARGET_FILE:${library}>"
                    )
                    get_property(library_options TARGET ${library} PROPERTY LINK_OPTIONS)
                    set_property(
                        TARGET ${target}
                        APPEND PROPERTY TRANSITIVE_LINK_OPTIONS
                        ${library_options}
                    )

                    # recurse to try to find any transitive libraries
                    get_transitive_link_libraries(${library})
                endif()
            endif()
        endif()
    endforeach()
endfunction()
get_transitive_link_libraries(${target})


