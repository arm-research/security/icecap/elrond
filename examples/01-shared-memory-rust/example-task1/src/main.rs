//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use elrond_helpers as elrond;
use libsel4rustbindings::*;


#[elrond::info("test_shared")]
static mut TEST_SHARED: *mut u8 = std::ptr::null_mut();
#[elrond::info("test_call")]
static mut TEST_CALL: seL4_CPtr = 0;


fn main() {
    // write to shared memory
    let test_shared = unsafe {
        core::slice::from_raw_parts_mut(TEST_SHARED, b"Chris was here!".len())
    };
    test_shared.copy_from_slice(b"Chris was here!");

    // notify that we're done
    println!("example-task1: sending \"{}\"...", core::str::from_utf8(test_shared).unwrap());
    unsafe {
        seL4_SetMR(0, test_shared.len() as u64);
        seL4_Send(TEST_CALL, seL4_MessageInfo_new(0, 0, 0, 1));
    }

    // don't segfault
    unsafe { seL4_TCB_Suspend(elrond::CAP_MAIN_TCB) };
}

