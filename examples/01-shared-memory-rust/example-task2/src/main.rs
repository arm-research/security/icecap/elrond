//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

#![no_main]

use elrond_helpers as elrond;
use libsel4rustbindings::*;


#[elrond::info("test_shared")]
static mut TEST_SHARED: *mut u8 = std::ptr::null_mut();


#[elrond::call_cb("test_call")]
fn test_call(msg: seL4_MessageInfo_t) {
    println!("example-task2: test_call!");
    unsafe {
        assert_eq!(seL4_MessageInfo_get_length(msg), 1);
        println!("example-task2: {}", core::str::from_utf8(
            core::slice::from_raw_parts(
                TEST_SHARED,
                seL4_GetMR(0) as usize
            )
        ).unwrap());
    }
}
