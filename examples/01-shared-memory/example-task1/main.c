//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <string.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>


char *test_shared = NULL;
ELROND_INFO("test_shared", &test_shared);
seL4_CPtr test_call;
ELROND_INFO("test_call", &test_call);


int main(int argc, char **argv) {
    // write to shared memory
    strcpy(test_shared, "Chris was here!");

    // notify that we're done
    printf("example-task1: sending \"%s\"...\n", test_shared);
    seL4_SetMR(0, strlen(test_shared));
    seL4_Send(test_call, seL4_MessageInfo_new(0, 0, 0, 1));

    // don't segfault                       
    seL4_TCB_Suspend(ELROND_CAP_MAIN_TCB);
}
