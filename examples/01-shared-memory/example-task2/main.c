//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>


char *test_shared = NULL;               
ELROND_INFO("test_shared", &test_shared);


void test_call(seL4_MessageInfo_t msg) {
    printf("example-task2: test_call!\n");
    assert(seL4_MessageInfo_get_length(msg) == 1);
    printf("example-task2: %.*s\n", (int)seL4_GetMR(0), test_shared);
}
ELROND_CALL_CB("test_call", test_call);
