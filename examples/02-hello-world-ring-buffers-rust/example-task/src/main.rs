//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use std::io::Read;


fn main() {
    loop {
        let mut c = 0;
        std::io::stdin()
            .read_exact(std::slice::from_mut(&mut c))
            .expect("Failed to read stdin");
        print!("{}{}", c as char, c as char);
    }
}
