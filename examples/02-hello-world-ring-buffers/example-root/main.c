//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <sel4/sel4.h>
#include <vka/object.h>
#include <vspace/vspace.h>
#include <cpio/cpio.h>

#include <elrond.h>
#include <elrond_ring.h>
#include <sel4platsupport/bootinfo.h>


// CPIO archive embedded by build system
extern const uint8_t _cpio_archive[];
extern const uint8_t _cpio_archive_end[];


int main(void) {
    seL4_DebugPutString("booting...\n");

    // bootstrap the root task and elrond
    elrond_t elrond;
    int err = elrond_bootstrap(&elrond, "example-root");
    assert(!err);
    vka_t *vka = elrond_vka(&elrond);
    vspace_t *vspace = elrond_vspace(&elrond);
    printf("example-root: successfully bootstrapped\n");

    // find elf file in CPIO archive
    size_t task_elf_size = 0;
    char const *task_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task",
            &task_elf_size);

    // setup stdin/stdout
    elrond_ep_t *root_ep;
    elrond_add_ep(&elrond, NULL, "main", &root_ep);
    elrond_ep_t *task_ep;
    elrond_add_ep(&elrond, "example-task", "main", &task_ep);

    elrond_ring_t stdin_ring;
    elrond_add_ring(&elrond, "example-task",
            "stdin", 4096,
            root_ep, NULL,
            task_ep, &stdin_ring);

    elrond_ring_t stdout_ring;
    elrond_add_ring(&elrond, "example-task",
            "stdout", 4096,
            task_ep, &stdout_ring,
            root_ep, NULL);

    // spawn process
    elrond_spawn(&elrond, "example-task", task_elf, task_elf_size);
    seL4_Yield();

    // print for debugging
//    elrond_debugdump(&elrond, NULL);
//    seL4_DebugDumpScheduler();

    size_t len = strlen("Hello world!\n");
    size_t sent = elrond_ring_nbsend(&stdin_ring, "Hello world!\n", len);
    assert(sent == len);
    while (1) {
        seL4_Wait(elrond_ep_ntfn_ep(&elrond, root_ep), NULL);
        char *buf;
        size_t recved = elrond_ring_nbbeginrecv(&stdout_ring, (void**)&buf);
        printf("%.*s", (int)recved, buf);
        elrond_ring_endrecv(&stdout_ring, recved);
    }

    // don't segfault
    seL4_TCB_Suspend(seL4_CapInitThreadTCB);
}
