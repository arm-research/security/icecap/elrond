//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>


int main(void) {
    while (1) {
        char c = getchar();
        printf("%c%c", c, c);
    }
}
