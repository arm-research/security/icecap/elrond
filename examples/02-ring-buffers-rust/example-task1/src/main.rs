//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use elrond_helpers as elrond;

use std::sync::atomic::AtomicU8;
use std::sync::atomic::Ordering;


#[elrond::ring::sender("ring1")]
static mut RING1: elrond::ring::Sender = elrond::ring::Sender::detached();

static COUNTER: AtomicU8 = AtomicU8::new(1);
#[elrond::ring::send_cb("ring2")]
fn ring2_send_cb(buf: &mut [u8]) -> usize {
    println!("example-task1: sending numbers...");
    for i in 0..buf.len() {
        buf[i] = COUNTER.fetch_add(1, Ordering::Relaxed);
    }
    buf.len()
}


fn main() {
    println!("example-task1: sending letters...");
    unsafe { &mut RING1 }.send(b"abcdefghijklmnopqrstuvwxyz");
    println!("example-task1: spinning...");
    elrond::spin();
}
