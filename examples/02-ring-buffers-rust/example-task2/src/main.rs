//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use elrond_helpers as elrond;

use std::io::Read;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;


static DONE: AtomicBool = AtomicBool::new(false);
#[elrond::ring::recv_cb("ring1")]
fn ring1_recv_cb(buf: &mut [u8]) -> usize {
    if !DONE.load(Ordering::Relaxed) && buf.len() >= 5 {
        println!("example-task2: recved letters: {} {} {} {} {}",
            buf[0] as char,
            buf[1] as char,
            buf[2] as char,
            buf[3] as char,
            buf[4] as char,
        );
        DONE.store(true, Ordering::Relaxed);
        5
    } else {
        0
    }
}

#[elrond::ring::recver("ring2")]
static mut RING2: elrond::ring::Recver = elrond::ring::Recver::detached();


fn main() {
    let mut buf = [0u8; 5];
    unsafe { &mut RING2 }.read_exact(&mut buf).unwrap();
    println!("example-task2: recved numbers: {} {} {} {} {}",
        buf[0], buf[1], buf[2], buf[3], buf[4]
    );
    println!("example-task2: spinning...");
    elrond::spin();
}
