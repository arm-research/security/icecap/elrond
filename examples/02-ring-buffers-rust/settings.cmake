#
# Copyright 2018, Data61, CSIRO (ABN 41 687 119 230)
#
# SPDX-License-Identifier: BSD-2-Clause
#

include_guard(GLOBAL)

list(
    APPEND
        CMAKE_MODULE_PATH
        "${CMAKE_CURRENT_LIST_DIR}/seL4"
        "${CMAKE_CURRENT_LIST_DIR}/seL4_tools/cmake-tool/helpers"
        "${CMAKE_CURRENT_LIST_DIR}/seL4_tools/elfloader-tool"
        "${CMAKE_CURRENT_LIST_DIR}/musllibc"
        "${CMAKE_CURRENT_LIST_DIR}/seL4_libs"
        "${CMAKE_CURRENT_LIST_DIR}/sel4runtime"
        "${CMAKE_CURRENT_LIST_DIR}/util_libs"
        "${CMAKE_CURRENT_LIST_DIR}/elrond"
        "${CMAKE_CURRENT_LIST_DIR}/elrond_helpers"
)

include(application_settings)

set(SIMULATION OFF CACHE BOOL "Simulate")
set(RELEASE OFF CACHE BOOL "Performance optimized build")
set(VERIFICATION OFF CACHE BOOL "Only verification friendly kernel features")
set(DOMAINS OFF CACHE BOOL "Multiple domains")
set(SMP OFF CACHE BOOL "SMP kernel")
set(NUM_NODES "" CACHE STRING "The number of nodes (default 4)")
set(PLATFORM "x86_64" CACHE STRING "Platform")
set(ARM_HYP OFF CACHE BOOL "Hyp mode for ARM platforms")
set(MCS OFF CACHE BOOL "MCS kernel")
set(KernelSel4Arch "" CACHE STRING "aarch32, aarch64, arm_hyp, ia32, x86_64, riscv32, riscv64")

#set(QEMU_MEMORY 2048)


# Begin seL4 configuration
correct_platform_strings()

find_package(seL4 REQUIRED)
sel4_configure_platform_settings()

set(valid_platforms ${KernelPlatform_all_strings} ${correct_platform_strings_platform_aliases})
set_property(CACHE PLATFORM PROPERTY STRINGS ${valid_platforms})
if(NOT "${PLATFORM}" IN_LIST valid_platforms)
    message(FATAL_ERROR "Invalid PLATFORM selected: \"${PLATFORM}\"
Valid platforms are: \"${valid_platforms}\"")
endif()

# We use 'FORCE' when settings these values instead of 'INTERNAL' so that they still appear
# in the cmake-gui to prevent excessively confusing users
if(ARM_HYP)
    set(KernelArmHypervisorSupport ON CACHE BOOL "" FORCE)
endif()

if(KernelPlatformQEMUArmVirt)
    set(SIMULATION ON CACHE BOOL "" FORCE)
endif()

if(SIMULATION)
    ApplyCommonSimulationSettings(${KernelSel4Arch})
else()
    if(KernelArchX86)
        set(KernelIOMMU ON CACHE BOOL "" FORCE)
    endif()
endif()

# Check the hardware debug API non simulated (except for ia32, which can be simulated),
# or platforms that don't support it.
if(((NOT SIMULATION) OR KernelSel4ArchIA32) AND NOT KernelHardwareDebugAPIUnsupported)
    set(HardwareDebugAPI ON CACHE BOOL "" FORCE)
else()
    set(HardwareDebugAPI OFF CACHE BOOL "" FORCE)
endif()

ApplyCommonReleaseVerificationSettings(${RELEASE} ${VERIFICATION})

if(DOMAINS)
    set(KernelNumDomains 16 CACHE STRING "" FORCE)
else()
    set(KernelNumDomains 1 CACHE STRING "" FORCE)
endif()

if(SMP)
    if(NUM_NODES MATCHES "^[0-9]+$")
        set(KernelMaxNumNodes ${NUM_NODES} CACHE STRING "" FORCE)
    else()
        set(KernelMaxNumNodes 4 CACHE STRING "" FORCE)
    endif()
else()
    set(KernelMaxNumNodes 1 CACHE STRING "" FORCE)
endif()

if(MCS)
    set(KernelIsMCS ON CACHE BOOL "" FORCE)
else()
    set(KernelIsMCS OFF CACHE BOOL "" FORCE)
endif()

# enable debug printing
set(LibSel4PlatSupportUseDebugPutChar true CACHE BOOL "" FORCE)

# set root task cnode size
set(KernelRootCNodeSizeBits 16 CACHE STRING "" FORCE) 

# set morecore to zero, this is so we can customize the head size in elrond
set(LibSel4MuslcSysMorecoreBytes 0 CACHE STRING "" FORCE)

# force syscalls to not be inlined so that Rust can link to them
set(LibSel4FunctionAttributes public CACHE STRING "" FORCE)
