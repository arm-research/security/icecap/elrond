//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <sel4/sel4.h>
#include <vka/object.h>
#include <vspace/vspace.h>
#include <cpio/cpio.h>

#include <elrond.h>
#include <elrond_ring.h>


// CPIO archive embedded by build system
extern const uint8_t _cpio_archive[];
extern const uint8_t _cpio_archive_end[];


int main(void) {
    seL4_DebugPutString("booting...\n");

    // bootstrap the root task and elrond
    elrond_t elrond;
    int err = elrond_bootstrap(&elrond, "example-root");
    assert(!err);
    vka_t *vka = elrond_vka(&elrond);
    vspace_t *vspace = elrond_vspace(&elrond);
    printf("example-root: successfully bootstrapped\n");

    // find elf file in CPIO archive
    size_t task1_elf_size = 0;
    char const *task1_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task1",
            &task1_elf_size);
    size_t task2_elf_size = 0;
    char const *task2_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task2",
            &task2_elf_size);

    // create shared memory and notification
    elrond_ep_t *task1_ep;
    elrond_add_ep(&elrond, "example-task1", "main", &task1_ep);
    elrond_ep_t *task2_ep;
    elrond_add_ep(&elrond, "example-task2", "main", &task2_ep);
    elrond_add_ring(&elrond, "example-task1,example-task2",
            "ring1", 4096,
            task1_ep, NULL,
            task2_ep, NULL);
    elrond_add_ring(&elrond, "example-task1,example-task2",
            "ring2", 4096,
            task1_ep, NULL,
            task2_ep, NULL);

    // spawn process
    elrond_spawn(&elrond, "example-task1", task1_elf, task1_elf_size);
    elrond_spawn(&elrond, "example-task2", task2_elf, task2_elf_size);
    seL4_Yield();

    // print for debugging
//    elrond_debugdump(&elrond, NULL);
//    seL4_DebugDumpScheduler();

    // don't segfault
    seL4_TCB_Suspend(seL4_CapInitThreadTCB);
}
