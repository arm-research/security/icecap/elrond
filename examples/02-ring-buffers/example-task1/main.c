//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <string.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>
#include <elrond_ring.h>


elrond_ring_t ring1;
ELROND_RING_SENDER("ring1", &ring1);

uint8_t counter = 1;
size_t ring2_send_cb(void *buf, size_t size) {
    printf("example-task1: sending numbers...\n");
    uint8_t *buf_ = buf;
    for (size_t i = 0; i < size; i++) {
        buf_[i] = counter;
        counter += 1;
    }
    return size;
}
ELROND_RING_SEND_CB("ring2", ring2_send_cb);


int main(void) {
    printf("example-task1: sending letters...\n");
    elrond_ring_send(&ring1, "abcdefghijklmnopqrstuvwxyz", 26);
    printf("example-task1: spinning...\n");
    elrond_spin();
}
