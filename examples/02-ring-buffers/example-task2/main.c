//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>
#include <elrond_ring.h>


bool done = false;
size_t ring1_recv_cb(void *buf, size_t size) {
    if (!done && size >= 5) {
        char *buf_ = buf;
        printf("example-task2: recved letters: %c %c %c %c %c\n",
                buf_[0], buf_[1], buf_[2], buf_[3], buf_[4]);
        done = true;
        return 5;
    }
    return 0;
}
ELROND_RING_RECV_CB("ring1", ring1_recv_cb);

elrond_ring_t ring2;
ELROND_RING_RECVER("ring2", &ring2);


int main(void) {
    uint8_t *buf;
    while (elrond_ring_beginrecv(&ring2, (void**)&buf) < 5) {}
    printf("example-task2: recved numbers: %d %d %d %d %d\n",
            buf[0], buf[1], buf[2], buf[3], buf[4]);
    elrond_ring_endrecv(&ring2, 5);

    printf("example-task2: spinning...\n");
    elrond_spin();
}
