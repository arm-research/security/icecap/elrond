//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <sel4/sel4.h>
#include <elrond_helpers.h>
#include <elrond_ring.h>


elrond_ring_t random_ring;
ELROND_RING_RECVER("random_ring", &random_ring);


#define COUNT 10

int main(void) {
    uint8_t buf[COUNT];
    elrond_ring_recv(&random_ring, buf, COUNT);

    printf("example-task2: random bytes: ");
    for (size_t i = 0; i < COUNT; i++) {
        printf("%02x", buf[i]);
    }
    printf("\n");

    // don't segfault
    seL4_TCB_Suspend(ELROND_CAP_MAIN_TCB);
}
