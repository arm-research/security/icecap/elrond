#
# Copyright 2018, Data61, CSIRO (ABN 41 687 119 230)
#
# SPDX-License-Identifier: BSD-2-Clause
#

include(settings.cmake)

cmake_minimum_required(VERSION 3.7.2)

project(elrond-example C ASM)

find_package(seL4 REQUIRED)
find_package(elfloader-tool REQUIRED)
find_package(musllibc REQUIRED)
find_package(util_libs REQUIRED)
find_package(seL4_libs REQUIRED)
find_package(elrond REQUIRED)
find_package(elrond_helpers REQUIRED)

sel4_import_kernel()
elfloader_import_project()
elrond_import_project()
elrond_helpers_import_project()

# This sets up environment build flags and imports musllibc and runtime libraries.
musllibc_setup_build_environment_with_sel4runtime()
sel4_import_libsel4()
util_libs_import_libraries()
sel4_libs_import_libraries()

# subdirectories
add_subdirectory(root-task)
add_subdirectory(runner-task)
add_subdirectory(random-task)
add_subdirectory(maze-task)
add_subdirectory(ant-task)

# assign root task
include(rootserver)
DeclareRootserver(root-task)

include(simulation)
GenerateSimulateScript()
