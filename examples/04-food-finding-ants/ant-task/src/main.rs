//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

#![no_main]

use elrond_helpers as elrond;
use libsel4rustbindings::*;

use core::f32::consts::PI;
use rand::{Rng, RngCore};


#[elrond::ring::recver("ant_random_ring")]
static mut RANDOM_RING: elrond::ring::Recver = elrond::ring::Recver::detached();

struct RingRng(&'static mut elrond::ring::Recver);

impl RngCore for RingRng {
    fn fill_bytes(&mut self, dest: &mut [u8]) {
        self.0.recv(dest);
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand::Error> {
        self.0.recv(dest);
        Ok(())
    }

    fn next_u32(&mut self) -> u32 {
        let mut buf = [0; 4];
        self.fill_bytes(&mut buf);
        u32::from_le_bytes(buf)
    }

    fn next_u64(&mut self) -> u64 {
        let mut buf = [0; 8];
        self.fill_bytes(&mut buf);
        u64::from_le_bytes(buf)
    }
}

fn rng() -> RingRng {
    RingRng(unsafe { &mut RANDOM_RING })
}


#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Ant {
    x: f32,
    y: f32,
    heading_x: f32,
    heading_y: f32,
    has_food: bool
}

impl Ant {
    fn move_(&mut self, s: f32) {
        self.x += s*self.heading_x;
        self.y += s*self.heading_y;
    }

    fn rotate(&mut self, a: f32) {
        let x = self.heading_x;
        let y = self.heading_y;
        let cos = a.cos();
        let sin = a.sin();
        self.heading_x = x*cos - y*sin;
        self.heading_y = x*sin + y*cos;
    }

    fn normalize(&mut self) {
        let x = self.heading_x;
        let y = self.heading_y;
        let len = (x*x + y*y).sqrt();

        // if length is zero we choose a random direction
        //
        // sort of a hack but also useful
        if len == 0.0 {
            let heading = rng().gen_range(0.0..2.0*PI);
            self.heading_x = heading.cos();
            self.heading_y = heading.sin();
            return;
        }

        self.heading_x /= len;
        self.heading_y /= len;
    }
}

#[elrond::info("ant_count")]
static mut ANT_COUNT: usize = 0;
#[elrond::info("ant_speed")]
static mut ANT_SPEED: f32 = 0.0;
#[elrond::info("ant_wander")]
static mut ANT_WANDER: f32 = 0.0;
#[elrond::info("ant_turn")]
static mut ANT_TURN: f32 = 0.0;
#[elrond::info("ant_sight")]
static mut ANT_SIGHT: f32 = 0.0;
#[elrond::info("ants")]
static mut ANTS: *mut Ant = core::ptr::null_mut();

fn ants() -> &'static mut [Ant] {
    unsafe {
        core::slice::from_raw_parts_mut(ANTS, ANT_COUNT)
    }
}


#[elrond::info("maze_width")]
static mut MAZE_WIDTH: usize = 0;
#[elrond::info("maze_height")]
static mut MAZE_HEIGHT: usize = 0;
#[elrond::info("maze")]
static mut MAZE: *mut bool = core::ptr::null_mut();

fn cell(x: usize, y: usize) -> Option<&'static mut bool> {
    unsafe {
        // in bounds?
        if x >= MAZE_WIDTH || y >= MAZE_HEIGHT {
            return None;
        }

        Some(&mut *MAZE.add(x*MAZE_WIDTH + y))
    }
}


#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Pheremone {
    food: f32,
    home: f32,
}

#[elrond::info("pheremone_decay")]
static mut PHEREMONE_DECAY: f32 = 0.0;
#[elrond::info("pheremones")]
static mut PHEREMONES: *mut Pheremone = core::ptr::null_mut();

fn pheremones() -> impl Iterator<Item=(usize, usize, &'static mut Pheremone)> {
    unsafe {
        (0..MAZE_WIDTH).flat_map(|x| {
            (0..MAZE_HEIGHT).map(move |y| {
                (x, y, &mut *PHEREMONES.add(x*MAZE_WIDTH + y))
            })
        })
    }
}

fn pheremone(x: usize, y: usize) -> Option<&'static mut Pheremone> {
    unsafe {
        if x >= MAZE_WIDTH || y >= MAZE_HEIGHT {
            return None;
        }

        Some(&mut *PHEREMONES.add(x*MAZE_WIDTH + y))
    }
}


// ant simulation based on https://www.youtube.com/watch?v=81GQNPJip2Y
#[elrond::info("ant_step_done")]
static mut ANT_STEP_DONE: seL4_CPtr = 0;
#[elrond::call_cb("ant_step")]
fn ant_step(msg: seL4_MessageInfo_t) {
    assert_eq!(unsafe { seL4_MessageInfo_get_length(msg) }, 4);
    let home_x = unsafe { seL4_GetMR(0) } as usize;
    let home_y = unsafe { seL4_GetMR(1) } as usize;
    let food_x = unsafe { seL4_GetMR(2) } as usize;
    let food_y = unsafe { seL4_GetMR(3) } as usize;
    let speed = unsafe { ANT_SPEED };
    let wander = unsafe { ANT_WANDER } * 2.0*PI;
    let turn = unsafe { ANT_TURN } * 2.0*PI;
    let sight = unsafe { ANT_SIGHT } as usize;

    for ant in ants() {
        // update ant position

        // reset the ant in some situations
        ant.normalize();

        // pheremones nearby?
        let mut best_delta = 0.0;
        let mut best_weight = -1.0;
        //let mut best_pheremone = 0.0;
        for ray in [-turn, -0.5*turn, 0.0, 0.5*turn, turn] {
            for ray_len in (1 ..= sight).map(|x| x as f32 + 0.5) {
                let ray_cos = ray.cos();
                let ray_sin = ray.sin();
                let ray_heading_x = ant.heading_x*ray_cos - ant.heading_y*ray_sin;
                let ray_heading_y = ant.heading_x*ray_sin + ant.heading_y*ray_cos;
                let ray_x = (ant.x + ray_len*ray_heading_x) as isize as usize;
                let ray_y = (ant.y + ray_len*ray_heading_y) as isize as usize;
                let delta = ray_heading_y.atan2(ray_heading_x)
                    - ant.heading_y.atan2(ant.heading_x);

                // found food or home?
                if ant.has_food && ray_x == home_x && ray_y == home_y {
                    best_delta = delta;
                    best_weight = 1.0;
                }

                if !ant.has_food && ray_x == food_x && ray_y == food_y {
                    best_delta = delta;
                    best_weight = 1.0;
                }

                // found pheremone?
                if let Some(pheremone) = pheremone(ray_x, ray_y) {
                    if ((ant.has_food && pheremone.home > 0.0)
                            || (!ant.has_food && pheremone.food > 0.0))
                        && best_weight <= 0.5
                        && (best_weight < 0.5
                            //|| (ant.has_food && best_pheremone < pheremone.home)
                            //|| (!ant.has_food && best_pheremone < pheremone.food)
                            || best_delta.abs() > delta.abs())
                    {
                        best_weight = 0.5;
                        best_delta = delta;
                        //best_pheremone = if ant.has_food { pheremone.home } else { pheremone.food };
                    }
                }

                // no wall is slightly better than a wall
                if cell(ray_x, ray_y).is_some() && !*cell(ray_x, ray_y).unwrap()
                    && best_weight <= 0.0
                    && (best_weight < 0.0
                        || best_delta.abs() > delta.abs())
                {
                    best_weight = 0.0;
                    best_delta = delta;
                }
            }
        }

        ant.rotate(best_delta);

        // leave pheremones
        let x = ant.x as isize as usize;
        let y = ant.y as isize as usize;
        if let Some(pheremone) = pheremone(x, y) {
            if ant.has_food {
                pheremone.food = 1.0;
            } else {
                pheremone.home = 1.0;
            }
        }

        // actually move
        ant.move_(speed);

        // wander a bit
        ant.rotate(rng().gen_range(-wander/2.0 ..= wander/2.0));
    }

    // decay pheremones
    for (_, _, pheremone) in pheremones() {
        if pheremone.food > 0.0 {
            pheremone.food -= unsafe { PHEREMONE_DECAY };
        }
        if pheremone.home > 0.0 {
            pheremone.home -= unsafe { PHEREMONE_DECAY };
        }
    }

    unsafe { seL4_Send(ANT_STEP_DONE, seL4_MessageInfo_new(0, 0, 0, 0)) };
}

