//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

#![no_main]

use elrond_helpers as elrond;
use libsel4rustbindings::*;

use rand::{Rng, RngCore};


#[elrond::ring::recver("maze_random_ring")]
static mut RANDOM_RING: elrond::ring::Recver = elrond::ring::Recver::detached();

struct RingRng(&'static mut elrond::ring::Recver);

impl RngCore for RingRng {
    fn fill_bytes(&mut self, dest: &mut [u8]) {
        self.0.recv(dest);
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand::Error> {
        self.0.recv(dest);
        Ok(())
    }

    fn next_u32(&mut self) -> u32 {
        let mut buf = [0; 4];
        self.fill_bytes(&mut buf);
        u32::from_le_bytes(buf)
    }

    fn next_u64(&mut self) -> u64 {
        let mut buf = [0; 8];
        self.fill_bytes(&mut buf);
        u64::from_le_bytes(buf)
    }
}

fn rng() -> RingRng {
    RingRng(unsafe { &mut RANDOM_RING })
}

#[elrond::info("maze_width")]
static mut MAZE_WIDTH: usize = 0;
#[elrond::info("maze_height")]
static mut MAZE_HEIGHT: usize = 0;
#[elrond::info("maze_prune_iterations")]
static mut MAZE_PRUNE_ITERATIONS: u32 = 0;
#[elrond::info("maze_erode_iterations")]
static mut MAZE_ERODE_ITERATIONS: u32 = 0;
#[elrond::info("maze_food_iterations")]
static mut MAZE_FOOD_ITERATIONS: u32 = 0;
#[elrond::info("maze")]
static mut MAZE: *mut bool = core::ptr::null_mut();

fn maze() -> &'static mut [bool] {
    unsafe {
        core::slice::from_raw_parts_mut(MAZE, MAZE_WIDTH*MAZE_HEIGHT)
    }
}

fn cell(x: usize, y: usize) -> Option<&'static mut bool> {
    unsafe {
        // in bounds?
        if x >= MAZE_WIDTH || y >= MAZE_HEIGHT {
            return None;
        }

        Some(&mut *MAZE.add(x*MAZE_WIDTH + y))
    }
}


// maze generation based on https://kairumagames.com/blog/cavetutorial
#[elrond::info("maze_gen_done")]
static mut MAZE_GEN_DONE: seL4_CPtr = 0;
#[elrond::call_cb("maze_gen")]
fn maze_gen(msg: seL4_MessageInfo_t) {
    assert_eq!(unsafe { seL4_MessageInfo_get_length(msg) }, 0);

    // find a random start
    let home_x = rng().gen_range(0..unsafe { MAZE_WIDTH });
    let home_y = rng().gen_range(0..unsafe { MAZE_HEIGHT });
    let mut x = home_x;
    let mut y = home_y;

    // reset maze
    maze().fill(true);

    // keep track of visited cells
    let mut unvisited = Vec::new();

    // generate maze with Prim's algorithm
    for cycle in 0.. {
        // clear cell
        *cell(x, y).unwrap() = false;

        // add any unvisited neighbors
        let mut visit = |x, y| {
            if cell(x, y).is_some()
                && *cell(x, y).unwrap()
                && !unvisited.contains(&(x, y))
            {
                unvisited.push((x, y));
            }
        };
        visit(x, y+2);
        visit(x+2, y);
        visit(x, y.wrapping_sub(2));
        visit(x.wrapping_sub(2), y);

        // done?
        if unvisited.len() == 0 {
            break;
        }

        // choose a random new cell
        (x, y) = unvisited.swap_remove(rng().gen_range(0..unvisited.len()));

        // randomly teardown a wall
        let (wall_x, wall_y) = loop {
            let choose = |x, y| {
                cell(x, y).is_some()
                    && !*cell(x, y).unwrap()
                    && rng().gen_bool(0.5)
            };

            if choose(x, y+2)               { break (x, y+1); }
            if choose(x+2, y)               { break (x+1, y); }
            if choose(x, y.wrapping_sub(2)) { break (x, y-1); }
            if choose(x.wrapping_sub(2), y) { break (x-1, y); }
        };
        *cell(wall_x, wall_y).unwrap() = false;
        if cycle % 10 == 0 {
            elrond::yield_();
        }
    }

    // prune dead ends
    for _ in 0..unsafe { MAZE_PRUNE_ITERATIONS } {
        let mut prune = Vec::new();
        for x in 0..unsafe { MAZE_WIDTH } {
            for y in 0..unsafe { MAZE_HEIGHT } {
                if !*cell(x, y).unwrap() {
                    let mut neighbors = 0;
                    let mut visit = |x, y| {
                        if cell(x, y).is_some() && !*cell(x, y).unwrap() {
                            neighbors += 1;
                        }
                    };
                    visit(x, y+1);
                    visit(x+1, y);
                    visit(x, y.wrapping_sub(1));
                    visit(x.wrapping_sub(1), y);

                    if neighbors <= 1 {
                        prune.push((x, y));
                    }
                }
            }
        }

        // two passes to avoid clearing whole lines
        for (x, y) in prune {
            *cell(x, y).unwrap() = true;
        }
        elrond::yield_();
    }

    // erode into larger blobs
    for _ in 0..unsafe { MAZE_ERODE_ITERATIONS } {
        let mut erode = Vec::new();
        for x in 0..unsafe { MAZE_WIDTH } {
            for y in 0..unsafe { MAZE_HEIGHT } {
                if *cell(x, y).unwrap() {
                    let mut neighbors = 0;
                    for delta_x in -1 ..= 1 {
                        for delta_y in -1 ..= 1 {
                            let x_ = (x as isize + delta_x) as usize;
                            let y_ = (y as isize + delta_y) as usize;
                            if cell(x_, y_).is_some() && !*cell(x_, y_).unwrap() {
                                neighbors += 1;
                            }
                        }
                    }

                    if neighbors >= 4 {
                        erode.push((x, y));
                    }
                }
            }
        }

        // two passes to avoid clearing whole lines
        for (x, y) in erode {
            *cell(x, y).unwrap() = false;
        }
        elrond::yield_();
    }

    // choose random location for food
    //
    // select the best of n tries, whichever is furthest
    let mut food_x = home_x;
    let mut food_y = home_y;
    for _ in 0..unsafe { MAZE_FOOD_ITERATIONS } {
        let (food_x_, food_y_) = loop {
            let food_x_ = rng().gen_range(0..unsafe { MAZE_WIDTH });
            let food_y_ = rng().gen_range(0..unsafe { MAZE_HEIGHT });
            if cell(food_x_, food_y_).is_some() && !*cell(food_x_, food_y_).unwrap() {
                break (food_x_, food_y_);
            }
        };
        let dx = food_x as isize - home_x as isize;
        let dy = food_y as isize - home_y as isize;
        let dx_ = food_x_ as isize - home_x as isize;
        let dy_ = food_y_ as isize - home_y as isize;
        if dx_*dx_ + dy_*dy_ > dx*dx + dy*dy {
            food_x = food_x_;
            food_y = food_y_;
        }
    } 

    unsafe { seL4_SetMR(0, home_x as seL4_Word) };
    unsafe { seL4_SetMR(1, home_y as seL4_Word) };
    unsafe { seL4_SetMR(2, food_x as seL4_Word) };
    unsafe { seL4_SetMR(3, food_y as seL4_Word) };
    unsafe { seL4_Send(MAZE_GEN_DONE, seL4_MessageInfo_new(0, 0, 0, 4)) };
}
