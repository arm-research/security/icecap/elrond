//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

#![no_main]
#![feature(c_variadic)]

use elrond_helpers as elrond;
use rand::{self, RngCore};
use std::ffi::VaList;


#[elrond::info("random_seed")]
static mut RANDOM_SEED: [u8; 4] = [0; 4];


// intercept getrandom syscall
#[no_mangle]
unsafe extern "C" fn __elrond_sys_getrandom(mut args: VaList) -> i64 {
    let buf: *mut u8 = args.arg::<*const u8>() as *mut u8;
    let len: usize = args.arg::<usize>();
    let _flags: u32 = args.arg::<u32>();

    if len > 0 {
        // just repeat our seed
        let mut iter = RANDOM_SEED.iter().cycle();
        std::slice::from_raw_parts_mut(buf, len)
            .fill_with(|| *iter.next().unwrap());
    }

    len as i64
}


// random rings, constantly filled with random data
#[elrond::ring::send_cb("maze_random_ring")]
fn maze_random_ring_cb(data: &mut [u8]) -> usize {
    rand::thread_rng().fill_bytes(data);
    data.len()
}

#[elrond::ring::send_cb("food_random_ring")]
fn food_random_ring_cb(data: &mut [u8]) -> usize {
    rand::thread_rng().fill_bytes(data);
    data.len()
}

#[elrond::ring::send_cb("ant_random_ring")]
fn ant_random_ring_cb(data: &mut [u8]) -> usize {
    rand::thread_rng().fill_bytes(data);
    data.len()
}
