//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <sel4/sel4.h>
#include <vka/object.h>
#include <vspace/vspace.h>
#include <cpio/cpio.h>

#include <elrond.h>
#include <elrond_ring.h>


// Constants that control the simulation
#define MAZE_WIDTH 80
#define MAZE_HEIGHT 80
#define MAZE_PRUNE_ITERATIONS 8
#define MAZE_ERODE_ITERATIONS 8
#define MAZE_FOOD_ITERATIONS 5

#define ANT_COUNT 250
#define ANT_SPEED 0.5
#define ANT_WANDER 0.1
#define ANT_TURN 0.25
#define ANT_SIGHT 3.0

#define PHEREMONE_DECAY 0.01

typedef struct ant {
    float x;
    float y;
    float heading_x;
    float heading_y;
    bool has_food;
} ant_t;

typedef struct pheremone {
    float food;
    float home;
} pheremone_t;


// CPIO archive embedded by build system
extern const uint8_t _cpio_archive[];
extern const uint8_t _cpio_archive_end[];


int main(void) {
    seL4_DebugPutString("booting...\n");

    // bootstrap the root task and elrond
    elrond_t elrond;
    int err = elrond_bootstrap(&elrond, "example-root");
    assert(!err);
    vka_t *vka = elrond_vka(&elrond);
    vspace_t *vspace = elrond_vspace(&elrond);
    printf("example-root: successfully bootstrapped\n");

    // find elf file in CPIO archive
    size_t runner_elf_size = 0;
    char const *runner_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "runner-task",
            &runner_elf_size);
    size_t random_elf_size = 0;
    char const *random_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "random-task",
            &random_elf_size);
    size_t maze_elf_size = 0;
    char const *maze_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "maze-task",
            &maze_elf_size);
    size_t ant_elf_size = 0;
    char const *ant_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "ant-task",
            &ant_elf_size);

    // create endpoints
    elrond_ep_t *runner_ep;
    elrond_add_ep(&elrond, "runner-task", "main", &runner_ep);
    elrond_ep_t *random_ep;
    elrond_add_ep(&elrond, "random-task", "main", &random_ep);
    elrond_ep_t *maze_ep;
    elrond_add_ep(&elrond, "maze-task", "main", &maze_ep);
    elrond_ep_t *ant_ep;
    elrond_add_ep(&elrond, "ant-task", "main", &ant_ep);

    // constants
    elrond_add_info(&elrond, "*",
            "maze_width", &(uint64_t){MAZE_WIDTH}, 8);
    elrond_add_info(&elrond, "*",
            "maze_height", &(uint64_t){MAZE_HEIGHT}, 8);
    elrond_add_info(&elrond, "*",
            "maze_prune_iterations", &(uint32_t){MAZE_PRUNE_ITERATIONS}, 4);
    elrond_add_info(&elrond, "*",
            "maze_erode_iterations", &(uint32_t){MAZE_ERODE_ITERATIONS}, 4);
    elrond_add_info(&elrond, "*",
            "maze_food_iterations", &(uint32_t){MAZE_FOOD_ITERATIONS}, 4);
    elrond_add_info(&elrond, "*",
            "ant_count", &(uint64_t){ANT_COUNT}, 4);
    elrond_add_info(&elrond, "*",
            "ant_speed", &(float){ANT_SPEED}, 4);
    elrond_add_info(&elrond, "*",
            "ant_wander", &(float){ANT_WANDER}, 4);
    elrond_add_info(&elrond, "*",
            "ant_turn", &(float){ANT_TURN}, 4);
    elrond_add_info(&elrond, "*",
            "ant_sight", &(float){ANT_SIGHT}, 4);
    elrond_add_info(&elrond, "*",
            "pheremone_decay", &(float){PHEREMONE_DECAY}, 4);

    // create random seed and rings
    elrond_add_info(&elrond, "random-task",
            "random_seed", (uint8_t[]){1,2,3,4}, 4);
    elrond_add_ring(&elrond, "random-task,maze-task",
            "maze_random_ring", 4096,
            random_ep, NULL,
            maze_ep, NULL);
    elrond_add_ring(&elrond, "random-task,ant-task",
            "ant_random_ring", 4096,
            random_ep, NULL,
            ant_ep, NULL);

    // manually create some memory so we can control who can read/write
    size_t maze_page_count = (MAZE_WIDTH*MAZE_WIDTH + PAGE_SIZE_4K-1) / PAGE_SIZE_4K;
    seL4_CPtr *maze_pages = malloc(maze_page_count*sizeof(seL4_CPtr));
    for (size_t i = 0; i < maze_page_count; i++) {
        vka_object_t frame;
        vka_alloc_frame(vka, PAGE_BITS_4K, &frame);
        maze_pages[i] = frame.cptr;
    }
    elrond_add_pages(&elrond, "*", "maze",
            maze_page_count*PAGE_SIZE_4K, maze_pages, PAGE_SIZE_4K,
            seL4_CanRead, true, NULL);
    elrond_add_pages(&elrond, "maze-task", "maze",
            maze_page_count*PAGE_SIZE_4K, maze_pages, PAGE_SIZE_4K,
            seL4_AllRights, true, NULL);

    elrond_add_shared(&elrond, "runner-task,ant-task", "ants",
            ANT_COUNT*sizeof(ant_t), NULL, NULL);
    elrond_add_shared(&elrond, "runner-task,ant-task", "pheremones",
            MAZE_WIDTH*MAZE_WIDTH*sizeof(pheremone_t), NULL, NULL);

    // and connect calls/ntfns
    elrond_add_call(&elrond, "runner-task", "maze_gen", maze_ep, NULL);
    elrond_add_call(&elrond, "maze-task", "maze_gen_done", runner_ep, NULL);
    elrond_add_call(&elrond, "runner-task", "ant_step", ant_ep, NULL);
    elrond_add_call(&elrond, "ant-task", "ant_step_done", runner_ep, NULL);

    // spawn process
    elrond_spawn(&elrond, "runner-task", runner_elf, runner_elf_size);
    elrond_spawn(&elrond, "random-task", random_elf, random_elf_size);
    elrond_spawn(&elrond, "maze-task", maze_elf, maze_elf_size);
    elrond_spawn(&elrond, "ant-task", ant_elf, ant_elf_size);
    seL4_Yield();

    // print for debugging
//    elrond_debugdump(&elrond, NULL);
//    seL4_DebugDumpScheduler();

    // don't segfault
    seL4_TCB_Suspend(seL4_CapInitThreadTCB);
}
