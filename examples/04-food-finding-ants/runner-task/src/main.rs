//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

#![allow(dead_code)]

use elrond_helpers as elrond;
use libsel4rustbindings::*;


#[elrond::info("maze_width")]
static mut MAZE_WIDTH: usize = 0;
#[elrond::info("maze_height")]
static mut MAZE_HEIGHT: usize = 0;
#[elrond::info("maze")]
static mut MAZE: *mut bool = core::ptr::null_mut();

fn cell(x: usize, y: usize) -> Option<&'static mut bool> {
    unsafe {
        // in bounds?
        if x >= MAZE_WIDTH || y >= MAZE_HEIGHT {
            return None;
        }

        Some(&mut *MAZE.add(x*MAZE_WIDTH + y))
    }
}


#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Ant {
    x: f32,
    y: f32,
    heading_x: f32,
    heading_y: f32,
    has_food: bool
}

#[elrond::info("ant_count")]
static mut ANT_COUNT: usize = 0;
#[elrond::info("ants")]
static mut ANTS: *mut Ant = core::ptr::null_mut();

fn ants() -> &'static mut [Ant] {
    unsafe {
        core::slice::from_raw_parts_mut(ANTS, ANT_COUNT)
    }
}


#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Pheremone {
    food: f32,
    home: f32,
}

#[elrond::info("pheremone_decay")]
static mut PHEREMONE_DECAY: f32 = 0.0;
#[elrond::info("pheremones")]
static mut PHEREMONES: *mut Pheremone = core::ptr::null_mut();

fn pheremone(x: usize, y: usize) -> Option<&'static mut Pheremone> {
    unsafe {
        if x >= MAZE_WIDTH || y >= MAZE_HEIGHT {
            return None;
        }

        Some(&mut *PHEREMONES.add(x*MAZE_WIDTH + y))
    }
}


static mut FOOD_PER_FRAME: f32 = 0.0;
fn step() {
    let mut food = 0;

    for ant in ants() {
        // kill and reset to home if off the map
        if let Some((home_x, home_y)) = unsafe { HOME } {
            // wall collisions
            let x = ant.x as isize as usize;
            let y = ant.y as isize as usize;
            if cell(x, y).is_none() || *cell(x, y).unwrap() {
                ant.x = home_x as f32;
                ant.y = home_y as f32;
                ant.heading_x = 0.0;
                ant.heading_y = 0.0;
            }

            // give food to home?
            let dx = ant.x - home_x as f32;
            let dy = ant.y - home_y as f32;
            if ant.has_food && dx*dx+dy*dy <= 2.0*2.0 {
                ant.has_food = false;
                ant.heading_x *= -1.0;
                ant.heading_y *= -1.0;
                food += 1;
            }
        }

        // found food?
        if let Some((food_x, food_y)) = unsafe { FOOD } {
            let dx = ant.x - food_x as f32;
            let dy = ant.y - food_y as f32;
            if dx*dx+dy*dy <= 2.0*2.0 {
                ant.has_food = true;
                ant.heading_x *= -1.0;
                ant.heading_y *= -1.0;
            }
        }
    }

    *unsafe { &mut FOOD_PER_FRAME } = unsafe { FOOD_PER_FRAME }*0.99 + (food as f32)*0.01;
}

static mut RENDER_PREV_LINES: usize = 0;
fn render() {
    // create a rendering of the full maze
    let mut render = vec![' '; unsafe { MAZE_WIDTH*MAZE_HEIGHT }];

//    for y in (0..unsafe { MAZE_HEIGHT }).step_by(2) {
//        for x in 0..unsafe { MAZE_WIDTH } {
//            if pheremone(x, y).unwrap().food > 0.0
//                || pheremone(x, y+1).unwrap().food > 0.0
//            {
//                render[x*unsafe { MAZE_HEIGHT } + y/2] = 'f';
//            } else if pheremone(x, y).unwrap().home > 0.0
//                || pheremone(x, y+1).unwrap().home > 0.0
//            {
//                render[x*unsafe { MAZE_HEIGHT } + y/2] = 'p';
//            }
//        }
//    }

    for ant in ants() {
        let x = ant.x as isize as usize;
        let y = ant.y as isize as usize;
        if cell(x, y).is_none() {
            continue;
        }

        if ant.has_food {
            render[x*unsafe { MAZE_HEIGHT } + y/2] = 'o';
        } else {
            render[x*unsafe { MAZE_HEIGHT } + y/2] = match (
                render[x*unsafe { MAZE_HEIGHT } + y/2],
                y & 1 == 1
            ) {
                ('o',  _)     => 'o',
                (':',  _)     => ':',
                (',',  true)  => ',',
                (',',  false) => ':',
                ('\'', true)  => ':',
                ('\'', false) => '\'',
                (_,    true)  => ',',
                (_,    false) => '\'',
            };
        }
    }

    for y in (0..unsafe { MAZE_HEIGHT }).step_by(2) {
        for x in 0..unsafe { MAZE_WIDTH } {
            if *cell(x, y).unwrap() && *cell(x, y+1).unwrap() {
                render[x*unsafe { MAZE_HEIGHT } + y/2] = '#';
            }
        }
    }

    if let Some((food_x, food_y)) = unsafe { FOOD } {
        render[food_x*unsafe { MAZE_HEIGHT } + food_y/2] = 'F';
    }

    if let Some((home_x, home_y)) = unsafe { HOME } {
        render[home_x*unsafe { MAZE_HEIGHT } + home_y/2] = 'A';
    }

    // print the maze to output
    if unsafe { RENDER_PREV_LINES } > 0 {
        print!("\x1b[{}A", unsafe { RENDER_PREV_LINES });
    }

    println!("\x1b[Kant simulation, {} ants, {:.2} food per frame",
        unsafe { ANT_COUNT },
        unsafe { FOOD_PER_FRAME },
    );
    for y in 0..unsafe { MAZE_HEIGHT }/2 {
        for x in 0..unsafe { MAZE_WIDTH } {
            print!("{}", render[x*unsafe { MAZE_HEIGHT } + y]);
        }
        println!();
    }

    unsafe { RENDER_PREV_LINES = MAZE_HEIGHT/2 + 1; }
}

#[elrond::info("maze_gen")]
static mut MAZE_GEN: seL4_CPtr = 0;
static mut MAZE_GEN_DONE: bool = false;
static mut HOME: Option<(usize, usize)> = None;
static mut FOOD: Option<(usize, usize)> = None;
#[elrond::call_cb("maze_gen_done")]
fn maze_gen_done(msg: seL4_MessageInfo_t) {
    assert_eq!(unsafe { seL4_MessageInfo_get_length(msg) }, 4);
    *unsafe { &mut HOME } = Some((
        unsafe { seL4_GetMR(0) as usize },
        unsafe { seL4_GetMR(1) as usize }
    ));
    *unsafe { &mut FOOD } = Some((
        unsafe { seL4_GetMR(2) as usize },
        unsafe { seL4_GetMR(3) as usize }
    ));
    *unsafe { &mut MAZE_GEN_DONE } = true;
}

#[elrond::info("ant_step")]
static mut ANT_STEP: seL4_CPtr = 0;
static mut ANT_STEP_DONE: bool = false;
#[elrond::call_cb("ant_step_done")]
fn ant_step_done(_msg: seL4_MessageInfo_t) {
    *unsafe { &mut ANT_STEP_DONE } = true;
}


fn main() {
    unsafe { seL4_Send(MAZE_GEN, seL4_MessageInfo_new(0, 0, 0, 0)) };

    while !unsafe { MAZE_GEN_DONE } {
        render();
        elrond::yield_();
    }
    render();

    if let Some((home_x, home_y)) = unsafe { HOME } {
        for ant in ants() {
            ant.x = home_x as f32;
            ant.y = home_y as f32;
            ant.heading_x = 0.0;
            ant.heading_y = 0.0;
            ant.has_food = false;
        }
    }
    render();

    loop {
        *unsafe { &mut ANT_STEP_DONE } = false;
        unsafe { seL4_SetMR(0, HOME.unwrap().0 as seL4_Word) };
        unsafe { seL4_SetMR(1, HOME.unwrap().1 as seL4_Word) };
        unsafe { seL4_SetMR(2, FOOD.unwrap().0 as seL4_Word) };
        unsafe { seL4_SetMR(3, FOOD.unwrap().1 as seL4_Word) };
        unsafe { seL4_Send(ANT_STEP, seL4_MessageInfo_new(0, 0, 0, 4)) };

        while !unsafe { ANT_STEP_DONE } {
            elrond::wait();
        }
        step();
        render();
    }
}
