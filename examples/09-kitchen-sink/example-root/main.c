//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

//#include <autoconf.h>

#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <sel4/sel4.h>
#include <vka/object.h>
#include <vspace/vspace.h>
#include <cpio/cpio.h>

#include <elrond.h>
#include <elrond_ring.h>


// CPIO archive embedded by build system
extern const uint8_t _cpio_archive[];
extern const uint8_t _cpio_archive_end[];


int main(void) {
    seL4_DebugPutString("booting...\n");

    // bootstrap the root task and elrond
    elrond_t elrond;
    int err = elrond_bootstrap(&elrond, "example-root");
    assert(!err);
    vka_t *vka = elrond_vka(&elrond);
    vspace_t *vspace = elrond_vspace(&elrond);
    printf("example-root: successfully bootstrapped\n");

    // create a root endpoint (optional)
    vka_object_t root_ep_object = {0};
    err = vka_alloc_endpoint(vka, &root_ep_object); assert(!err);
    seL4_CPtr root_ep_cptr = root_ep_object.cptr;

    // find elf file in CPIO archive
    size_t elf_size = 0;
    char const *elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task",
            &elf_size);
    size_t task2_elf_size = 0;
    char const *task2_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task2",
            &task2_elf_size);
    size_t task3_elf_size = 0;
    char const *task3_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task3",
            &task3_elf_size);
    size_t task4_elf_size = 0;
    char const *task4_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task4",
            &task4_elf_size);
    size_t task5_elf_size = 0;
    char const *task5_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task5",
            &task5_elf_size);
    size_t task6_elf_size = 0;
    char const *task6_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task6",
            &task6_elf_size);
    size_t task7_elf_size = 0;
    char const *task7_elf = cpio_get_file(
            _cpio_archive,
            _cpio_archive_end - _cpio_archive,
            "example-task7",
            &task7_elf_size);

    elrond_add_env(&elrond, "*",            "TEST",  "1");
    elrond_add_env(&elrond, "example-task", "TEST2", "2");
    elrond_add_env(&elrond, "*",            "TEST3", "3");
    elrond_add_env(&elrond, "example-task", "TEST2", "4");
    elrond_add_env(&elrond, "example-hey",  "TEST4", "5");

    elrond_add_info(&elrond, "*",  "test_info", &(uint32_t){0x12345678}, 4);
    elrond_add_info(&elrond, "*", "alphabet", "abcdefghijklmnopqrstuvwxyz", 26);

    // create an endpoint to recv on
    err = elrond_add_cap(&elrond, "example-task",
            "test_root_add", root_ep_cptr);
    elrond_ep_t root_ep;
    elrond_ep_frombadged(&root_ep, root_ep_cptr, seL4_CapNull);
    elrond_add_call(&elrond, "*", "test_root_call_add", &root_ep, NULL);

    // create test calls, notifications
    elrond_ep_t *example_task_ep;
    elrond_add_ep(&elrond, "example-task", "main", &example_task_ep);
    seL4_CPtr hello_call;
    elrond_add_call(&elrond, NULL,
            "hello_call", example_task_ep, &hello_call);
    seL4_CPtr hello_ntfn;
    elrond_add_ntfn(&elrond, NULL,
            "hello_ntfn", example_task_ep, &hello_ntfn);
    seL4_CPtr hello_wakeup;
    elrond_add_wakeup(&elrond, NULL,
            NULL, example_task_ep, &hello_wakeup);

    // more tests
    elrond_ep_t *example_task2_ep;
    elrond_add_ep(&elrond, "example-task2", "main", &example_task2_ep);
    elrond_add_call(&elrond, "*", "test_call1", example_task2_ep, NULL);
    elrond_add_call(&elrond, "*", "test_call2", example_task2_ep, NULL);
    elrond_add_ntfn(&elrond, "*", "test_ntfn1", example_task2_ep, NULL);
    elrond_add_ntfn(&elrond, "*", "test_ntfn2", example_task2_ep, NULL);

    // test shared memory
    elrond_add_shared(&elrond, "*", "test_shared1",
            strlen("Hello test_shared1!")+1, "Hello test_shared1!",
            NULL);
    void *test_shared2_vaddr;
    elrond_add_shared(&elrond, "*", "test_shared2",
            strlen("Chris was here")+1, NULL,
            &test_shared2_vaddr);
    strcpy(test_shared2_vaddr, "Chris was here!");

    // test ring buffers
    elrond_ep_t *example_task3_ep;
    elrond_add_ep(&elrond, "example-task3", "main", &example_task3_ep);
    elrond_add_ring(&elrond, "example-task2,example-task3",
            "test_ring1", 4096,
            example_task3_ep, NULL,
            example_task2_ep, NULL);
    elrond_add_ring(&elrond, "example-task2,example-task3",
            "test_ring2", 2*4096,
            example_task3_ep, NULL,
            example_task2_ep, NULL);
    elrond_ep_t *example_task4_ep;
    elrond_add_ep(&elrond, "example-task4", "main", &example_task4_ep);
    elrond_ring_t task4_stdout;
    elrond_add_ring(&elrond, "example-task4",
            "stdout", 4096,
            example_task4_ep, &task4_stdout,
            NULL, NULL);

    // more tests for the Rust tasks
    elrond_ep_t *example_task5_ep;
    elrond_add_ep(&elrond, "example-task5", "main", &example_task5_ep);
    seL4_CPtr rust_wakeup;
    elrond_add_wakeup(&elrond, NULL, NULL, example_task5_ep, &rust_wakeup);
    elrond_ep_t *example_task6_ep;
    elrond_add_ep(&elrond, "example-task6", "main", &example_task6_ep);
    elrond_add_call(&elrond, "example-task5",
            "test_rust_call", example_task6_ep, NULL);
    elrond_add_ntfn(&elrond, "example-task5",
            "test_rust_ntfn", example_task6_ep, NULL);
    elrond_add_ring(&elrond, "example-task6,example-task5",
            "test_rust_ring", 4096,
            NULL, NULL,
            example_task6_ep, NULL);
    elrond_add_ring(&elrond, "example-task6,example-task5",
            "test_rust_bytes_ring_in", 4096,
            NULL, NULL,
            example_task6_ep, NULL);
    elrond_add_ring(&elrond, "example-task5,example-task6",
            "test_rust_bytes_ring_out", 4096,
            example_task6_ep, NULL,
            example_task5_ep, NULL);

    elrond_add_info(&elrond, "example-task7",
            "random_seed", (uint8_t[]){1,2,3,4}, 4);
    elrond_ep_t *example_task7_ep;
    elrond_add_ep(&elrond, "example-task7", "main", &example_task7_ep);
    elrond_ring_t random_ring;
    elrond_add_ring(&elrond, "example-task7",
            "random_ring", 4096,
            example_task7_ep, &random_ring,
            NULL, NULL);
        
    // spawn process
    elrond_spawn(&elrond, "example-task", elf, elf_size);
    elrond_spawn(&elrond, "example-task2", task2_elf, task2_elf_size);
    elrond_spawn(&elrond, "example-task3", task3_elf, task3_elf_size);
    elrond_spawn(&elrond, "example-task4", task4_elf, task4_elf_size);
    elrond_spawn(&elrond, "example-task5", task5_elf, task5_elf_size);
    elrond_spawn(&elrond, "example-task6", task6_elf, task6_elf_size);
    elrond_spawn(&elrond, "example-task7", task7_elf, task7_elf_size);
    seL4_Yield();

    // print for debugging
    elrond_debugdump(&elrond, NULL);
    seL4_DebugDumpScheduler();

    ///////////////////////////

    // kick off some things here to avoid too much stdout contention
    seL4_Signal(rust_wakeup);

    // try to recv a raw cap
    for (size_t i = 0; i < 2; i++) {
        seL4_Word badge = 0;
        seL4_MessageInfo_t tag = seL4_Recv(root_ep_cptr, &badge);

        printf("example-root recved %ld + %ld, len = %lu, badge = %lx\n",
                seL4_GetMR(0),
                seL4_GetMR(1),
                seL4_MessageInfo_get_length(tag),
                badge);

        seL4_SetMR(0, seL4_GetMR(0) + seL4_GetMR(1));
        tag = seL4_MessageInfo_new(0, 0, 0, 1);
        seL4_Reply(tag);
    }

    // try to call and notify
    printf("example-root calling hello_call...\n");
    assert(hello_call);
    seL4_MessageInfo_t tag = seL4_MessageInfo_new(0, 0, 0, 0);
    seL4_Call(hello_call, tag);
    printf("example-root got a response!\n");

    printf("example-root notifying hello_ntfn...\n");
    assert(hello_ntfn);
    seL4_Signal(hello_ntfn);

    printf("example-root waking up hello_wakeup...\n");
    assert(hello_wakeup);
    seL4_Signal(hello_wakeup);

    printf("spinning...\n");
    size_t random_bytes = 10;
    while (1) {
        seL4_Yield();
        void *data;
        size_t n = elrond_ring_nbbeginrecv(&task4_stdout, &data);
        if (n > 0) {
            printf("example-root: example-task4 stdout: %ld:\n", n);
            elrond_ring_debugdump(&task4_stdout, "example-root: ");
            ssize_t n_ = write(STDOUT_FILENO, data, n);
            elrond_ring_endrecv(&task4_stdout, n_);
        }
        n = elrond_ring_nbbeginrecv(&random_ring, &data);
        if (n > 0 && random_bytes > 0) {
            printf("example-root: random bytes: ");
            for (int i = 0; i < random_bytes && i < n; i++) {
                printf("%02x", ((uint8_t*)data)[i]);
            }
            random_bytes = random_bytes > n ? random_bytes-n : 0;
            printf("\n");
        }
    }
}
