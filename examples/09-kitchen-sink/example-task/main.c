//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>

#include <sel4/sel4.h>
#include <sel4utils/process.h>

#include <utils/zf_log.h>
#include <sel4utils/sel4_zf_logif.h>

extern char **environ;


typedef struct elrond_info {
    const char *name;
    void *data;
    size_t size;
} elrond_info_t;

uint32_t test_info;
__attribute__((section(".note.elrond.info.test_info,\"\",@note//")))
static const void *__elrond_info_test_info = &test_info;

const char test_alphabet[26];
__attribute__((section(".note.elrond.info.alphabet,\"\",@note//")))
static const void *__elrond_info_alphabet = &test_alphabet;

seL4_CPtr test_root_add;
__attribute__((section(".note.elrond.info.test_root_add,\"\",@note//")))
static void *__elrond_info_test_root_add = &test_root_add;
seL4_CPtr test_root_call_add;
__attribute__((section(".note.elrond.info.test_root_call_add,\"\",@note//")))
static void *__elrond_info_test_root_call_add = &test_root_call_add;

seL4_CPtr test_cspace_root;
__attribute__((section(".note.elrond.info.cspace_root,\"\",@note//")))
static void *__elrond_info_cspace_root = &test_cspace_root;

seL4_CPtr main_call_ep;
__attribute__((section(".note.elrond.info.main_call_ep,\"\",@note//")))
static void *__elrond_info_main_call_ep = &main_call_ep;

seL4_CPtr call1;
__attribute__((section(".note.elrond.info.test_call1,\"\",@note//")))
static void *__elrond_info_test_call1 = &call1;
seL4_CPtr call2;
__attribute__((section(".note.elrond.info.test_call2,\"\",@note//")))
static void *__elrond_info_test_call2 = &call2;
seL4_CPtr ntfn1;
__attribute__((section(".note.elrond.info.test_ntfn1,\"\",@note//")))
static void *__elrond_info_test_ntfn1 = &ntfn1;
seL4_CPtr ntfn2;
__attribute__((section(".note.elrond.info.test_ntfn2,\"\",@note//")))
static void *__elrond_info_test_ntfn2 = &ntfn2;

int main(int argc, char **argv) {
    printf("HEEEEEEEEEEEEEEEEEEEEEEEEEEY\n");

    printf("env:\n");
    for (size_t i = 0; environ[i]; i++) {
        printf("%s\n", environ[i]);
    }

    printf("info:\n");
    printf("test_info <%p>: 0x%08x\n", &test_info, test_info);
    printf("test_alphabet <%p>: ", &test_alphabet);
    for (size_t i = 0; i < 26; i++) {
        printf("%c", test_alphabet[i]);
    }
    printf("\n");
    printf("test_root_add <%p>: %ld\n",
            &test_root_add, test_root_add);
    printf("test_cspace_root <%p>: %ld\n",
            &test_cspace_root, test_cspace_root);

    // send a message to the root
    seL4_SetMR(0, 1);
    seL4_SetMR(1, 2);
    seL4_MessageInfo_t tag = seL4_MessageInfo_new(0, 0, 0, 2);
    tag = seL4_Call(test_root_add, tag);
    assert(seL4_MessageInfo_get_length(tag) == 1);
    int res = seL4_GetMR(0);
    printf("example-task recved %d + %d = %d\n", 1, 2, res);

    seL4_SetMR(0, 3);
    seL4_SetMR(1, 4);
    tag = seL4_MessageInfo_new(0, 0, 0, 2);
    tag = seL4_Call(test_root_call_add, tag);
    assert(seL4_MessageInfo_get_length(tag) == 1);
    res = seL4_GetMR(0);
    printf("example-task recved %d + %d = %d\n", 3, 4, res);

    // send messages to example-task2
    printf("example-task calling %ld\n", call1);
    tag = seL4_MessageInfo_new(0, 0, 0, 0);
    seL4_Call(call1, tag);
    tag = seL4_MessageInfo_new(0, 0, 0, 0);
    seL4_Call(call2, tag);
    seL4_Signal(ntfn1);
    seL4_Signal(ntfn2);

    printf("spinning...\n");
    while (1) {
        seL4_Word badge;
        tag = seL4_Recv(main_call_ep, &badge);
        printf("example-task got a message 0x%016lx!\n", badge);
        if (!(badge & 0x8000000000000000)) {
            tag = seL4_MessageInfo_new(0, 0, 0, 0);
            seL4_Reply(tag);
        }
    }

}
