
cmake_minimum_required(VERSION 3.7.2)

set(target example-task2)
file(GLOB sources *.c)

add_executable(${target} ${sources})
target_link_libraries(${target}
    sel4
    sel4runtime
    muslc
    utils
    elrond_helpers
)
