//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <elrond_helpers.h>
#include <elrond_ring.h>
#include <sel4/sel4.h>
#include <stdio.h>
#include <stdbool.h>

//int main(int argc, char **argv) {
//    printf("hi from example-task2!\n");
//
//    elrond_poll();
//    elrond_spin();
//}


void test_call1(seL4_MessageInfo_t msg) {
    printf("test_call1!\n");
    seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 0));
}
ELROND_CALL_CB("test_call1", test_call1);

void test_call2(seL4_MessageInfo_t msg) {
    printf("test_call2!\n");
    seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 0));
}
ELROND_CALL_CB("test_call2", test_call2);

void test_call3(seL4_MessageInfo_t msg) {
    printf("test_call3!\n");
    seL4_Reply(seL4_MessageInfo_new(0, 0, 0, 0));
}
ELROND_CALL_CB("test_call3", test_call3);

void test_ntfn1(void) {
    printf("test_ntfn1!\n");
}
ELROND_NTFN_CB("test_ntfn1", test_ntfn1);

void test_ntfn2(void) {
    printf("test_ntfn2!\n");
}
ELROND_NTFN_CB("test_ntfn2", test_ntfn2);

void test_ntfn3(void) {
    printf("test_ntfn3!\n");
}
ELROND_NTFN_CB("test_ntfn3", test_ntfn3);

size_t test_ring1_cb(void *data, size_t size) {
    printf("example-task2: recved %ld \"%.*s\"!\n",
            size, (int)size, (char*)data);
    return size;
}
ELROND_RING_RECV_CB("test_ring1", test_ring1_cb);

size_t counter = 0;
bool counter_err = false;

size_t test_ring2_cb(void *data, size_t size) {
    uint8_t *data_ = data;
    for (size_t i = 0; i < size; i++) {
        if (data_[i] != (uint8_t)(counter+i)) {
            printf("example-task2: found error with test_ring2 at byte %lu\n",
                    counter+i);
            printf("example-task2: 0x%02x != 0x%02x\n",
                    data_[i], (uint8_t)(counter+i));
            counter_err = true;
        }

        if (!counter_err && (counter+i+1) % 10000 == 0) {
            printf("example-task2: recved %lu bytes without issue\n",
                    counter+i+1);
        }
    }

    counter += size;
    return size;
}
ELROND_RING_RECV_CB("test_ring2", test_ring2_cb);




//
///*
// * Copyright 2017, Data61, CSIRO (ABN 41 687 119 230).
// *
// * SPDX-License-Identifier: BSD-2-Clause
// */
//
///*
// * seL4 tutorial part 4: application to be run in a process
// */
//
//#include <stdio.h>
//#include <assert.h>
//
//#include <sel4/sel4.h>
//#include <sel4utils/process.h>
//
//#include <utils/zf_log.h>
//#include <sel4utils/sel4_zf_logif.h>
//
///* constants */
//#define MSG_DATA 0x6161 //  arbitrary data to send
//
//extern char **environ;
//
//
//typedef struct elrond_info {
//    const char *name;
//    void *data;
//    size_t size;
//} elrond_info_t;
//
//__attribute__((section("__elrond_info_test_info")))
//uint32_t test_info;
//
//__attribute__((section("__elrond_info_alphabet")))
//const char test_alphabet[26];
//
//__attribute__((section("__elrond_info_test_root_add")))
//seL4_CPtr test_root_add_cptr;
//
//__attribute__((section("__elrond_info_cnode_cptr")))
//seL4_CPtr test_cnode_cptr;
//
//__attribute__((section("__elrond_info_call_ep_cptr")))
//seL4_CPtr call_ep_cptr;
//
//int main(int argc, char **argv) {
//    printf("HEEEEEEEEEEEEEEEEEEEEEEEEEEY\n");
//
//    printf("env:\n");
//    for (size_t i = 0; environ[i]; i++) {
//        printf("%s\n", environ[i]);
//    }
//
//    printf("info:\n");
//    printf("test_info <%p>: 0x%08x\n", &test_info, test_info);
//    printf("test_alphabet <%p>: ", &test_alphabet);
//    for (size_t i = 0; i < 26; i++) {
//        printf("%c", test_alphabet[i]);
//    }
//    printf("\n");
//    printf("test_root_add_cptr <%p>: %ld\n",
//            &test_root_add_cptr, test_root_add_cptr);
//    printf("test_cnode_cptr <%p>: %ld\n",
//            &test_cnode_cptr, test_cnode_cptr);
//
//    // send a message to the root
//    seL4_SetMR(0, 1);
//    seL4_SetMR(1, 2);
//    seL4_MessageInfo_t tag = seL4_MessageInfo_new(0, 0, 0, 2);
//    tag = seL4_Call(test_root_add_cptr, tag);
//    assert(seL4_MessageInfo_get_length(tag) == 1);
//    int res = seL4_GetMR(0);
//    printf("example-task recved %d + %d = %d\n", 1, 2, res);
//
//    printf("spinning...\n");
//    while (1) {
//        seL4_Word badge;
//        tag = seL4_Recv(call_ep_cptr, &badge);
//        printf("example-task got a call 0x%016lx!\n", badge);
//        tag = seL4_MessageInfo_new(0, 0, 0, 0);
//        seL4_Reply(tag);
//    }
//
////    /*
////     * send a message to our parent, and wait for a reply
////     */
////
////    /* set the data to send. We send it in the first message register */
////    tag = seL4_MessageInfo_new(0, 0, 0, 1);
////    seL4_SetMR(0, MSG_DATA);
////
////    
//// /* TASK 8: send and wait for a reply */
////    /* hint 1: seL4_Call()
////     * seL4_MessageInfo_t seL4_Call(seL4_CPtr dest, seL4_MessageInfo_t msgInfo)
////     * @param dest The capability to be invoked.
////     * @param msgInfo The messageinfo structure for the IPC.  This specifies information about the message to send (such as the number of message registers to send).
////     * @return A seL4_MessageInfo_t structure.  This is information about the repy message.
////     *
////     * hint 2: send the endpoint cap using argv (see TASK 6 in the other main.c)
////     */
////
////    ZF_LOGF_IF(argc < 1,
////               "Missing arguments.\n");
////    seL4_CPtr ep = (seL4_CPtr) atol(argv[0]);
////    tag = seL4_Call(ep, tag);
////    
////
////
////    /* check that we got the expected reply */
////    ZF_LOGF_IF(seL4_MessageInfo_get_length(tag) != 1,
////               "Length of the data send from root thread was not what was expected.\n"
////               "\tHow many registers did you set with seL4_SetMR, within the root thread?\n");
////
////    msg = seL4_GetMR(0);
////    ZF_LOGF_IF(msg != ~MSG_DATA,
////               "Unexpected response from root thread.\n");
////
////    printf("process_2: got a reply: %#" PRIxPTR "\n", msg);
////
////    return 0;
//}
