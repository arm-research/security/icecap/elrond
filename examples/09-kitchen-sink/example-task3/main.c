//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <elrond_helpers.h>
#include <elrond_ring.h>
#include <sel4/sel4.h>
#include <stdio.h>
#include <string.h>

void *test_shared1;
__attribute__((section(".note.elrond.info.test_shared1,\"\",@note//")))
static void *__elrond_info_test_shared1 = &test_shared1;
size_t test_shared1_size;
__attribute__((section(".note.elrond.info.test_shared1_size,\"\",@note//")))
static void *__elrond_info_test_shared1_size = &test_shared1_size;

void *test_shared2;
__attribute__((section(".note.elrond.info.test_shared2,\"\",@note//")))
static void *__elrond_info_test_shared2 = &test_shared2;
size_t test_shared2_size;
__attribute__((section(".note.elrond.info.test_shared2_size,\"\",@note//")))
static void *__elrond_info_test_shared2_size = &test_shared2_size;


elrond_ring_t test_ring1;
ELROND_RING_SENDER("test_ring1", &test_ring1);
elrond_ring_t test_ring2;
ELROND_RING_SENDER("test_ring2", &test_ring2);


int main(void) {
    printf("example-task3: found test_shared1 %p, %lu bytes\n",
            test_shared1,
            test_shared1_size);
    printf("example-task3: test_shared1 contents: %s\n",
            (char*)test_shared1);

    printf("example-task3: found test_shared2 %p, %lu bytes\n",
            test_shared2,
            test_shared2_size);
    printf("example-task3: test_shared2 contents: %s\n",
            (char*)test_shared2);

    printf("example-task3: found test_ring1: %ld, %p, %p, %ld bytes\n",
            test_ring1.ntfn,
            test_ring1.control,
            test_ring1.buffer,
            test_ring1.size);
    printf("example-task3: test_ring1 recvavail: %ld bytes\n",
            elrond_ring_recvavail(&test_ring1));
    printf("example-task3: test_ring1 sendavail: %ld bytes\n",
            elrond_ring_sendavail(&test_ring1));
    const char *message = "Hello from example-task3!";
    size_t size = strlen(message);
    elrond_ring_send(&test_ring1, message, size);
    printf("example-task3: test_ring1 sent: %ld bytes\n",
            size);

    printf("example-task3: found test_ring2: %ld, %p, %p, %ld bytes\n",
            test_ring2.ntfn,
            test_ring2.control,
            test_ring2.buffer,
            test_ring2.size);
    size_t total = 50000;
    printf("example-task3: sending %lu bytes to test things...\n",
            total);
    for (size_t i = 0; i < total; i++) {
        elrond_ring_send(&test_ring2, &(uint8_t){(uint8_t)i}, 1);
    }

    elrond_spin();
}
