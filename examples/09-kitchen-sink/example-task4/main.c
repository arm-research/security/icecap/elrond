//
// Copyright (c) 2022 Arm Limited
// SPDX-License-Identifier: MIT

#include <elrond_helpers.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/uio.h>

// intercept __elrond_syscall
extern long __elrond_syscall0(long sysnum, va_list args);
long __elrond_syscall(long sysnum, va_list args) {
    seL4_DebugPutString("example-task4: syscall no ");
    unsigned long x = sysnum;
    unsigned long npw10 = 1;
    while (x/npw10 >= 10) {
        npw10 *= 10;
    }
    while (npw10 > 0) {
        seL4_DebugPutChar('0' + (x/npw10));
        x %= npw10;
        npw10 /= 10;
    }
    seL4_DebugPutString("\n");

    return __elrond_syscall0(sysnum, args);
}

// intercept __elrond_sys_writev
extern long __elrond_sys0_writev(va_list args);
long __elrond_sys_writev(va_list args) {
    va_list args_;
    va_copy(args_, args);
    int fd = va_arg(args_, int);
    struct iovec *iov = va_arg(args_, struct iovec*);
    int iovcnt = va_arg(args_, int);
    va_end(args_);

    if (fd == 42) {
        long total = 0;
        for (int i = 0; i < iovcnt; i++) {
            const char *base = iov[i].iov_base;
            for (int j = 0; j < iov[i].iov_len; j++) {
                if (base[j] >= 'a' && base[j] <= 'z') {
                    seL4_DebugPutChar(base[j] + ('A' - 'a'));
                } else {
                    seL4_DebugPutChar(base[j]);
                }
            }
            total += iov[i].iov_len;
        }
        return total;
    } else {
        return __elrond_sys0_writev(args);
    }
}

int main(void) {
    printf("example-task4: Hello!!!!!!!!!!!!!!!!!!!!!\n");
    dprintf(42, "example-task4: Hello!!!!!!!!!!!!!!!!!!!!!\n");
    elrond_spin();
}
