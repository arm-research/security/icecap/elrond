//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use elrond_helpers as elrond;
use libsel4rustbindings::*;


static mut TEST_INFO: u32 = 0;

#[link_section=".note.elrond.info.test_info"]
#[used]
static mut TEST_INFO_INFO: &'static u32 = unsafe { &TEST_INFO };


#[elrond::info("test_info")]
static mut TEST_INFO2: u32 = 0;


static mut TEST_INFO3: u32 = 0;
elrond::info_ref!("test_info", &mut TEST_INFO3);


#[elrond::info("test_rust_call")]
static mut TEST_RUST_CALL: seL4_CPtr = 0;
#[elrond::info("test_rust_ntfn")]
static mut TEST_RUST_NTFN: seL4_CPtr = 0;

#[elrond::ring::sender("test_rust_ring")]
static mut TEST_RUST_RING: elrond::ring::Sender
    = elrond::ring::Sender::detached();

#[elrond::ring::sender("test_rust_bytes_ring_in")]
static mut TEST_RUST_BYTES_RING_IN: elrond::ring::Sender
    = elrond::ring::Sender::detached();
#[elrond::ring::recver("test_rust_bytes_ring_out")]
static mut TEST_RUST_BYTES_RING_OUT: elrond::ring::Recver
    = elrond::ring::Recver::detached();


fn main() {
    unsafe {
        seL4_DebugPutString(
            b"example-task5: seL4_DebugPutString: Hello from Rust!!!\n\0"
                .as_ptr() as *mut u8
        );
    }
    println!("example-task5: println!: Hello from Rust again!!!");

    // elrond info working?
    println!("example-task5: test_info: 0x{:x}", unsafe { TEST_INFO });
    println!("example-task5: test_info2: 0x{:x}", unsafe { TEST_INFO });
    println!("example-task5: test_info3: 0x{:x}", unsafe { TEST_INFO });

    // this happens
    #[cfg(target_os = "linux")]
    println!("example-task5: Rust thinks it's linux (target_os=linux)");

    // this does not happen :(
    #[cfg(target_os = "sel4")]
    println!("example-task5: Rust thinks it's sel4 (target_os=sel4)");

    // this does happen :)
    #[cfg(target_family = "sel4")]
    println!("example-task5: Rust thinks it's sel4 (target_family=sel4)");

    // wait for a wakeup to do more things, the stdout is just really messy
    // at this point
    elrond::wait();

    // try call
    unsafe {
        println!("example-task5: calling test_rust_call...");
        let msg = seL4_MessageInfo_new(0, 0, 0, 1);
        seL4_SetMR(0, 0x1234);
        let msg = seL4_Call(TEST_RUST_CALL, msg);
        assert_eq!(seL4_MessageInfo_get_length(msg), 1);
        println!("example-task5: recved 0x{:x}", seL4_GetMR(0));
    }

    // try notification
    unsafe {
        println!("example-task5: ntfning test_rust_ntnf...");
        seL4_Signal(TEST_RUST_NTFN);
    }

    // try ring
    println!("example-task5: sending test_rust_ring...");
    unsafe { &mut TEST_RUST_RING }.send(b"Does this ring work?");

    // yield through thread API
    std::thread::yield_now();


    // try another ring
    let data = b"12345678";
    println!("example-task5: sending {}...", String::from_utf8_lossy(data));
    unsafe { &mut TEST_RUST_BYTES_RING_IN }.send(data);

    // spin, note that cbs are still handled in ring recv
    loop {
        let data = unsafe { &mut TEST_RUST_BYTES_RING_OUT }.beginrecv();
        println!("example-task5: example-task5: recved {}", String::from_utf8_lossy(data));
        unsafe { &mut TEST_RUST_BYTES_RING_OUT }.endrecv(data.len());
    }
}

