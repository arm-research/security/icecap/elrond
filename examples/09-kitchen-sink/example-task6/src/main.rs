//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

#![no_main]

use elrond_helpers as elrond;
use libsel4rustbindings::*;

use std::io::Write;


#[elrond::call_cb("test_rust_call")]
fn test_rust_call(msg: seL4_MessageInfo_t) {
    println!("example-task6: Rust got call! ({:?})", msg);

    unsafe {
        let msg = seL4_MessageInfo_new(0, 0, 0, 1);
        seL4_SetMR(0, 0x4321);
        seL4_Reply(msg);
    }
}

#[elrond::ntfn_cb("test_rust_ntfn")]
fn test_rust_ntfn() {
    println!("example-task6: Rust got ntfn!");
}

#[elrond::ring::recv_cb("test_rust_ring")]
fn test_rust_ring(data: &mut [u8]) -> usize {
    print!("example-task6: Rust got ring! ");
    for b in data.iter() {
        print!("{}", *b as char);
    }
    println!();
    data.len()
}


#[elrond::ring::sender("test_rust_bytes_ring_out")]
static mut TEST_RUST_BYTES_RING_OUT: elrond::ring::Sender
    = elrond::ring::Sender::detached();

#[elrond::ring::recv_cb("test_rust_bytes_ring_in")]
fn test_rust_bytes_ring(data: &mut [u8]) -> usize {
    let out = unsafe { &mut TEST_RUST_BYTES_RING_OUT };
    for b in data.iter() {
        write!(out, "{:02x}", b)
            .expect("failed to write to ring");
    }
    data.len()
}

