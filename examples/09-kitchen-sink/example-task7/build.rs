//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use std::env;

// NOTE! build.rs tasks occur in the project's root, but linking occurs
// in the workspace's root. So make sure paths are correct for their
// context.

fn main() {
    // get any C library dependencies and link against them
    println!("cargo:rerun-if-env-changed=C_LIBRARIES");
    let c_libraries = env::var("C_LIBRARIES").unwrap_or_default();
    for library in c_libraries.split(' ') {
        println!("cargo:rerun-if-changed={}", library.trim());
        println!("cargo:rustc-link-arg={}", library.trim());
    }

    // and any extra link flags
    println!("cargo:rerun-if-env-changed=C_LINK_FLAGS");
    let c_link_flags = env::var("C_LINK_FLAGS").unwrap_or_default();
    for link_flag in c_link_flags.split(' ') {
        println!("cargo:rustc-link-arg={}", link_flag.trim());
    }
}
