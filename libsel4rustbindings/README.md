Simple Rust bindings for libsel4, see [seL4's documentation][seL4-api]

[seL4-api]: https://docs.sel4.systems/projects/sel4/api-doc.html
