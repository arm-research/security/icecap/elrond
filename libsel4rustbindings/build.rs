//!
//! Copyright (c) 2022 Arm Limited
//! SPDX-License-Identifier: MIT

use std::env;
use std::path::Path;

fn main() {
    // find sel4/sel4.h
    println!("cargo:rerun-if-env-changed=C_INCLUDE_DIRS");
    let c_include_dirs = env::var("C_INCLUDE_DIRS").unwrap_or_default();
    let sel4_h = c_include_dirs.split(' ')
        .find_map(|dir| {
            let path = format!("{}/sel4/sel4.h", dir.trim());
            if Path::new(&path).is_file() {
                Some(path)
            } else {
                None
            }
        })
        .expect("Could not find sel4/sel4.h");

    // generate bindings for libsel4
    println!("cargo:rerun-if-changed={}", sel4_h);
    bindgen::Builder::default()
        .header(sel4_h)
        .clang_args(
            c_include_dirs.split(' ')
                .map(|dir| format!("-I{}", dir.trim()))
        )
        .use_core()
        .ctypes_prefix("cty")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings for libsel4")
        .write_to_file(format!("{}/sel4.rs", env::var("OUT_DIR").unwrap()))
        .expect("Unable to write bindings for libsel4");
}
